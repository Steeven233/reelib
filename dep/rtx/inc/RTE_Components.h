/*
	\name		RTE_Components.h
	\author		Laurence DV
	\version	1.0.0
	\brief		Specific device mapping file for CMSIS
	\note		Copied from uVision example project
	\license	All right reserved RealEE inc. (2019)
*/

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H	1


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "em_device.h"

/* TODO: Are those needed? */
#define RTE_CMSIS_RTOS                  		/* CMSIS-RTOS */
        #define RTE_CMSIS_RTOS_RTX5             /* CMSIS-RTOS Keil RTX5 */
#define RTE_CMSIS_RTOS2                 		/* CMSIS-RTOS2 */
        #define RTE_CMSIS_RTOS2_RTX5            /* CMSIS-RTOS2 Keil RTX5 */
        #define RTE_CMSIS_RTOS2_RTX5_SOURCE     /* CMSIS-RTOS2 Keil RTX5 Source */
#define RTE_Compiler_EventRecorder
          #define RTE_Compiler_EventRecorder_DAP
//#define RTE_Compiler_IO_STDERR          /* Compiler I/O: STDERR */
//          #define RTE_Compiler_IO_STDERR_ITM      /* Compiler I/O: STDERR ITM */
//#define RTE_Compiler_IO_STDIN           /* Compiler I/O: STDIN */
//          #define RTE_Compiler_IO_STDIN_ITM       /* Compiler I/O: STDIN ITM */
//#define RTE_Compiler_IO_STDOUT          /* Compiler I/O: STDOUT */
//          #define RTE_Compiler_IO_STDOUT_EVR      /* Compiler I/O: STDOUT EVR */
//#define RTE_Compiler_IO_TTY             /* Compiler I/O: TTY */
//          #define RTE_Compiler_IO_TTY_ITM         /* Compiler I/O: TTY ITM */

//#define RTE_RV_GENWAIT                     /* RTOS Validation - GenWait test enabled */
//#define RTE_RV_MAILQUEUE                   /* RTOS Validation - MailQueue test enabled */
//#define RTE_RV_MEMORYPOOL                  /* RTOS Validation - MemoryPool test enabled */
//#define RTE_RV_MSGQUEUE                    /* RTOS Validation - MsgQueue test enabled */
//#define RTE_RV_MUTEX                       /* RTOS Validation - Mutex test enabled */
//#define RTE_RV_SEMAPHORE                   /* RTOS Validation - Semaphore test enabled */
//#define RTE_RV_SIGNAL                      /* RTOS Validation - Signal test enabled */
//#define RTE_RV_THREAD                      /* RTOS Validation - Thread test enabled */
//#define RTE_RV_TIMER                       /* RTOS Validation - Timer test enabled */
//#define RTE_RV_WAITFUNC                    /* RTOS Validation - WaitFunc test enabled */

#endif /* RTE_COMPONENTS_H */
