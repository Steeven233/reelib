/*
 * Copyright (c) 2013-2018 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 * $Revision:   V5.1.0
 *
 * Project:     CMSIS-RTOS RTX
 * Title:       RTX Configuration
 *
 * -----------------------------------------------------------------------------
 */

#include <hardware.h>
#include <conf.h>
#include "cmsis_compiler.h"
#include "rtx_os.h"
#include <stdbool.h>
#include <stdio.h>
#include <REEBUG.h>
#include <em_dbg.h>

// OS Idle Thread
__WEAK __NO_RETURN void osRtxIdleThread (void *argument) {
	(void)argument;

	/* Nom nom nom the tasty Joules... */
	for (;;) {
//		REEBUG_idleCounter();
	}
}

// OS Error Callback function
__WEAK uint32_t osRtxErrorNotify(uint32_t code, void * object_id) {
	char errorString[128] = {};

	/* -- Parsing -- */
	switch (code) {
		/* Stack overflow detected for thread (thread_id=object_id) */
		case osRtxErrorStackUnderflow: 	 {
			osRtxThread_t * obj = object_id;
			if (obj->name == NULL) {
				sprintf(errorString, "[ERR][RTOS]Thread 0x%08lx stack overflow\n", (uint32_t)((osThreadId_t)object_id));
			} else {
				sprintf(errorString, "[ERR][RTOS]Thread %s caused a stack overflow\n", obj->name);
			}

			break;
		}
		/* ISR Queue overflow detected when inserting object (object_id) */
		case osRtxErrorISRQueueOverflow: {
			sprintf(errorString, "[ERR][RTOS]ISR Queue overflow with object 0x%08lx \n", (uint32_t)object_id);
			break;
		}
		/* User Timer Callback Queue overflow detected for timer (timer_id=object_id) */
		case osRtxErrorTimerQueueOverflow: {
			osRtxTimer_t * obj = object_id;
			if (obj->name == NULL) {
				sprintf(errorString, "[ERR][RTOS]Timer CB Queue overflow on timer 0x%08lx\n", (uint32_t)obj);
			} else {
				sprintf(errorString, "[ERR][RTOS]Timer CB Queue overflow on timer %s\n", obj->name);
			}
			break;
		}
		/* Standard C/C++ library libspace not available: increase OS_THREAD_LIBSPACE_NUM */
		case osRtxErrorClibSpace:		 {
			sprintf(errorString, "[ERR][RTOS]stdlib not enough space\n");
			break;
		}
		/* Standard C/C++ library mutex initialization failed */
		case osRtxErrorClibMutex:		 {
			sprintf(errorString, "[ERR][RTOS]stdlib mutex init failed\n");
			break;
		}
		/* Reserved */
		default:						 {
			sprintf(errorString, "[ERR][RTOS]Unknown error with object 0x%08lx\n", (uint32_t)object_id);
			break;
		}
	}
	/* ------------- */

	/* -- Error reporting -- */
	REEBUGSWO_String(errorString);
	/* --------------------- */

	/* -- Error logging -- */
	/* ------------------- */

	/* -- Debug trap -- */
	#if defined(RTOS_DEBUGBREAK_ON_ERROR) && defined(DEBUG)
	bool trap = true;
	while (trap) {
		#if defined __CORE_ACCESS
		if (DBG_Connected()) {
		#else
		{
		#endif
			__BKPT(1);
		}
	}
	#endif
	/* ---------------- */

	return 0;
}
