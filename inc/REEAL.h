/*
	\name		REEAS.h
	\author		Laurence DV
	\version	1.0.0
	\note		Analog Layer services and utils
	\usage		All the function suffixed with _mV handles values as floating point voltages value. The Analog Server does the math
				internally depending on the currently selected reference voltages.
				Where there is uint8_t ain, avout and aiout, it refer to the index inside of REEALconf

	\license	All right reserved RealEE inc. (2018)
*/
#ifndef REEAL_H_
#define	REEAL_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>
#include <cmsis_os2.h>						/*	There is a problem with this... osCMSIS is defined in this file, so it's needed from RTOS
												but when not present the compiler throws an error, so all the compile switches are kinda useless,
												even though, it's the best way for now... please suggest any other scheme */
#include <hardware.h>
#include <conf.h>
#include <REEutil.h>
#include <REEtype.h>
#include <em_chip.h>			/* For low-level def */
#include <em_cmu.h>				/* For clock definition */
#include <em_cryotimer.h>		/* For current timed trigger */
#include <em_adc.h>				/* For ADC fct */
#include <em_vdac.h>			/* For VDAC fct */
#include <em_idac.h>			/* For IDAC fct */
#include <em_ldma.h>			/* For LDMA fct */
#include <em_prs.h>				/* For PRS fct */


/* -------------------------------- +
|									|
|	Hardware definition				|
|									|
+ -------------------------------- */
/* == EDIT BELOW == */
/* \/\/\/\/\/\/\/\/ */
#ifndef REEAL_ADC_DEV
	#define	REEAL_ADC_DEV				(ADC0)
#endif

#ifndef REEAL_ADC_CLK
	#define	REEAL_ADC_CLK				(cmuClock_ADC0)
#endif

#ifndef REEAL_ADC_IRQ
	#define	REEAL_ADC_IRQ				ADC0_IRQn

#endif

#ifndef REEAL_VDAC_DEV
	#define	REEAL_VDAC_DEV				(VDAC0)
#endif

#ifndef REEAL_VDAC_CLK
	#define	REEAL_VDAC_CLK				(cmuClock_VDAC0)
#endif

#ifndef REEAL_VDAC_RATE
	#define	REEAL_VDAC_RATE				(500000)			/* Conversion rate (max 500ksps) */
#endif

#ifndef REEAL_ADC_DMA_CH
	#define	REEAL_ADC_DMA_CH			(DMAM_ADC)
#endif

#ifndef REEAL_TRIG_PRS_CH
	#define	REEAL_TRIG_PRS_CH			3				/* PRS channel used for the ADCServer (NOTE: do not use "()", ie: for Ch0:   0 ) */
#endif

/* /\/\/\/\/\/\/\/\ */
/* == EDIT ABOVE == */

/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */
/* AnalogServer lib error code */
typedef enum {
	REEAL_success =		0,
	REEAL_fail =		-1,
	REEAL_timeout =		-2,
	REEAL_null =		-127,
}REEAL_err_t;

/* Input/Output refreshed callback prototype */
typedef uint16_t (*REEALioCb_t)(uint16_t);


/* -------------------------------- +
|									|
|	Config							|
|									|
+ -------------------------------- */
/* Memory related */

#ifndef REEAL_VERIFICATION_EN
	#define	REEAL_VERIFICATION_EN		(1)				/* 1: Enable multiple verifications and sanity check in all functions (useful in dev phase) */
#endif


/* Callback related */
#ifndef REEAL_CB_MAXNB
	#define REEAL_CB_MAXNB				(4)				/* Maximum number of registered callback (of each type) */
#endif

/* Reference selection */

#ifndef REEAL_ADCREF
	#define	REEAL_ADCREF				(adcRefVDD)		/* Full-scale voltage reference for ADC (see ADC_Ref_TypeDef) */
#endif

#ifndef REEAL_ADCREF_V
	#define	REEAL_ADCREF_V				(3.3)			/* Nominal voltage (in V) of the ADC ref (useful if using external ref) */
#endif

#ifndef REEAL_VDACREF
	#define	REEAL_VDACREF				(vdacRefAvdd)	/* Full-scale voltage reference for VDAC (see VDAC_Ref_TypeDef) */
#endif

#ifndef REEAL_VDACREF_V
	#define	REEAL_VDACREF_V				(3.3)			/* Nominal voltage (in V) of the VDAC ref (useful if using external ref) */
#endif


/* Execution related */
#ifndef REEAL_MUTEX_TIMEOUT_MS
	#define	REEAL_MUTEX_TIMEOUT_MS		(1)				/* Maximum wait time for any mutex protected calls (all set/get) */
#endif

#ifndef REEAL_DEF_REFRESHRATE
	#define REEAL_DEF_REFRESHRATE		(1000UL)		/* ADC Server conversion rate (in Sample per sec) */
#endif

#ifndef REEAL_VADC_NB
	#define REEAL_VADC_NB				(4)				/* Total number of ADC input channel used (max: 32) */
#endif

#ifndef REEAL_VOUTPUT_NB
	#define REEAL_VOUTPUT_NB			(1)				/* Total number of VDAC output channel used (max: 2) */
#endif

#ifndef REEAL_IOUTPUT_NB
	#define REEAL_IOUTPUT_NB			(0)				/* Total number of IDAC output channel used (max: 2) */
#endif


/* Converter timings */
#ifndef REEAL_ADC_CONVCLK
	#define	REEAL_ADC_CONVCLK			(10000000UL)	/* ADC internal clock (in Hz) */
#endif

#ifndef REEAL_ADC_ACQCYCLENB
	#define	REEAL_ADC_ACQCYCLENB		(adcAcqTime128)	/* Number of acquisition cycles for the external input */
#endif

#ifndef REEAL_DAC_CONVCLK
	#define	REEAL_DAC_CONVCLK			(10000000UL)	/* DAC internal clock (in Hz) */
#endif



/* Static pin mapping */
static const struct REEALconf_S {
//	REEAL_ain_t			ain[REEAL_VADC_NB];
//	REEAL_avout_t		avout[REEAL_VOUTPUT_NB];
//	REEAL_aiout_t		aiout[REEAL_IOUTPUT_NB];
	ADC_Ref_TypeDef		ADCref;
	VDAC_Ref_TypeDef	VDACref;
}REEALconf = {
	.ADCref =	REEAL_ADCREF,
	.VDACref =	REEAL_VDACREF,

	/*
	 * This requires a comprehension of
	 * Scan input groups, please see
	 * Section 25.3.5.2 of the reference
	 * manual of EFM32PG12B family
	 * */
	#ifdef REEAL_AIN_0				/* VDAC input 0 */
		.ain[0] =	REEAL_AIN_0,
	#endif
	#ifdef REEAL_AIN_1				/* VDAC input 1 */
		.ain[1] =	REEAL_AIN_1,
	#endif
	#ifdef REEAL_AIN_2				/* VDAC input 2 */
		.ain[2] =	REEAL_AIN_2,
	#endif
	#ifdef REEAL_AIN_3				/* VDAC input 3 */
		.ain[3] =	REEAL_AIN_3,
	#endif
	#ifdef REEAL_AIN_4				/* VDAC input 4 */
		.ain[4] =	REEAL_AIN_4,
	#endif
	#ifdef REEAL_AIN_5				/* VDAC input 5 */
		.ain[5] =	REEAL_AIN_5,
	#endif
	#ifdef REEAL_AIN_6				/* VDAC input 6 */
		.ain[6] =	REEAL_AIN_6,
	#endif
	#ifdef REEAL_AIN_7				/* VDAC input 7 */
		.ain[7] =	REEAL_AIN_7,
	#endif
	#ifdef REEAL_AIN_8				/* VDAC input 8 */
		.ain[8] =	REEAL_AIN_8,
	#endif
	#ifdef REEAL_AIN_9				/* VDAC input 9 */
		.ain[9] =	REEAL_AIN_9,
	#endif

	#ifdef REEAL_AVO_0				/* VDAC output 0 */
		.avout[0] =	REEAL_AVO_0,
	#endif
	#ifdef REEAL_AVO_1				/* VDAC output 1 */
		.avout[1] =	REEAL_AVO_1,
	#endif


//	.ain[REEAL_AIN_VSUP] =	{ .pos=	ANM_VSUPSNS, .neg=	adcNegSelVSS, .group=	adcScanInputGroup1},	/* This requires a comprehension of */
//	.ain[REEAL_AIN_VCAM] =	{ .pos=	ANM_VCAMSNS, .neg=	adcNegSelVSS, .group=	adcScanInputGroup0},	/* Scan input groups, please see */
//	.ain[REEAL_AIN_VBK] =	{ .pos=	ANM_VBKUPSNS,.neg=	adcNegSelVSS, .group=	adcScanInputGroup2},	/* Section 25.3.5.2 of the reference */
//	.ain[REEAL_AIN_VBAT] =	{ .pos=	ANM_VBATSNS, .neg=	adcNegSelVSS, .group=	adcScanInputGroup0},	/* manual of EFM32PG12B family */

//	.avout[0]=	{ .out= ANM_VBKUPCTL},
};


/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Global							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Thread 							|
|									|
+ -------------------------------- */
osThreadId_t REEAL_threadId;

/*
	\brief	Analog Layer control thread
	\note	will call REEAL_init() at boot
 	\param	void const *argument		nothing to do with this parameter
	\return	void
*/
void REEAL_thread(void const *argument);


/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */
/* ==== General ==== */
/*
	\brief	Initialize and configure the analog server for operation
	\note	Ready to operate when this function return
	\usage
	\param	void
	\return	AS_err_t status			AS_success if everything went well
									AS_fail if general failure
*/
REEAL_err_t REEAL_init(void);

/*
	\brief	Put the Analog Layer in a sleep-ready state
	\note	Ready to sleep when this function return
	\usage
	\param	void
	\return	void
*/
void REEAL_sleep(void);


//Thread life-cycle control
REEAL_err_t REEAL_startThread(void);
REEAL_err_t REEAL_stopThread(void) ;


uint8_t REEAL_getADCresolution(void);
float REEAL_getADClsbValue_mV(void);

uint8_t REEAL_getVDACresolution(void);
float REEAL_getVDAClsbValue_mV(void);


/* ==== CB registering ==== */
/*
	\brief	Register a callback to be executed after updated every input/output
	\note
	\usage
	\param	REEALeventCb_t newCb	New callback function
	\return	AS_err_t status			AS_success if everything went well
									AS_fail if general failure
*/
//REEAL_err_t REEAL_registerEventCb(REEALeventCb_t newCb);

/*
	\brief	Register a callback to be executed after a specific ain has been updated
	\note
	\usage
	\param	REEALioCb_t newCb		New callback function
	\return	AS_err_t status			AS_success if everything went well
									AS_fail if general failure
*/
//REEAL_err_t REEAL_registerAinCb(REEALioCb_t newCb, REEAL_ain_t ain);


/* ==== Data access ==== */
/*
	\brief	Return the last value of the selected Analog in
	\note
	\usage
	\param	uint8_t ain				Analog input channel ID
			uint16_t * dstPtr		Where to write the last value
	\return	AS_err_t status			AS_success if everything went well
									AS_fail if general failure
*/
REEAL_err_t REEAL_getAIN(uint8_t ain, uint16_t * dstPtr);
REEAL_err_t REEAL_getAIN_mV(uint8_t ain, float * dstPtr);
REEAL_err_t REEAL_getAIN_raw(uint8_t ain, uint16_t * dstPtr);

/*
	\brief	Prep and output a new value on the selected Analog out
	\note	MUTEX protected
	\usage
	\param	uint8_t aout			Analog output channel ID
			uint16_t  src			New value to be outputed
	\return	AS_err_t status			AS_success if everything went well
									AS_fail if general failure
*/
REEAL_err_t REEAL_setAOUT(uint8_t aout, uint16_t src);
REEAL_err_t REEAL_setAOUT_mV(uint8_t aout, float src);








// REEcode_t REEAL_AINget(void);
// REEcode_t REEAL_AINget_raw(void);
// REEcode_t REEAL_AINget_mV(void);

// REEcode_t REEAL_AINattachCB(void);
// REEcode_t REEAL_AINattachMEM(void);

// REEcode_t REEAL_AOUTset(void);
// REEcode_t REEAL_AOUTset_mV(void);

// REEcode_t REEAL_AOUTattachCB(void);
// REEcode_t REEAL_AOUTattachMEM(void);







/* -------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */
/*
	\brief	ISR to execute on the DMA done
	\note	Must be called only when the ADC's DMA channel is done (same as AS_ADC_DMA_CH)
	\usage
	\param	void
	\return	void
*/
void REEAL_DMAADCISR(void);

#ifdef __cplusplus
}
#endif
#endif	/* REEAL_H_ */

