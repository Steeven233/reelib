/*
	\name		REEBUG.h
	\author		Laurence DV
	\version	1.0.1
	\brief		Debug interfaces and utils
	\note
	\license	All right reserved RealEE inc. (2019)
*/
#ifndef __REEBUG_H__
#define __REEBUG_H__
#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include <REEtype.h>
#include <REEutil.h>					/* For bit def and util */

/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */


/* -------------------------------- +
|	!WARNING!						|
|	Default Config					|
|	Define those before you include	|
|	this file, if you want to		|
|	customize them					|
+ -------------------------------- */
#ifdef REEBUGIO						/* Define this in conf.h to use the IO functions */
	#ifndef REEBUGIO_D0
		#define REEBUGIO_D0			(GPIO(GPIO_PORTF, 4))		/* Bit0 of the IO debug port */
	#endif
	#ifndef REEBUGIO_DMASK
		#define REEBUGIO_DMASK		(0xF)						/* Data mask aligned to Bit0 */
	#endif
	#ifndef REEBUGIO_CLK
		#define REEBUGIO_CLK		(GPIO(GPIO_PORTF, 3))		/* Pin used for the clock sig */
	#endif
	#ifndef REEBUGIO_SYNC
		#define REEBUGIO_SYNC		(1)							/* If 1, enables the clock sig */
	#endif
#endif

#ifdef REEBUGSWO					/* Define this in conf.h to use the SWO/SWV functions */
	#ifndef	REEBUGSWO_LOC			/* Default use Location 0 (PF2) */
		#define	REEBUGSWO_LOC		(0)
	#endif
#endif


/* -------------------------------- +
|									|
|	RTOS idle time counting API		|
|									|
+ -------------------------------- */
/*
	@brief		Count the idle time
	@note		Just call this function in the idle loop, and everything will be taken care of
				This should be called from the idle thread only
	@warning	The first time this function is called, it auto calibrate which preempt all other thread
	@param		void
	@return		void
*/
void REEBUG_idleCounter(void);

/*
	@brief		Return the current idle ratio
	@note		1.0 means the idle thread is running 100% of the available cpu time
	@param		void
	@return		flaot idleRatio			Last measured idle ratio (valid: 0.0 to 1.0)
*/
float REEBUG_idleRatioGet(void);


/* -------------------------------- +
|									|
|	Debug IO						|
|									|
+ -------------------------------- */
/*
	@brief	Specific mode init function
	@note	Those function are used by MXBDG_Init(), but you can call them directly if
			you need multiple debug system at the same time
	@param	void
	@return	void
*/
void REEBUGIO_Init(void);

/*
	@brief	Set all the masked pins to 1
	@note
	@param	uint16_t mask		Bitwise mask determining the affected pins
	@return	void
*/
void REEBUGIO_SET(uint16_t mask);

/*
	@brief	Set all the masked pins to 0
	@note
	@param	uint16_t mask		Bitwise mask determining the affected pins
	@return	void
*/
void REEBUGIO_CLR(uint16_t mask);

/*
	@brief	Toggle all the masked pins
	@note
	@param	uint16_t mask		Bitwise mask determining the affected pins
	@return	void
*/
void REEBUGIO_TOG(uint16_t mask);

/*
	@brief	Short pulse on all the masked pins
	@note
	@param	uint16_t mask		Bitwise mask determining the affected pins
	@return	void
*/
void REEBUGIO_PLS(uint16_t mask);

/*
	@brief	Output the code on Debug port with a configurable number of bit
	@note
	@param	uint32_t code		Code value to print
			uint8_t codeWidth	Code width in bit number (valid: depend on underlying port)
	@return	void
*/
void REEBUGIO_Code(uint32_t code, uint8_t codeWidth);

/*
	@brief	Output an array of bytes on the debug IO port
	@note	This is a wrapper using REEBUGIO_Code()
	@param	uint8_t * arrayPtr	Start address of the array to output
	@param	uint32_t byteNb		Size of the array (in bytes)
	@return	void
*/
void REEBUGIO_Array(uint8_t * array, uint32_t len);

/*
	@brief	Output a null terminated string on the debug IO port
	@note	This is a wrapper using REEBUGIO_Code()
	@param	const char * string		string to print
	@return	void
*/
void REEBUGIO_String(const char * string);



/* -------------------------------- +
|									|
|	SWO	/ SWV						|
|									|
+ -------------------------------- */
/*
	@brief	Specific mode init function
	@note	Those function are used by MXBDG_Init(), but you can call them directly if
			you need multiple debug system at the same time
	@param	void
	@return	void
*/
void REEBUGSWO_Init(void);


/*
	@brief	Output the code via the SWO pin
	@note
	@param	uint32_t code		Code value to print
	@return	void
*/
void REEBUGSWO_Code(uint32_t code);

/*
	@brief	Output an array of bytes on the SWO port
	@note	This is a wrapper using REEBUGSWO_Code()
	@param	uint8_t * arrayPtr	Start address of the array to output
	@param	uint32_t byteNb		Size of the array (in bytes)
	@return	void
*/
void REEBUGSWO_Array(uint8_t * array, uint32_t len);

/*
	@brief	Output a null terminated string on the debug output port
	@note	This is a wrapper using REEBUGSWO_Code()
	@param	const char * string		string to print
	@return	void
*/
void REEBUGSWO_String(const char * string);



#ifdef __cplusplus
}
#endif
#endif

