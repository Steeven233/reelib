/*
	@name		REEFled.h
	@author		Laurence DV
	@version	2.0.0
	@brief		REEF LED driver
	@note		
	@license	SEE $REElib_root/README.md
*/
#ifndef __REEFLED_H_
#define	__REEFLED_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <hardware.h>
#include <conf.h>
#include <REEtype.h>
#include <REEutil.h>

#include <REEF.h>


/* -------------------------------- +
|									|
|	Config							|
|	MUST BE SET						|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	HW Config						|
|	MUST BE SET						|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */
typedef struct REEFled_ioConf_s{
	const uint8_t		pin;			/* REEIO controlling the LED */
	const bool			activeVal;		/* IO value to active the LED */
	const REEcolor_t	color;			/* This is only metadata, could be useful to some code using REEF */
}REEFled_ioConf_t;

typedef struct REEFled_s{
	
}REEFled_t;


/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Global							|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Normal API						|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	CMSIS-RTOS API					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */


#ifdef __cplusplus
}
#endif
#endif	/* __REEFLED_H_ */
