/*
	@name		REEtime.h
	@author		Laurence DV
	@version	1.0.0
	@brief		Timing and RTCC utilities
	@note
	@license	SEE $REElib_root/LICENSE.md
*/
#ifndef __REETIME_H__
#define __REETIME_H__	1
#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include <REEtype.h>					/* General Type, ready to serve! */
#include <REEutil.h>					/* For bit def and util */
#include <em_device.h>					/* For register type */
#include <em_rtcc.h>					/* For RTCC types */


/* -------------------------------- +
|									|
|	Public Type						|
|									|
+ -------------------------------- */
/* Example custom configuration structure */
typedef struct __attribute__((packed)) REEtime_conf_s {
	RTCC_TypeDef *				hw;
	RTCC_Init_TypeDef			hw_conf;
	RTCC_CCChConf_TypeDef		cc0_conf;
	RTCC_CCChConf_TypeDef		cc1_conf;
	RTCC_CCChConf_TypeDef		cc2_conf;
	uint32_t					freqA_hz;
	uint32_t					freqB_hz;
}REEtime_conf_t;

typedef struct __attribute__((packed)) REEtime_s {
	uint32_t					sec;
	uint16_t					usec;
	uint16_t					:16;
}REEtime_t;

/* -------------------------------- +
|	!WARNING!						|
|	Default Config					|
|	Define any of these in conf.h,	|
|	If not defined, these default 	|
|	will be used instead			|
+ -------------------------------- */
#define	REETIME_BASEFREQ_HZ_DEF		(1U)			/* Basefreq is the reverse of time-base quanta (undividable base unit) */
#define	REETIME_FREQA_HZ_DEF		(10000U)
#define	REETIME_FREQB_HZ_DEF		(50U)
#define	REETIME_FREQA_PRS_DEF		(1U)
#define	REETIME_FREQB_PRS_DEF		(2U)


/* -------------------------------- +
|									|
|	Public Constant					|
|									|
+ -------------------------------- */
static const REEtime_conf_t		REEtime_conf_def = {
	.hw =							RTCC,

	.hw_conf.cntMode =				rtccCntModeNormal,
	.hw_conf.enaOSCFailDetect =		true,
	.hw_conf.enable =				true,
	.hw_conf.precntWrapOnCCV0 =		true,
	.hw_conf.prescMode =			rtccCntTickCCV0Match,

	.cc0_conf.chMode =				rtccCapComChModeOff,			/* CC0 is used for pre-counter period */

	.cc1_conf.chMode =				rtccCapComChModeCompare,		/* CC1 is used for FREQA generation */
	.cc1_conf.compMatchOutAction =	rtccCompMatchOutActionToggle,
	.cc1_conf.prsSel =				REETIME_FREQA_PRS_DEF,
	.cc1_conf.compBase =			rtccCompBasePreCnt,
	.cc1_conf.compMask =			31,

	.cc2_conf.chMode =				rtccCapComChModeCompare,		/* CC2 is used for FREQB generation */
	.cc2_conf.compMatchOutAction =	rtccCompMatchOutActionToggle,
	.cc2_conf.prsSel =				REETIME_FREQB_PRS_DEF,
	.cc2_conf.compBase =			rtccCompBasePreCnt,
	.cc2_conf.compMask =			31,

	.freqA_hz =						REETIME_FREQA_HZ_DEF,
	.freqB_hz =						REETIME_FREQB_HZ_DEF,
};


/* -------------------------------- +
|									|
|	Public Global					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Public BareMetal API			|
|									|
+ -------------------------------- */
/*	@brief	REEtime init function
	@note	Use REEtime_conf_t if you are not sure what to pass
	@param	REEtime_conf_t * conf		Custom configuration struct
	@return	REEcode_t 					REEcode_success if everything went well
										REEcode_null if given NULL pointer
*/
REEcode_t REEtime_Init(const REEtime_conf_t * conf);

/*	@brief	Timestamping function
	@note	returns the current time aligned for easy analysis
	@param	void						-
	@return	REEtime_t					Current time
*/
REEtime_t REEtime_stamp(void);


/* -------------------------------- +
|									|
|	Public ISR						|
|	(wtf would be a private one...)	|
+ -------------------------------- */
/*	@brief	Example hw Interrupt Service Routine
	@note	The standard pattern used here is custom function doing the specific ISR handling needed,
			but the hardware on which this ISR work is abstracted through the $hwPtr argument
			You should have all IRQ vectors in a project-specific source file like main.c or interrupt.c
	@param	void * hwPtr				Hardware register pointer for handling the ISR
	@return	void						ISR CANNOT RETURN SOMETHING YOU DUMMY
*/
void REEtime_RTCC_ISR(void * hwPtr);


#ifdef __cplusplus
}
#endif
#endif	/* __REETIME_H__ */

