/*
 * REEUSB.h
 *
 *  Created on: Aug 20, 2019
 *      Author: jlecuyer2
 */

#ifndef REELIB_INC_REEUSB_H_
#define REELIB_INC_REEUSB_H_

#include <em_usb.h>

#if defined(USB_PRESENT) && (USB_COUNT == 1)

/*Largest byte exchange*/
#define REEUSB_MAX_SIZE 	(USB_FS_BULK_EP_MAXSIZE-1)

#define REEUSB_MSG_QUEUE_LEN	(4)

typedef struct{
	uint8_t qty;
	uint8_t val[REEUSB_MAX_SIZE];
}usbMessage_t;


/***************************************************************************//**
 * @brief
 *  Usb data receive callback function.
 *
 * @details
 *  Called whenever the device receive a usb frame (64 bytes).
 *
 * @param[in] data_rcv
 *   Pointer to the received byte buffer
 *
 * @param[in] len
 *   Number of bytes received.
 ******************************************************************************/
typedef void (*REEUSB_onReceive_cb_t)(usbMessage_t *msg);

/***************************************************************************//**
 * @brief
 *  Usb data transmit callback function.
 *
 * @details
 *  Called whenever the device succesfully transmitted a usb frame .
 *
 ******************************************************************************/
bool REEUSB_isBusy();
bool REEUSB_tx(usbMessage_t *msg);
//void REEUSB_reset();


void cdcInit(void);
int  cdcSetupCmd(const USB_Setup_TypeDef *setup);
void cdcStateChangeEvent(USBD_State_TypeDef oldState, USBD_State_TypeDef newState);

void REEUSB_init(REEUSB_onReceive_cb_t onReceive_cb);


#endif

#endif /* REELIB_INC_REEUSB_H_ */
