/*
	\name		REEUSI.h
	\author		Laurence DV
	\version	1.0.0
	\note		I2C Peripheral lib, largely inspired by i2cspm driver from SiLabs
				Only single-master mode currently implemented
				Not yet thread-safe
	\license	All right reserved RealEE inc. (2019)
*/
#ifndef REEUSI_H_
#define	REEUSI_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>						/* Because stdint mofo */
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include <REEtype.h>					/* Universal types */
#include <REEutil.h>					/* Utilities */


/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */
typedef enum {
	REEUSI_success =	0,
	REEUSI_fail =		-1,
	REEUSI_timeout =	-2,
	REEUSI_param =		-3,
	REEUSI_null =		-128,
}REEUSI_err_t;

typedef struct {
	void *			hw;
	CMUclock_t		clk;
	bool			blocking;		/* 1: functions are blocking | 0: will return immediately */
}REEUSI_ctl_t;

typedef struct {
	REE_type_t		dataType;													/* Data access type used by tx,rx,trx functions */
	REEUSI_ctl_t *	ctl;														/* Specific interface control structure */
	REEEUSI_err_t	(*REEUSI_take)(void *);										/* Threads take exclusive control of USI peripheral */
	REEEUSI_err_t	(*REEUSI_release)(void *);									/* Threads release exclusive control of USI peripheral */
	uint32_t 		(*REEUSI_tx)(void *, void *, uint32_t);						/* Transmit data on the USI */
	uint32_t 		(*REEUSI_rx)(void *, void *, uint32_t);						/* Receive data on the USI */
	uint32_t 		(*REEUSI_trx)(void *, void *, void *, uint32_t);			/* Bidirectional transfer on the USI */
	REEUSI_err_t	(*REEUSI_ioctl)(void *, void *)								/* Special IO commands for the USI (specific to each type) */
	REEUSI_err_t	(*REEUSI_hook)(void *, void *, void(*REEcb_t)(void *));		/* Attach a callback function to the specified hook of the USI (specific to each type) */
	REEUSI_err_t	(*REEUSI_unhook)(void *, void *, void(*REEcb_t)(void *));	/* Detach a callback function from the specified hook of the USI (specific to each type) */
}REEUSI_t;

typedef struct {
	uint32_t					action;			/* Action to do */
	union {
		uint32_t				value;			/* Action's data as a numerical value */
		void *					ptr;			/* Action's data as a pointer */
	}data;	
}REEUSI_ioctl_t;


/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Global							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Example API						|
|	(no implementation present)		|
+ -------------------------------- */
/*
	\brief	Try to take/release exclusive control of an hw peripheral
	\note	This function will block if $usi.blocking == TRUE, otherwise it
			will return immediately
	\usage	Useful to ensure multiple calls of tx/rx will be sequencial on the hw bus

	\param	REEUSI_t * usi						USI handle 
	\return	REEUSI_err_t status					REEUSI_success if everything went well
												REEUSI_null if usi is NULL
												REEUSI_param is invalid parameters
												REEUSI_fail if couldn't take/release the hw
*/
REEUSI_err_t REEUSI_take(REEUSI_t * usi);
REEUSI_err_t REEUSI_release(REEUSI_t * usi);

/*
	\brief	Send/Receive a transaction
	\note	
	\usage	

	\param	REEUSI_t * usi						USI handle  
	\param	void * src/dst						Data source/destination pointer
	\param	uint32_t len						Data lenght
	\return	uint32_t actualLen					Actual data lenght transfered
*/
uint32_t REEUSI_tx(REEUSI_t * usi, void * src, uint32_t len);
uint32_t REEUSI_rx(REEUSI_t * usi, void * dst, uint32_t len);
uint32_t REEUSI_trx(REEUSI_t * usi, void * src, void * dst, uint32_t len);

/*
	\brief	Execute a special IO action
	\note	
	\usage	

	\param	REEUSI_t * usi						USI handle  
	\param	REEUSI_ioctl_t * args				IO action arguments
	\return	REEUSI_err_t status					REEUSI_success if everything went well
												REEUSI_null if $usi or $args is NULL
												REEUSI_param is invalid parameters
												REEUSI_fail if generic error
*/
REEUSI_err_t REEUSI_ioctl(REEUSI_t * usi, REEUSI_ioctl_t * args);

/*
	\brief	Attach/Detach a function to a specific hook
	\note
	\usage	

	\param	REEUSI_t * usi						USI handle  
	\param	REEUSI_hook_t * hook				Hook type to attach/detach
	\param	void(*REEcb_t)(void *)				Callback function to call when the hook is triggered
	\return	REEUSI_err_t status					REEUSI_success if everything went well
												REEUSI_null if $usi or $args is NULL
												REEUSI_param is invalid parameters
												REEUSI_fail if generic error
*/
REEUSI_err_t REEUSI_hook(REEUSI_t * usi, REEUSI_hook_t * hook, void(*REEcb_t)(void *));
REEUSI_err_t REEUSI_unhook(REEUSI_t * usi, REEUSI_hook_t * hook, void(*REEcb_t)(void *));


#ifdef __cplusplus
}
#endif
#endif	/* REEUSI_H_ */

