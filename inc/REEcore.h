/*
*	\name		REEcore.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		Core access and low level execution stuff
*	\note		
*	\warning	None
*	\license	All right reserved RealEE inc. (2019)
*/

#ifndef __REECORE_H__
#define __REECORE_H__

#ifdef __cplusplus
	extern "C" {
#endif

#include <REEtype.h>


/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Define							|
|									|
+ -------------------------------- */


/*--------------------------------- +
|									|
|	Macro							|
|									|
+ -------------------------------- */



/*--------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */
/*
	\brief	Return the cmu's clock id of the designated peripheral
	\note	Return 0 if peripheral not found
	\usage	CMU_ClockEnable(REEcore_findCMUClock((void*)TIMER0), true)

	\param	void * hwPtr						Hardware register pointer 
	\return	CMU_Clock_TypeDef clock				CMU's clock id
*/
CMU_Clock_TypeDef REEcore_findCMUClock(void * hwPtr);

/*
	\brief	Return the irq number of the designated peripheral
	\note	Return 0 if peripheral not found
			Will return GPIO_EVEN_IRQn for GPIO_BASE (GPIO_ODD_IRQn is +8)
			Will return USARTx_RX_IRQn for any USART,UART (USARTx_TX_IRQn is +1) (LEUART have only 1 IRQ)
	\usage	
			
	\param	void * hwPtr						Hardware register pointer 
	\return	IRQn_Type irq						IRQ number
*/
IRQn_Type REEcore_findIRQ(void * hwPtr);


#ifdef __cplusplus
}
#endif
#endif

