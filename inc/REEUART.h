/*
	\name		REEUART.h
	\author		Laurence DV
	\version	1.0.0
	\note		
	\license	All right reserved RealEE inc. (2019)
*/
#ifndef REEUART_H_
#define	REEUART_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>						/* Because stdint mofo */
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include <REEtype.h>					/* Universal types */
#include <REEutil.h>					/* Utilities */
#include <cmsis_os2.h>					/* RTOS API */
#include <em_chip.h>					/* Chip's register definitions */

#include <string.h>
#include <em_cmu.h>
#include <em_gpio.h>
#include <em_usart.h>
#include <em_leuart.h>


/* -------------------------------- +
|									|
|	Config							|
|									|
+ -------------------------------- */
#ifndef REEUART_RXTIMEOUT_MS
	#define	REEUART_RXTIMEOUT_MS		(1)		/* Period to receive a character, after which error is thrown */
#endif


/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */
/* UART handle (the owner of the peripheral should have this) */
typedef struct {
	REEtype_t					dataType;		/* Data access type used by tx,rx,trx functions */
	struct {
		USART_TypeDef *			dev;			/* HW Peripheral registers */
		CMU_Clock_TypeDef		clk;			/* Peripheral clock */
		struct {
			IRQn_Type			irq;			/* HW IRQ */
			uint8_t				loc;			/* port location */
			uint8_t				pin;			/* pin number */
			struct {
				uint32_t		in;				/* buffer's input index */
				uint32_t		out;			/* buffer's output index */
				uint32_t		cnt;			/* buffer's current pending bytes number */
				void *			data;			/* ring buffer */
			}buf;
		}tx;
		struct {
			IRQn_Type			irq;			/* HW IRQ */
			uint8_t				pin;			/* pin number */
			uint8_t				loc;			/* port location */
			struct {
				uint32_t		in;				/* buffer's input index */
				uint32_t		out;			/* buffer's output index */
				uint32_t		cnt;			/* buffer's current pending bytes number */
				void *			data;			/* ring buffer */
			}buf;
		}rx;
		uint32_t				bitrate;		/* Current bitrate of the UART (in bps) */
	}ctl;
}REEUART_t;

/* Sequence type (for convience) */
//typedef I2C_TransferSeq_TypeDef REEI2C_Sequence_t;

/* REEUART lib error code */
typedef enum {
	REEUART_success =		0,
	REEUART_fail =			-1,
	REEUART_timeout =		-2,
	REEUART_collision =		-3,
	REEUART_null =			-127,
}REEUART_err_t;

typedef struct {

}REEUART_ioctl_t;

typedef struct {

}REEUART_hook_t;

/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Global							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */
REEUART_err_t REEUART_take(REEUART_t * uart);
REEUART_err_t REEUART_release(REEUART_t * uart);

uint32_t REEUART_tx(REEUART_t * uart, void * src, uint32_t len);
uint32_t REEUART_rx(REEUART_t * uart, void * dst, uint32_t len);
uint32_t REEUART_trx(REEUART_t * uart, void * src, void * dst, uint32_t len);

REEUART_err_t REEUART_ioctl(REEUART_t * uart, REEUART_ioctl_t * args);

REEUART_err_t REEUART_hook(REEUART_t * uart, REEUART_hook_t * hook, void(*REEcb_t)(void *));
REEUART_err_t REEUART_unhook(REEUART_t * uart, REEUART_hook_t * hook, void(*REEcb_t)(void *));



















/*
	\brief	Initialize and configure an USART as a UART hw peripheral
	\note	Ready to operate when this function return
	\usage	The "owner" of the peripheral should create a REEUART_t variable with
			the desired configuration and pass it to the lib. Hold on to that variable as
			it's the main control struct for the lib and it's uses for all function call.
			Use only REEUART API function to access the handle AFTER initalization
	\param	REEUART_t * UART				UART peripheral handle 
	\return	REEUART_err_t status				REEUART_success if everything went well
												REEUART_timeout if no response
												REEUART_null if given a NULL pointer
												REEUART_fail if general failure
*/
REEUART_err_t REEUART_Init(REEUART_t * uart);

/*
	\brief	Data transmission of an array of bytes 
	\note	Blocking, but uses CMSIS RTOS sleeping if available (only the calling thread will be blocked)
	\usage	
	\param	REEUART_t * UART				UART peripheral handle 
			uint8_t * src						Pointer to the data to send
			uint16_t len						Lenght of the data to send
	\return	uint16_t count						Actual number of byte transfered
*/
uint16_t REEUART_Send(REEUART_t * uart, uint8_t * src, uint16_t len);


uint16_t REEUART_Receive(REEUART_t * uart, uint8_t * dst, uint16_t len);

uint16_t REEUART_GetRxPending(REEUART_t * uart);

/* -------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */
void REEUART_RXISR(REEUART_t * uart);
void REEUART_TXISR(REEUART_t * uart);


#ifdef __cplusplus
}
#endif
#endif	/* REEUART_H_ */

