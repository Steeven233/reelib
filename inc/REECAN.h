/*
	@name		REECAN.h
	@author		Jonathan Lecuyer LEsperance
	@version	1.0.0
	@note		CAN Peripheral lib, largely inspired by REEI2C
	@license	SEE $REElib_root/LICENSE.md
*/
#ifndef REECAN_H_
#define	REECAN_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ ---------------------------------*/
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include <REEtype.h>					/* General Type, ready to serve! */
#include <REEutil.h>					/* For bit def and util */
#include <stdio.h>
#include <em_can.h>


/* -------------------------------- +
|	!WARNING!						|
|	Default Config					|
|	Define any of these in conf.h,	|
|	If not defined, these default 	|
|	will be used instead			|
+ -------------------------------- */
#define		REECAN_RTOS_TIMEOUT_MS_DEF			(10)	/* Maximum time allowed to access RTOS ressources (in millisec) */
#define		REECAN_HOOK_MAXCNT_DEF				(4)		/* Maximum number of callbacks attached to a single hook (excluding data hooks) */


/* -------------------------------- +
|									|
|	Type							|
|									|
+ ---------------------------------*/
/* REECAN hook list */
typedef enum {
	REECAN_hook_any			= 1,		/* Called on any message reception */
	REECAN_hook_rx			= 2,		/* Called on any message reception */
	REECAN_hook_rxId		= 3,		/* Called on message reception matching CAN ID */
	REECAN_hook_rxBroadcast	= 4,		/* Called on message reception matching broadcast pattern */
	REECAN_hook_tx			= 5,		/* Called on any message transmission*/
	REECAN_hook_txMIR		= 6,		/* Called on message transmission matching specified Message Interface Interface */

	REECAN_hook_error		= 7,		/* An error occured on the bus, see CAN_STATUS->LEC*/

}REECAN_hook_type_t;

/* REECAN hook control block */
typedef struct{
	REECAN_hook_type_t REECAN_hookType;
	void(*REECAN_callback)(void *);
}REECAN_hook_t;

/* REECAN control block */
typedef struct {
	#ifdef CAN_PRESENT
	CAN_TypeDef *			candev;			/* Pointer to the CAN peripheral register block.*/
	CAN_Mode_TypeDef		mode;			/* CAN operating mode*/
	#endif
	uint32_t				maxWaitTime_ms;	/* max wait time if device/mutex isn't available */
	uint8_t					tx;				/* tx pin encoded with REEIO() 			*/
	uint8_t					rx;				/* rx pin encoded with REEIO() 			*/
	uint8_t					locationTx;		/* Port location of tx signal 			*/
	uint8_t					locationRx;		/* Port location of rx signal 			*/
	uint32_t				bitrate;		/* CAN operating bitrate (in bps) 		*/
	uint8_t					canRxChannelReg;/* CAN channel for regular ID reception	*/
	uint8_t					canRxChannelExt;/* CAN channel for extended ID reception*/
	uint8_t					hookQty;		/* Number of hook pointed by hookList	*/
	REECAN_hook_t *			hookList;		/* Hook list 							*/
}REECAN_t;

/* REECAN incoming message format */
typedef struct {
	#ifdef CAN_PRESENT
	REECAN_t *		canHandle;	/* A pointer to the associated canHandle */

	#endif
	uint32_t		id;					/* ID of the message with 11 bits (standard) or 28 bits (extended). LSBs are used for both. */
	uint32_t		mask;				/* A mask for ID filtering. */
	uint8_t			data[8];			/* A pointer to data, [0 - 8] bytes. */
	struct {
		uint8_t		dlc				:4; /* Data Length Code [0 - 8]. */
		uint8_t		mirChannel		:5; /* msgNum A message number of this Message Object, [1 - 32]. */
		bool		direction_tx	:1; /* Direction of the message, transmit if true, receive if false */
		bool		extended		:1; /* ID extended if true, standard if false. */
		bool		extendedMask	:1; /* Enable the use of 'extended' value for filtering. */
		bool		remoteTransfer	:1; /* True if the Message Object is used for remote transmission, false otherwise. */
		bool		endOfBuffer		:1; /* True if it is for a single Message Object or the end of a FIFO buffer, false if the Message Object is part of a FIFO buffer and not the last. */
		bool		useMask			:1;	/* A boolean to choose whether or not to use the masks. */
		bool		isError			:1;	/* Empty message reporting an error*/
	};
}REECAN_messageObject_t;

/* REECAN error code */
typedef enum {
	REECAN_success =		0,
	REECAN_fail =			-1,
	REECAN_timeout =		-2,
	REECAN_collision =		-3,
	REECAN_nack =			-4,
	REECAN_full =			-5,
	REECAN_notFound =		-6,
	REECAN_denied =			-7,

	REECAN_param =			-127,
	REECAN_null =			-128,
}REECAN_err_t;

/* REECAN IOctl supported actions list */
typedef enum {
	REECAN_act_setMode 		= 3,	/* Change CAN operating mode, 						[value = (CAN_Mode_TypeDef) mode ]				*/
	REECAN_act_setBaudrate 	= 4,	/* Change CAN operating Baud rate					[value = Baudrate] 								*/
}REECAN_ioctl_actionList_t;

/* REECAN IOctl message format */
typedef struct {
	REECAN_ioctl_actionList_t	action;			/* Action to do */
	union {
		uint32_t				value;			/* Action's data */
		void *					ptr;			/* Action's pointer */
	};
}REECAN_ioctl_t;


/* -------------------------------- +
|									|
|	Constant						|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	Global							|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	API								|
|									|
+ ---------------------------------*/
/*
	@brief	Try to take/release exclusive control of an CAN hw peripheral
	@note	This function will block if $can.ctl.blocking == TRUE, otherwise it
			will return immediately
	@usage	Useful to ensure multiple calls of tx/rx will be sequencial on the hw bus

	@param	REECAN_t * can						CAN virtual peripheral handle
	@return	REECAN_err_t status					REECAN_success if everything went well
												REECAN_null if CAN is NULL
												REECAN_param is invalid parameters
												REECAN_fail if couldn't take/release the hw
*/
REECAN_err_t REECAN_open(REECAN_t * can);
REECAN_err_t REECAN_close(REECAN_t * can);

/*
	@brief	Send/Receive a transaction on the CAN bus
	@note	For now those function are always blocking (upto REECAN_RTOS_TIMEOUT_MS)
			TRX will do WriteToSlave then ReadFromSlave will the same len (interface requirements)

	@param	REECAN_t * can						CAN virtual peripheral handle
 	@param CAN_MessageObject_TypeDef *message	CAN message
	@return	REECAN_err_t 						reecan return code
*/
REECAN_err_t REECAN_tx(REECAN_t * can, CAN_MessageObject_TypeDef *message, bool remoteTransfer);
uint32_t REECAN_rx(REECAN_t * can, uint32_t messageId, bool extendedID, uint32_t channel, uint32_t filterMask,  uint8_t * src, uint8_t len);
uint32_t REECAN_trx(REECAN_t * can, uint32_t messageId, bool extendedID, uint32_t channel, uint32_t filterMask,  uint8_t * src, uint8_t len);

/*
	@brief	Execute a special IO action on the CAN bus
	@note	For now those function are always blocking (upto REECAN_RTOS_TIMEOUT_MS)
	@usage

	@param	REECAN_t * can						CAN virtual peripheral handle
	@param	REECAN_ioctl_t * args				IO action arguments
	@return	REECAN_err_t status					REECAN_success if everything went well
												REECAN_null if $can or $args is NULL
												REECAN_param is invalid parameters
												REECAN_fail if generic error
*/
REECAN_err_t REECAN_ioctl(REECAN_t * can, REECAN_ioctl_t * args);

/*
	@brief	Attach/Detach a function to a specific hook on the I2C bus
	@note	NOT IMPLEMENT
	@usage

	@param	REECAN_t * can						CAN virtual peripheral handle
	@param	REECAN_hook_t * hook				Hook type to attach/detach
	@return	REECAN_err_t status					REECAN_success if everything went well
												REECAN_null if $i2c or $args is NULL
												REECAN_param is invalid parameters
												REECAN_fail if generic error
*/
REECAN_err_t REECAN_hook(REECAN_t * can, REECAN_hook_t * hook);
REECAN_err_t REECAN_unhook(REECAN_t * can, REECAN_hook_t * hook);
uint8_t REECAN_getRxErrorCount(REECAN_t * can);


#ifdef __cplusplus
}
#endif
#endif	/* REECAN_H_ */

