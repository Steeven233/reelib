/*
	\name		REEPER.h
	\author		Laurence DV
	\date		2018-09-16
	\version	0.3.0
	\brief		RealEE Provisioning & Emergency Recovery interface

	\note		
	\license	All right reserved RealEE inc. (2018)
*/

/* PROTOCOL:


*/



#ifndef REEPER_H_
#define	REEPER_H_	1

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <cmsis_os2.h>
#include <hardware.h>

#include <em_device.h>		/* for HW module addresses */
#include <em_core.h>		/* for ATOMIC operation */
#include <em_gpio.h>		/* for configuring IOs */
#include <em_usart.h>		/* for actual communication */

#include <REEUART.h>		/* For UART com */
#include <REEBUG.h>

/* -------------------------------- +
|									|
|	Config							|
|	MUST BE SET						|
+ -------------------------------- */
/* == EDIT BELOW == */
/* \/\/\/\/\/\/\/\/ */
/* Supported Communication */
#define	REEPER_COM_SERIAL				(1)
#define	REEPER_COM						REEPER_COM_SERIAL		/* com type used, use only REEPER_COM_xxxx definition */
#define	REEPER_INTERBYTETIMEOUT_MS		(5000)					/* Timeout between each byte of the com complete frame */
#define	REEPER_BITRATE					(115200)				/* Bitrate of any serial interface */

#define	REEPER_THREADMAXPERIOD_MS		(100)					/*	Maximum period to wait for signals from other threads
																(will force check after that and go back to sleep) */
/* /\/\/\/\/\/\/\/\ */
/* == EDIT ABOVE == */



/* -------------------------------- +
|									|
|	HW Config						|
|	MUST BE SET						|
+ -------------------------------- */
/* == EDIT BELOW == */
/* \/\/\/\/\/\/\/\/ */
//#define	REEPER_DEV				(PEM_REEPER_DEV)			/* HW usart's registers address */
//#define	REEPER_CLK				(PEM_REEPER_CLK)			/* CMU clock ID */
//#define	REEPER_IRQTX			(PEM_REEPER_IRQTX)
//#define	REEPER_IRQRX			(PEM_REEPER_IRQRX)
//#define	REEPER_LOCTX			(PEM_REEPER_LOCTX)
//#define	REEPER_LOCRX			(PEM_REEPER_LOCRX)
//#define	REEPER_LOC				()					/* valid for old gecko (without multiple tx/rx location) */
//
//#define	REEPER_TX_PIN			(IOM_REEPER_TX_PIN)
//#define	REEPER_TX_PORT			(IOM_REEPER_TX_PORT)
//#define	REEPER_RX_PIN			(IOM_REEPER_RX_PIN)
//#define	REEPER_RX_PORT			(IOM_REEPER_RX_PORT)
/* /\/\/\/\/\/\/\/\ */
/* == EDIT ABOVE == */


/* -------------------------------- +
|									|
|	Supported Command				|
|									|
+ -------------------------------- */
#define	REEPER_CMD_PROTOCOLHELP	('?')
#define	REEPER_CMD_READ			('R')
#define	REEPER_CMD_WRITE		('W')
#define	REEPER_CMD_EXECUTE		('E')

#define	REEPER_PARAM_HELP		('H')
#define	REEPER_PARAM_REBOOT		('B')
#define	REEPER_PARAM_MEMORY		('M')
#define	REEPER_PARAM_JSON		('J')
//#define	REEPER_PARAM_FORMAT		('F')

#define	REEPER_RESP_OK			('Y')		/* Response for generic success */
#define	REEPER_RESP_NOTOK		('N')		/* Response for generic failure */
#define	REEPER_RESP_INVALID		('-')		/* Response for invalid command */
#define	REEPER_RESP_CHECKSUM	('C')		/* Response for invalid checksum */

/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */
typedef enum {
	REEPER_sig_poke =	0x01,
}REEPER_sig_t;


/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */
#define	REEPER_PROTOCOL_VER			1
#define	REEPER_PROTOCOL_PAYLOADMAX	1024
#define	REEPER_PROTOCOL_FORMAT		"JSON"


/* -------------------------------- +
|									|
|	Global							|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Thread 							|
|									|
+ -------------------------------- */
osThreadId_t REEPER_threadId;

/*
	\brief	REEPER interface thread
	\note	will call REEPER_init() at boot
 	\param	void const *argument		nothing to do with this paramter
	\return	void
*/
void REEPER_thread(void const *argument);


/* -------------------------------- +
|									|
|	Direct API						|
|									|
+ -------------------------------- */
/*
	\brief	Configure and initialize all the necessary peripherals
	\note	
	\param	void
	\return	void
*/
void REEPER_init(void);


void * REEPER_GetComHandle(void);

#ifdef __cplusplus
}
#endif
#endif	/* REEPER_H_ */
