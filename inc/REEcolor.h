/*
	@name		REEcolor.h
	@author		Laurence DV
	@version	0.2.0
	@brief		Color Palettes and color types for all kind of purposes
	@note		
	@license	SEE $REElib_root/README.md
*/


#ifndef __REECOLOR_H_
#define	__REECOLOR_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>
#include <stddef.h>
#include <conf.h>


/* -------------------------------- +
|									|
|	Color Container					|
|									|
+ -------------------------------- */
/* 8bit RGB color encoding (VGA) */
typedef union {
	uint8_t		all;
	struct {
		uint8_t	blue:3;
		uint8_t	green:2;
		uint8_t	red:3;
	};
}rgb332_t;
#define	RGB332_REDMAX		(0b111)
#define	RGB332_REDMIN		(0)
#define	RGB332_REDBITNB		(3)
#define	RGB332_REDMASK		(0b111)

#define	RGB332_GREENMAX		(0b11)
#define	RGB332_GREENMIN		(0)
#define	RGB332_GREENBITNB	(2)
#define	RGB332_GREENMASK	(0b11)

#define	RGB332_BLUEMAX		(0b111)
#define	RGB332_BLUEMIN		(0)
#define	RGB332_BLUEBITNB	(3)
#define	RGB332_BLUEMASK		(0b111)

/* 16bit RGB color encoding (HiColor) */
typedef union {
	uint16_t	all;
	struct {
		uint16_t	blue:6;
		uint16_t	green:4;
		uint16_t	red:6;
	};
}rgb646_t;
#define	RGB646_REDMAX		(0b111111)
#define	RGB646_REDMIN		(0)
#define	RGB646_REDBITNB		(6)
#define	RGB646_REDMASK		(0b111111)

#define	RGB646_GREENMAX		(0b1111)
#define	RGB646_GREENMIN		(0)
#define	RGB646_GREENBITNB	(4)
#define	RGB646_GREENMASK	(0b1111)

#define	RGB646_BLUEMAX		(0b111111)
#define	RGB646_BLUEMIN		(0)
#define	RGB646_BLUEBITNB	(6)
#define	RGB646_BLUEMASK		(0b111111)


/* 24bit RGB color encoding */
typedef union {
	uint8_t		all[3];
	struct {
		uint8_t		red;
		uint8_t		green;
		uint8_t		blue;
	};
}rgb888_t;
#define	RGB888_REDMAX		(0xFF)
#define	RGB888_REDMIN		(0)
#define	RGB888_REDBITNB		(8)
#define	RGB888_REDMASK		(0xFF)

#define	RGB888_GREENMAX		(0xFF)
#define	RGB888_GREENMIN		(0)
#define	RGB888_GREENBITNB	(8)
#define	RGB888_GREENMASK	(0xFF)

#define	RGB888_BLUEMAX		(0xFF)
#define	RGB888_BLUEMIN		(0)
#define	RGB888_BLUEBITNB	(8)
#define	RGB888_BLUEMASK		(0xFF)

/* 32bit RGBW color encoding */
typedef union {
	uint32_t	all;
	struct {
		uint8_t	red;
		uint8_t	green;
		uint8_t	blue;
		uint8_t	white;
	};
}rgbw8888_t;

#define	RGBW8888_REDMAX		(0xFF)
#define	RGBW8888_REDMIN		(0)
#define	RGBW8888_REDBITNB	(8)
#define	RGBW8888_REDMASK	(0xFF)

#define	RGBW8888_GREENMAX	(0xFF)
#define	RGBW8888_GREENMIN	(0)
#define	RGBW8888_GREENBITNB	(8)
#define	RGBW8888_GREENMASK	(0xFF)

#define	RGBW8888_BLUEMAX	(0xFF)
#define	RGBW8888_BLUEMIN	(0)
#define	RGBW8888_BLUEBITNB	(8)
#define	RGBW8888_BLUEMASK	(0xFF)

#define	RGBW8888_WHITEMAX	(0xFF)
#define	RGBW8888_WHITEMIN	(0)
#define	RGBW8888_WHITEBITNB	(8)
#define	RGBW8888_WHITEMASK	(0xFF)


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
/*
	\brief	Convert an RGB format to another
	\note	If you don't understand how to use those functions, you should not be here anyway...
	\param	rgbXXX_t in				input color to be converted
	\return	rgbXXX_t converted		converted color in the new format
*/
rgb888_t rgb332_to_rgb888(rgb332_t in);
rgb332_t rgb888_to_rgb332(rgb888_t in);

/*
	\brief	Blend two RGB pixel together with a specified ratio
	\note	
	\param	rgb888_t inA,inB		pixels to blend together
			uint8_t ratio			Bleding ratio (0xFF = 100% inA, 0x00 = 100% inB)
	\return	rgb888_t blended		blended pixel
*/
rgb888_t rgb888_blend(rgb888_t * inA, rgb888_t * inB, uint8_t ratio);

/*
	\brief	Apply an alpha level to an RGB pixel
	\note	
	\param	rgb888_t inA			pixels to affect
			uint8_t alpha			Alpha level (0xFF: full opacity, 0x00: full transparency)
	\return	rgb888_t blended		blended pixel
*/
rgb888_t rgb888_alpha(rgb888_t * in, uint8_t alpha);


/* -------------------------------- +
|									|
|	VGA Palette						|
|									|
+ -------------------------------- */
/* Example usages:
	VGA_COLORTYPE myColor = VGApalette[VGA_GREEN]
	fct_setColor(VGApalette[VGA_GREEN])
*/
#define	VGA_COLORNB			(16)
#define	VGA_COLORTYPE		rgb332_t

/* VGA color palette names */
/* Uses only as index of VGApalette[] */
typedef enum {
	VGA_BLACK =		0,
	VGA_MAROON =	1,
	VGA_GREEN =		2,
	VGA_OLIVE =		3,
	VGA_NAVY =		4,
	VGA_PURPLE =	5,
	VGA_TEAL =		6,
	VGA_SILVER =	7,
	VGA_GRAY =		8,
	VGA_RED =		9,
	VGA_LIME =		10,
	VGA_YELLOW =	11,
	VGA_BLUE =		12,
	VGA_FUCHSIA =	13,
	VGA_AQUA =		14,
	VGA_WHITE =		15
}VGAColors_t;

static const VGA_COLORTYPE VGApalette[VGA_COLORNB] = {
	{.red=0,				.green=0,					.blue=0					},	/* Black */
	{.red=RGB332_REDMAX/2,	.green=0,					.blue=0					},	/* Maroon */
	{.red=0,				.green=RGB332_GREENMAX/2,	.blue=0					},	/* Green */
	{.red=RGB332_REDMAX/2,	.green=RGB332_GREENMAX/2,	.blue=0					},	/* Olive */
	{.red=0,				.green=0,					.blue=RGB332_BLUEMAX/2	},	/* Navy */
	{.red=RGB332_REDMAX/2,	.green=0,					.blue=RGB332_BLUEMAX/2	},	/* Purple */
	{.red=0,				.green=RGB332_GREENMAX/2,	.blue=RGB332_BLUEMAX/2	},	/* Teal */
	{.red=RGB332_REDMAX/2,	.green=RGB332_GREENMAX/2,	.blue=RGB332_BLUEMAX/2	},	/* Silver */
	{.red=RGB332_REDMAX/4,	.green=RGB332_GREENMAX/4,	.blue=RGB332_BLUEMAX/4	},	/* Gray */
	{.red=RGB332_REDMAX,	.green=0,					.blue=0					},	/* Red */
	{.red=0,				.green=RGB332_GREENMAX,		.blue=0					},	/* Lime */
	{.red=RGB332_REDMAX,	.green=RGB332_GREENMAX,		.blue=0					},	/* Yellow */
	{.red=0,				.green=0,					.blue=RGB332_BLUEMAX	},	/* Blue */
	{.red=RGB332_REDMAX,	.green=0,					.blue=RGB332_BLUEMAX	},	/* Fuchsia */
	{.red=0,				.green=RGB332_GREENMAX,		.blue=RGB332_BLUEMAX	},	/* Aqua */
	{.red=RGB332_REDMAX,	.green=RGB332_GREENMAX,		.blue=RGB332_BLUEMAX	},	/* White */
};


/* -------------------------------- +
|									|
|	RealColor Palette				|
|	Most used colors				|
+ -------------------------------- */
/* Example usages:
	REE_COLORTYPE myColor = REEpalette[REE_ORANGE]
	fct_setColor(REEpalette[REE_ORANGE])
*/
#define	REE_COLORNB			(17)
#define	REE_COLORTYPE		rgb888_t

/* RealColor palette names */
/* Uses only as index of REEpalette[] */
typedef enum {
	REE_RED =			0,		/* Peak RED */
	REE_VERMILLON =		1,
	REE_ORANGE =		2,		
	REE_AMBER =			3,
	REE_YELLOW =		4,		/* Peak RED+GREEN */
	REE_CHARTREUSE =	5,
	REE_GREEN =			6,		/* Peak GREEN */
	REE_TEAL =			7,		/* Peak BLUE+GREEN */
	REE_BLUE =			8,		/* Peak BLUE */
	REE_VIOLET =		9,
	REE_PURPLE =		10,		/* Peak BLUE+RED */
	REE_MAGENTA =		11,

	REE_BLACK =			12,
	REE_DARKGRAY =		13,
	REE_GRAY =			14,
	REE_LIGHTGRAY =		15,
	REE_WHITE =			16,
}REEColors_t;

static const REE_COLORTYPE REEpalette[REE_COLORNB] = {
	{.red=RGB888_REDMAX,	.green=0,					.blue=0					},		/* Red */
	{.red=RGB888_REDMAX/2,	.green=RGB888_GREENMAX/32,	.blue=0					},		/* Vermillon */
	{.red=RGB888_REDMAX/2,	.green=RGB888_GREENMAX/8,	.blue=0					},		/* Orange */
	{.red=RGB888_REDMAX/2,	.green=RGB888_GREENMAX/4,	.blue=0					},		/* Amber */
	{.red=RGB888_REDMAX,	.green=RGB888_GREENMAX,		.blue=0					},		/* Yellow */
	{.red=RGB888_REDMAX/4,	.green=RGB888_GREENMAX/2,	.blue=0					},		/* Chartreuse */
	{.red=0,				.green=RGB888_GREENMAX,		.blue=0					},		/* Green */
	{.red=0,				.green=RGB888_GREENMAX,		.blue=RGB888_BLUEMAX	},		/* Teal */
	{.red=0,				.green=0,					.blue=RGB888_BLUEMAX	},		/* Blue */
	{.red=RGB888_REDMAX/4,	.green=RGB888_GREENMAX/8,	.blue=RGB888_BLUEMAX/2	},		/* Violet */
	{.red=RGB888_REDMAX,	.green=0,					.blue=RGB888_BLUEMAX	},		/* Purple */
	{.red=RGB888_REDMAX/2,	.green=0,					.blue=RGB888_BLUEMAX/4	},		/* Magenta */
	{.red=0,				.green=0,					.blue=0					},		/* Black */
	{.red=RGB888_REDMAX/32,	.green=RGB888_GREENMAX/32,	.blue=RGB888_BLUEMAX/32	},		/* Dark Gray */
	{.red=RGB888_REDMAX/16,	.green=RGB888_GREENMAX/16,	.blue=RGB888_BLUEMAX/16	},		/* Gray */
	{.red=RGB888_REDMAX/8,	.green=RGB888_GREENMAX/8,	.blue=RGB888_BLUEMAX/8	},		/* Light Gray */
	{.red=RGB888_REDMAX,	.green=RGB888_GREENMAX,		.blue=RGB888_BLUEMAX	},		/* White */
};


/* -------------------------------- +
|									|
|	Default REElib color type		|
|	Can be overloaded in conf.h		|
+ -------------------------------- */
#ifndef REELIB_COLOR
	#define	REELIB_COLOR 	rgb888_t
#endif
typedef	REELIB_COLOR REEcolor_t;

#ifdef __cplusplus
}
#endif
#endif	/* __REECOLORS_H_ */

