/*
*	\name		REEtype.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		General types
*	\note		
*	\warning	None
*	\license	All right reserved RealEE inc. (2019)
*/

#ifndef __REETYPE_H__
#define __REETYPE_H__

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	STDlib is a trusted ally		|
|	Support and Logistics unit		|
+ -------------------------------- */
#include <stdint.h>
#include <stdbool.h>
#include <float.h>


/* -------------------------------- +
|									|
|	Data Type						|
|	Endurance & Shock unit			|
+ -------------------------------- */
/* == Generic return code == */
/* Useful function status return and generic error codes */
typedef enum REEcode_e{
	/* Positive number are reserved for usage specific return code */
	REEcode_success	=	0,
	REEcode_fail =		-1,
	REEcode_param =		-127,
	REEcode_null =		-128,
}REEcode_t;
/* ========================= */

/* == Generic data type list == */
typedef enum REEtype_e{
	REEtype_None =	0x00,		/* void */
	
	REE_type_U8 =	0x11,		/* uint8_t */
	REE_type_U16 =	0x12,		/* uint16_t */
	REE_type_U24 =	0x13,		/* uint24_t */
	REE_type_U32 =	0x14,		/* uint32_t */
	REE_type_U64 =	0x18,		/* uint64_t */

	REE_type_S8 =	0x21,		/* int8_t */
	REE_type_S16 =	0x22,		/* int16_t */
	REE_type_S24 =	0x23,		/* int24_t */
	REE_type_S32 =	0x24,		/* int32_t */
	REE_type_S64 =	0x28,		/* int64_t */

	REE_type_F32 =	0x44,		/* float */
	REE_type_F64 =	0x48,		/* double */

	REE_type_BOOL =	0x81,		/* bool */
}REEtype_t;
#define REETYPE_SIZEOF(x)		(((REE_dataType_t)(x)) & 0x0F)
#define REETYPE_ISUNSIGNED(x)	(((REE_dataType_t)(x)) & 0x10)
#define REETYPE_ISSIGNED(x)		(((REE_dataType_t)(x)) & 0x20)
#define REETYPE_ISFLOAT(x)		(((REE_dataType_t)(x)) & 0x40)
#define REETYPE_ISBOOLEAN(x)	(((REE_dataType_t)(x)) & 0x80)
/* ============================ */


/* -------------------------------- +
|									|
|	Function Type					|
|	Mobile unit (light & heavy)		|
+ -------------------------------- */
/* Generic callback function */
typedef void(*REEcb_t)(void *);


#ifdef __cplusplus
}
#endif
#endif

