/*
	@name		REEservo.h
	@author		Laurence DV
	@version	1.0.0
	@brief		Baremetal Servo driving with hw timer
	@note		REEservo is not implemented, only dumped here to cleanup template_main
	@license	SEE $REElib_root/README.md
*/
#ifndef __REESERVO_H__
#define __REESERVO_H__	1
#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include <REEtype.h>					/* General Type, ready to serve! */
#include <REEutil.h>					/* For bit def and util */


/* -------------------------------- +
|									|
|	Public Type						|
|									|
+ -------------------------------- */
/* servo object state definition */
typedef enum {
	REEservo_unknown =	0x00,
	REEservo_started =	0x01,
	REEservo_stoped =	0x02,
	REEservo_moving =	0x03,
	REEservo_error =	0xFF,
}REEservoState_t;

/* servo object definition */
/* uDeg precision because why not use the entire 32bit god fucken dammit, more than that would be just stupid... */
typedef struct {
	REEservoState_t		state;		/* FSM */
	uint8_t				pin;		/* encoded with REEIO */
	TIMER_TypeDef *		timer;		/* hw timer driving this servo */
	CMU_Clock_TypeDef	clk;		/* hw timer clock */
	uint8_t				cc;			/* compare/capture channel used for this servo */
	uint8_t				loc_cc;		/* location of the cc channel */
	int32_t				max_uDeg;	/* absolute maximum rotation angle (360 000 000 for continuous) */
	int32_t				home_uDeg;	/* initial position of the servo at POR */
	int32_t				min_uDeg;	/* absolute minimum rotation angle (0 for continuous) */
	int32_t				cur_uDeg;	/* current position */
	int32_t				cmd_uDeg;	/* current target angle */
}REEservo_t;


/* -------------------------------- +
|	!WARNING!						|
|	Default Config					|
|	Define any of these in conf.h,	|
|	If not defined, these default 	|
|	will be used instead			|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Public Constant					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Public Global					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Public Bare metal API			|
|									|
+ -------------------------------- */
void REEservo_start(REEservo_t * servo);
void REEservo_stop(REEservo_t * servo);
void REEservo_irq(void);
void REEservo_setAngle(REEservo_t * servo, int32_t newAngle);
int32_t REEservo_getAngle(REEservo_t * servo);
REEservoState_t REEservo_getState(REEservo_t * servo);


/* -------------------------------- +
|									|
|	Public ISR						|
|	(wtf would be a private one...)	|
+ -------------------------------- */


#ifdef __cplusplus
}
#endif
#endif	/* __REESERVO_H__ */

