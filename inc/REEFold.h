/*
	\name		REEF.h
	\author		Laurence DV
	\date		2018-08-15
	\version	0.3.0
	\brief		REEF Human Interface, enable simple human interaction within an CMSIS RTOS
				through momentary push-button, LEDs, RGB LEDs and SPI OLED matrix display

	\note		The IDs (ie RGBID, LEDID, etc) are the same as the table index of each device in your REEFpins definition
				The RGBSTR can be used with less than 2 pixel but it will use time and memory like there were 2 pixel.
				This thread will create it's own internal refresh signal (see REEF_REFRESHRATE_HZ),
				no need to send signals if you only want to use the services of this thread (display stuff, react to buttons, etc)
	\license	All right reserved RealEE inc. (2018)
*/


/*	Button press usage
	    		___________   _________
			___/                       \________
triggers	   |       |         |     |
rise:		   c       |         |     |
fall:		           |         |     c
short:		   ^-------c         |						^ = trigger
long:		   ^-----------   ---c						c = callback execution
periodic:	   ^---c---c---   ---c---c					- = time counting

Register your callback to the specified trigger type (multiples possible) and will be called when the
trigger conditions are present. Periodic callback are called at regular interval until the button is
released mechanically.

The periodic is useful in a "flooding" use case or when you need to ensure something is kept running

*/

#ifndef REEF_H_
#define	REEF_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>
#include <cmsis_os2.h>
#include <rtx_os.h>			/* for static tcb */	/* For now it is a dependency, will generalize when cmsis-rtos2 have an easy way for that */
#include <hardware.h>
#include <REEcolor.h>		/* for RGB pixel def & utils */
#include <render.h>			/* for different animation */

#include <em_device.h>		/* for HW module addresses */
#include <em_cmu.h>
#include <em_core.h>		/* for ATOMIC operation */
#include <em_gpio.h>		/* for LED, BTN, RGB, RGBSTR, DISP */
#include <em_timer.h>		/* for RGB */
#include <em_usart.h>		/* for RGBSTR, DISP */
#include <em_ldma.h>		/* for RGBSTR, DISP */


/* -------------------------------- +
|									|
|	Config							|
|	MUST BE SET						|
+ -------------------------------- */
/* == EDIT BELOW == */
/* \/\/\/\/\/\/\/\/ */
#define	REEF_REFRESHRATE_HZ		(30)	/* Refresh rate of the entire interface (keep it >15 for smoothness) */
#define	REEF_CMDTIMEOUT_MS		(50)	/* Timeout when using any of the direct API (in millisec) */
#define	REEF_MUXRATE_HZ			(100)	/* Muxing rate between all RGBs */
#define	REEF_RGBPWMFREQ_HZ		(5000)	/* PWM output to RGBs frequency */

#define	REEF_BTNCBNB			(4)		/* Maximum number of callback per trigger type per button */
#define	REEF_BTNPERIODIC_MS		(1000)	/* Period of the calls of the "periodic callback" (in millisec) */
#define	REEF_BTNSHORTPRESS_MS	(500)	/* Minimum time of a "short press" (in millisec) */
#define	REEF_BTNLONGPRESS_MS	(2000)	/* Minimum time of a "long press" (in millisec) */

#define	REEF_LEDNB				(1)		/* Number of single color LED */
#define	REEF_RGBNB				(2)		/* Number of muxed RGB LED */
#define	REEF_BTNNB				(1)		/* Number of push-button */
#define	REEF_DISPEN				(0)		/* Enable compilation of the OLED SPI Display */
#define	REEF_RGBSTRPIXNB		(10)	/* Number of pixel on the RGB String (0 if not present) */

#define	REEF_RENDERCBNB			(1)		/* Rendering callback queue size */

#define	REEF_LED_ACTIVE			(1)		/* Logic level to activate a LED */
#define	REEF_RGB_ACTIVE			(0)		/* Logic level to activate a color of an RGB LED */
#define	REEF_RGBCOM_ACTIVE		(1)		/* Logic level to activate the common terminal of an RGB LED */
#define	REEF_BTN_ACTIVE			(0)		/* Logic level of a pressed button */
/* /\/\/\/\/\/\/\/\ */
/* == EDIT ABOVE == */

/* -------------------------------- +
|									|
|	HW Config						|
|	MUST BE SET						|
+ -------------------------------- */
/* == EDIT BELOW == */
/* \/\/\/\/\/\/\/\/ */
#define	REEF_RGBSTR_PIN			(4)
#define	REEF_RGBSTR_PORT		(gpioPortC)
#define	REEF_RGBSTR_MODE		(gpioModePushPull)

#define	REEF_TIMER_DEV			(TIMER0)							/* HW timer's registers address 0 */
#define	REEF_TIMER_CLK			(cmuClock_TIMER0)					/* CMU clock ID */
#define	REEF_TIMER_IRQ			(TIMER0_IRQn)
#define	REEF_TIMER_RED_CC		(0)									/* HW timer Compare Channel ID */
#define	REEF_TIMER_RED_CCLOC	(TIMER_ROUTELOC0_CC0LOC_LOC0)		/* Compare Channel Location ID */
#define	REEF_TIMER_GREEN_CC		(1)
#define	REEF_TIMER_GREEN_CCLOC	(TIMER_ROUTELOC0_CC1LOC_LOC0)
#define	REEF_TIMER_BLUE_CC		(2)
#define	REEF_TIMER_BLUE_CCLOC	(TIMER_ROUTELOC0_CC2LOC_LOC0)

#define	REEF_RGBSTRSPI_DEV		(USART3)
#define	REEF_RGBSTRSPI_CLK		(cmuClock_USART3)
#define	REEF_RGBSTRSPI_LOCTX	(USART_ROUTELOC0_TXLOC_LOC2)
#define	REEF_RGBSTRSPI_IRQ		(PEM_RGBSTRINGSPI_IRQ)
#define	REEF_RGBSTRDMA_CH		(1)
/* /\/\/\/\/\/\/\/\ */
/* == EDIT ABOVE == */


typedef struct {
	GPIO_Port_TypeDef port;
	uint8_t pin;
	GPIO_Mode_TypeDef mode;
}REEFio_t;

/* Static pin mapping */
static const struct {
	/* LEDs */
	#if (REEF_LEDNB > 0)
	REEFio_t		LED[REEF_LEDNB];
	#endif
	
	/* RGBs */
	#if (REEF_RGBNB > 0)
	struct {
		REEFio_t	COM[REEF_RGBNB];
		REEFio_t	R;
		REEFio_t	G;
		REEFio_t	B;
	}RGB;
	#endif
	
	/* Buttons */
	#if (REEF_BTNNB > 0)
	REEFio_t		BTN[REEF_BTNNB];
	#endif
	
	/* Display */
	#if REEF_DISPEN == 1
	struct {
		REEFio_t	SCK;
		REEFio_t	MISO;
		REEFio_t	MOSI;
		REEFio_t	CS;
	}DISP;
	#endif
	
	/* RGBString */
	#if (REEF_RGBSTRPIXNB > 0)
	REEFio_t		RGBSTR;
	#endif
}REEFpins = {

/* === EDIT BELOW === */
/* \/--\/--\/--\/--\/ */
	/* LEDs */
	#if (REEF_LEDNB > 0)
//	.LED[0] = { gpioPortK, 1, gpioModePushPull},					/* Only for testing */
	#endif

	/* RGBs */
	#if (REEF_RGBNB > 0)
//	.RGB.R = {IOM_RGBR_PORT, IOM_RGBR_PIN, IOM_RGBR_MODE},
//	.RGB.G = {IOM_RGBG_PORT, IOM_RGBG_PIN, IOM_RGBG_MODE},
//	.RGB.B = {IOM_RGBB_PORT, IOM_RGBB_PIN, IOM_RGBB_MODE},
//	.RGB.COM[0] = {IOM_RGB0A_PORT, IOM_RGB0A_PIN, IOM_RGB0A_MODE},
//	.RGB.COM[1] = {IOM_RGB1A_PORT, IOM_RGB1A_PIN, IOM_RGB1A_MODE},
	#endif

	/* Buttons */
	#if (REEF_BTNNB > 0)
//	.BTN[0] = {IOM_BTN0_PORT, IOM_BTN0_PIN, IOM_BTN0_MODE},
	#endif
	#if (REEF_DISPEN == 1)

	/* Display */
	#endif
	#if (REEF_RGBSTRPIXNB > 0)
	.RGBSTR = {REEF_RGBSTR_PORT, REEF_RGBSTR_PIN, REEF_RGBSTR_MODE},
	/* RGBString */
	#endif
/* /\--/\--/\--/\--/\ */
/* === EDIT ABOVE === */
};


/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */
/* Valid input signal values */
typedef enum {
	REEF_sig_stop =			0x0001,		/* Explicit stop rendering of current frame (NOT IMPLEMENT) */
	REEF_sig_execFrame =	0x0002,		/* Start the rendering of a new frame now */
	REEF_sig_render =		0x0010,		/* Render all effects */
}REEFSignals_t;

/* LEDs mode */
typedef enum {
	REEF_LEDoff =			0x0000,		/* LED is now in Solid OFF state */
	REEF_LEDon = 			0x0001,		/* LED is now in Solid ON state */
	REEF_LEDblink =			0x0010,		/* LED is currently blinking */
}REEFLEDmode_t;

/* RGB mode */
typedef enum {
	REEF_RGBoff =			0x0000,		/* RGB is now in Solid OFF state */
	REEF_RGBon =			0x0001,		/* RGB is now in Solid ON state */
	REEF_RGBsquare =		0x0020,		/* RGB outputs a square wave between colorA & colorB */
	REEF_RGBtriangle =		0x0021,		/* RGB outputs a triangle wave between colorA & colorB */
	REEF_RGBheartBeat =		0x0022,		/* RGB outputs a heart beat partern between colorA */
}REEFRGBmode_t;

/* Buttons triggers */
/* can be ORed together */
typedef enum {
	REEF_BTNpress =			0x01,		/* Button just got pressed */
	REEF_BTNrelease =		0x02,		/* Button just got released */
	REEF_BTNshort =			0x04,		/* Button has been pressed for the "short delay" period of time */
	REEF_BTNlong =			0x08,		/* Button has been pressed for the "long delay" period of time */
	REEF_BTNperiodic =		0x10,		/* Button is currently pressed, called at each period interval */
	REEF_BTNall =			0xFF,		/* Special value to represent all trigger types */
}REEFBTNtrig_t;

typedef rgb888_t REEFrgb_t;				/* RGB type used for all REEF functions */

/* Buttons callback function prototype */
typedef void (*REEFbtnCb_t)(REEFBTNtrig_t);

/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */
#define	REEF_COLOR_BITNB	(8)
#define	REEF_COLOR_MAX		(0xFF)
#define	REEF_COLOR_MIN		(0)


/* -------------------------------- +
|									|
|	Global							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Thread 							|
|									|
+ -------------------------------- */
/*
	\brief	Framed Human Interface control thread
	\note	will call REEF_init() at boot
 	\param	void const *argument		nothing to do with this paramter
	\return	void
*/
void REEF_thread(void *argument);


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
/* ============= */
/* == General == */
/* ============= */
/* Launch the REEF thread in a CMSIS-RTOS2 with static config */
void REEF_startThread(void);

/*
	\brief	Configure and initialize all the necessary peripherals
	\note	
	\param	void
	\return	void
*/
void REEF_init(void);

/* ========= */
/* == BTN == */
/* ========= */
/*
	\brief	Will execute the callback function when the specified press is detected
	\note	
	\param	void
	\return	void
*/
void REEF_BTNregisterCb(uint8_t btnID, REEFBTNtrig_t trigger, REEFbtnCb_t callback);

/* ========= */
/* == LED == */
/* ========= */
/*
	\brief	Change the current mode of the specified LED
	\note	
	\param	void
	\return	void
*/
void REEF_LEDsetMode(uint8_t LEDID, REEFLEDmode_t mode);

/*
	\brief	Change the current period of the specified LED
	\note	The period if the amount of time for the LED to
			complete a cycle of whatever mode is currently set
	\param	uint8_t LEDID		ID of the LED
			int32_t millisec	new period (in millisec)
	\return	int32_t	period		actual period (in millisec)
								INT32_MIN if LEDID invalid
*/
void REEF_LEDsetPeriod(uint8_t LEDID, int32_t millisec);
int32_t REEF_LEDgetPeriod(uint8_t LEDID);

/* ========= */
/* == RGB == */
/* ========= */
/*
	\brief	Force the output of the specified LED or RGB
	\note	Should be avoided at the profit of the more complex functions to simplify usage
	\param	uint8_t RGBID			ID of the affected RGB
			REEFrgb_t color			new color to apply
	\return	void
*/
void REEF_RGBforce(uint8_t RGBID, REEFrgb_t color);

/*
	\brief	Change the current mode of the specified RGB
	\note	
	\param	void
	\return	void
*/
void REEF_RGBsetMode(uint8_t RGBID, REEFRGBmode_t mode);

/*
	\brief	Change the current period of the specified RGB
	\note	The period if the amount of time for the RGB to
			complete a cycle of whatever mode is currently set
	\param	uint8_t RGBID		ID of the RGB
			int32_t millisec	new period (in millisec)
	\return	int32_t	period		actual period (in millisec)
								INT32_MIN if RGBID invalid
*/
void REEF_RGBsetPeriod(uint8_t RGBID, int32_t millisec);
int32_t REEF_RGBgetPeriod(uint8_t RGBID);

/*
	\brief	Set the colors of an RGB
	\note	Color A and B are used in any oscillation mode (ie: square, triangle, etc)
			Use color A when only a single color is enabled in the current RGB mode (ie: on)
	\param	uint8_t RGBID			ID of the affected RGB
			REEFrgb_t color			new color to applp
	\return	void
*/
#define	REEF_RGBsetColor(x)		REEF_RGBsetColorA(x)
void REEF_RGBsetColorA(uint8_t RGBID, REEFrgb_t color);
void REEF_RGBsetColorB(uint8_t RGBID, REEFrgb_t color);


/* ============= */
/* == RGB STR == */
/* ============= */
/*
	\brief	Set the color of a single pixel in the RGB String
	\note	
	\param	uint8_t PIXID			ID of the affected pixel
			REEFrgb_t color			new color to apply
	\return	void
*/
void REEF_RGBSTRsetPixel(uint8_t PIXID, REEFrgb_t color);

/*
	\brief	Fill a continuous section of pixel with a single color
	\note	
	\param	uint8_t startID			ID of the affected first pixel
			uint8_t stopID			ID of the affected last pixel
			REEFrgb_t color			new color to apply
	\return	void
*/
void REEF_RGBSTRfill(uint8_t startID, uint8_t stopID, REEFrgb_t color);

/*
	\brief	Set/Get the global alpha level of the entire RGB string
	\note	
	\param	uint8_t alpha		new Alpha level (ratio on UINT8_MAX)
	\return	uint8_t alpha		current Alpha level
*/
void REEF_RGBSTRsetAlpha(uint8_t alpha);
uint8_t REEF_RGBSTRgetAlpha(void);

/*
	\brief	Add a new VUmeter on the RGB string to be auto rendered
	\note	The .dest element of the handle is overwritten for the correct
			value, no need to set it
	\param	VUmeter_t * handle	VUmeter control struct (see render.h)
	\return	void
*/
void REEF_RGBSTRaddVUmeter(VUmeter_t * handle);


/* -------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */
/*
	\brief	Interrupt Service routine that must be executed on RGB TIMER IRQ
	\note	
	\param	void
	\return	void
*/
void REEF_RGBISR(void);

/*
	\brief	Interrupt Service routine for RGB String must be executed on DMA IRQ
	\note	
	\param	void
	\return	void
*/
void REEF_RGBSTRISR(void);

#ifdef __cplusplus
}
#endif
#endif	/* REEF_H_ */
