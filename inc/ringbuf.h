/*
 * RingBuffer.h
 *
 *	Created on: 2015-02-20
 *	Author:		Laurence DV
 *	Depend:		mxHal.c project specific definition
 *	Note:		Ring Buffer implementation for various uses
 *				Buffer element are octet only and the total number is dynamic.
 */

/*	TIMING ANALYSIS
 * 	Function		|	Octet Nb	|	Tot Time	|	Time/Octet
 * 	=================================================================
 *	rBufBytePush	|	1 Octet		|	2.32s		|	2.32s
 *	=================================================================
 *	rBufBytePull	|	1 Octet		|	2.16s		|	2.16s
 *	=================================================================
 *	rBufArrayPush	|	1 Octet		|	3.52s		|	3.52s
 *					|	2 Octet		|	4.83s		|	2.42s
 *					|	3 Octet		|	6.16s		|	2.05s
 *					|	4 Octet		|	7.32s		|	1.83s
 *					|	5 Octet		|	8.64s		|	1.73s
 *					|	10 Octet	|	14.82s		|	1.48s
 *					|	50 Octet	|	64.40s		|	1.29s
 *					|	100 Octet	|	126.3s		|	1.26s
 *	=================================================================
 *	rBufArrayPull	|	1 Octet		|	3.23s		|	3.23s
 *					|	2 Octet		|	4.55s		|	2.28s
 *					|	3 Octet		|	5.75s		|	1.92s
 *					|	4 Octet		|	6.80s		|	1.70s
 *					|	5 Octet		|	7.99s		|	1.60s
 *					|	10 Octet	|	13.60s		|	1.36s
 *					|	50 Octet	|	58.33s		|	1.17s
 *					|	100 Octet	|	114.3s		|	1.14s
 * All tested with a global source and destination and a buffer of 100 byte
 * with a MCLK of 25MHz valid for commit #dca3318170a25a653ed33e60d09213e28e74161f
 * */

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

/* -------------------------------- +
|	Include							|
+ ---------------------------------*/
#include <stdlib.h>
#include <stdint.h>

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/


/* -------------------------------- +
|	Type							|
+ ---------------------------------*/
typedef struct{
	volatile uint8_t * in;
	volatile uint8_t * out;
	volatile uint8_t * end;
	volatile uint8_t * buffer;
	volatile uint16_t elementNb;
	uint16_t bufferSize;
}rBuf_t;


/* -------------------------------- +
|	Prototype						|
+ ---------------------------------*/
// Create and return a handle for a  new ring buffer of the desired size (in byte)
// Return NULL if there is not enough heap space
rBuf_t * rBufCreate(uint16_t size);

// Destroy and free the allocated memory for a previously allocated ring buffer
void rBufDestroy(rBuf_t * rBuf);

// Flush the entire content of the buffer (elementNb = 0)
void rBufFlush(rBuf_t * rBuf);

// Return the current space available in the buffer (in byte)
uint16_t rBufFreeSpaceGet(rBuf_t * rBuf);

// Return the current space used in the buffer (in byte)
uint16_t rBufUsedSpaceGet(rBuf_t * rBuf);

// Return the maximum space available in this buffer (in byte)
uint16_t rBufTotalSpaceGet(rBuf_t * rBuf);

// Return true if the buffer is full
uint8_t rBufIsFull(rBuf_t * rBuf);

// Return true if the buffer is empty
uint8_t rBufIsEmpty(rBuf_t * rBuf);

// Push a byte into the buffer
// Return the number of byte pushed (max 1)
uint16_t rBufBytePush(rBuf_t * rBuf, uint8_t * srcPtr);

// Pull a byte from the buffer
// Return the number of byte pulled (max 1)
uint8_t rBufBytePull(rBuf_t * rBuf, uint8_t * dstPtr);

// Push an array of $byteNb size to the buffer from the $srcPtr
// Return the number of byte pushed
uint16_t rBufArrayPush(rBuf_t * rBuf, uint8_t * srcPtr, uint16_t byteNb);

// Pull an array of $byteNb size from the buffer to the $dstPtr
// Return the number of byte pulled
uint16_t rBufArrayPull(rBuf_t * rBuf, uint8_t * dstPtr, uint16_t byteNb);

// Concurrent access protected version of the array functions
// Note:	Those function ensure that no less or no more than $byteNb bytes were pushed/pulled.
// 			Disable global interrupt before pushing/pulling and restore it after
uint16_t rBufArrayPushProtected(rBuf_t * rBuf, uint8_t * srcPtr, uint8_t byteNb);
uint16_t rBufArrayPullProtected(rBuf_t * rBuf, uint8_t * dstPtr, uint8_t byteNb);

#endif /* RINGBUFFER_H_ */
