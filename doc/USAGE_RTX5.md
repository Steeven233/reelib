RTX5
====
[RTX5](https://github.com/ARM-software/CMSIS_5) is the RTOS used and [included](/dep/rtx) in this lib.  
All **RTOS Service** are implemented to use CMSIS-RTOS2 has the generic interface, but RTX is currently present and needed because of some
specific features of RTX5, like static tcb.  
Please note that CMSIS-RTOS2 does not provide an easy compiler-time way of differentiating between RTOS implementation.
This could be resolve in the near future, if you have ideas please share!  
  
    
For examples on how to build an RTX5 project check the [example folder](https://github.com/ARM-software/CMSIS_5/tree/develop/CMSIS/RTOS2/RTX/Examples) of the public [CMSIS5 repo](https://github.com/ARM-software/CMSIS_5)

