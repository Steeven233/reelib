/*
	@name		template_header.h
	@author		Laurence DV
	@version	1.0.0
	@brief		template for header files
	@note		
	@license	SEE $REElib_root/LICENSE.md
*/
#ifndef __TEMPLATEHEADER_H__
#define __TEMPLATEHEADER_H__	1
#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <hardware.h>					/* Global hardware configuration file */
#include <conf.h>						/* Global configuration file */
#include <REEtype.h>					/* General Type, ready to serve! */
#include <REEutil.h>					/* For bit def and util */


/* -------------------------------- +
|									|
|	Public Type						|
|									|
+ -------------------------------- */
typedef uint8_t					myU8_t;				/* Example custom basic type */

/* Example custom configuration structure */
typedef struct __attribute__((packed)) myAPI_conf_s {
	volatile uint32_t *			hw;					/* Associated hardware registers */
}myAPI_conf_t;


/* -------------------------------- +
|	!WARNING!						|
|	Default Config					|
|	Define any of these in conf.h,	|
|	If not defined, these default 	|
|	will be used instead			|
+ -------------------------------- */
#define	MYAPI_MYCONF_DEF				(22)	/* Example default value for a custom static configuration */
#define MYAPI_THREAD_EXECRATE_MS_DEF	(100)	/* Execution rate of the example thread (in ms) */


/* -------------------------------- +
|									|
|	Public Constant					|
|									|
+ -------------------------------- */
#define							MYAPI_MYCONST		(22)	/* Example constant value */
static const myU8_t				myConst =			22;		/* Example constant "variable" */
static const myAPI_conf_t		myAPI_conf_def = {
	.hw =		NULL,										/* That's an evil default value :P */
};


/* -------------------------------- +
|									|
|	Public Global					|
|									|
+ -------------------------------- */
static myU8_t myGlobal =		0x22;	/* Example publicly available global variable */


/* -------------------------------- +
|									|
|	Public BareMetal API			|
|									|
+ -------------------------------- */
/*	@brief	Example init function
	@note	Use myAPI_conf_def if you are not sure what to pass
	@param	myAPI_conf_t * conf			Custom configuration struct
	@return	REEcode_t 					REEcode_success if everything went well
										REEcode_null if given NULL pointer
*/
REEcode_t myAPI_Init(const myAPI_conf_t * conf);

/*	@brief	Example function
	@note	This counts how many potato you pass
	@param	myU8_t potatoCnt			Your potato quantity, will be counted
	@return	myU8_t totalCnt				Your correctly counted potatoes
*/
myU8_t myAPI_myFct(myU8_t potatoCnt);		/* Example function public prototype */


/* -------------------------------- +
|									|
|	Public RTOS API					|
|									|
+ -------------------------------- */
/*	@brief	Example abstracted function for RTOS-enabled project
	@note	This create the thread and put it in a ready state
			RTOS should still be initialize by its specific API
	@param	void						No param
	@return	REEcode_t					REEcode_success if everything went well
										REEcode_fail if couldn't create the thread
*/
REEcode_t myAPI_startThread(void);

/*	@brief	Stop the thread used by myAPI
	@note	This will not be soft on thread, it kills it immediately
			use custom mechanism if a smooth stop is required
	@param	void						No param
	@return	REEcode_t					REEcode_success if everything went well
										REEcode_fail if couldn't stop the thread
*/
REEcode_t myAPI_stopThread(void);


/* -------------------------------- +
|									|
|	Public ISR						|
|	(wtf would be a private one...)	|
+ -------------------------------- */
/*	@brief	Example hw Interrupt Service Routine
	@note	The standard pattern used here is custom function doing the specific ISR handling needed,
			but the hardware on which this ISR work is abstracted through the $hwPtr argument
			You should have all IRQ vectors in a project-specific source file like main.c or interrupt.c
	@param	void * hwPtr				Hardware register pointer for handling the ISR
	@return	void						ISR CANNOT RETURN SOMETHING YOU DUMMY
*/
void myAPI_ISR(void * hwPtr);


#ifdef __cplusplus
}
#endif
#endif	/* __TEMPLATEHEADER_H__ */

