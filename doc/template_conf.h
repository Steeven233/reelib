/*
	@name		template_conf.h
	@author		
	@version	1.0.0
	@note		Project specifics Configuration constants and definitions
	@license	SEE $REElib_root/LICENSE.md
*/
#ifndef __TEMPLATE_CONF_H_
#define __TEMPLATE_CONF_H_

#include <hardware.h>


/* -------------------------------- +
|									|
|	CMSIS-RTOS2 config				|
|									|
+ -------------------------------- */
#define	REELIB_USE_CMSISRTOS2				1				/* Define this for using an RTOS in your project */
#ifdef REELIB_USE_CMSISRTOS2
	/* Give access to all files to rtos */
	#include <cmsis_os2.h>		/* for CMSIS-RTOS2 API */
	#include <rtx_os.h>			/* for static mem type */	/* For now it is a dependency, will generalize when cmsis-rtos2 have an easy way for that */
	#include <RTX_Config.h>		/* project specific config should be there */
#endif

/* Enable a breakpoint on rtos error if a debugger is present */
#if defined(DEBUG)
	#define	RTOS_DEBUGBREAK_ON_ERROR		1
#endif


/* -------------------------------- +
|									|
|	REEBUG symbol config overloading|
|									|
+ -------------------------------- */
#if defined(DEBUG) && (defined(_EFM32_PEARL_FAMILY) || defined(_EFM32_JADE_FAMILY) || defined(_EFM32_GIANT_FAMILY) || defined(_EFM32_WONDER_FAMILY))
	#define	REEBUGSWO		1				/* Enable SWO debugging */
#endif

#if defined(DEBUG)
	#define REEBUGIO		1				/* Enable compilation of REEBUG IO functions */
	#define REEBUGIO_D0		IOM_DBGD0
	#define	REEBUGIO_DMASK	(0xF)			/* 4bit wide debug port */
	#define REEBUGIO_CLK	IOM_DBGCLK
#endif


#endif	/* ifndef __TEMPLATE_CONF_H_ */

