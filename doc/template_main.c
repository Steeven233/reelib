/* Special deploy/distrib voodoo */
#ifndef __REELIB_DEV__
	#ifndef __IGNORE_REELIB_TEMPLATES__
	#error "This file is a template for a quick start, use it to create your own main.c file, then delete this file or simply define symbol __IGNORE_REELIB_TEMPLATES__"
	/* Except if you are deving on REElib. In that case, this is the actual main,
	 * which forces you to keep it clean and clear has it is distributed to all to see! */
	#endif
#else

/*
	@name		main.c
	@author		YOU!
	@version	1.0.0
	@brief		YOURS super project using REElib description.
	@note
	@license	YOURS!
*/


/*--------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>
#include <stdbool.h>
#include <hardware.h>
#include <conf.h>

#include <cmsis_os2.h>
#include <rtx_os.h>
#include <REEutil.h>
#include <REEcore.h>
#include <REEtime.h>

#include <em_chip.h>
#include <em_emu.h>
#include <em_cmu.h>
#include <em_ldma.h>
#include <em_gpio.h>
#include <em_prs.h>

#include <REEBUG.h>
#include <REEF.h>
#include <REEFled.h>

#include <template_header.h>


/*--------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */
REEFled_ioConf_t bspLED[2] = {
	{	.pin =			IOM_LED0,
		.activeVal =	true,
		.color.red =	0x80,
		.color.green =	0x20,
		.color.blue =	0x00},
	{	.pin =			IOM_LED1,
		.activeVal =	true,
		.color.red =	0x80,
		.color.green =	0x20,
		.color.blue =	0x00},
};

/*--------------------------------- +
|									|
|	Global 							|
|									|
+ -------------------------------- */
//osRtxInfo_t osRtxInfo;
//uint64_t os_mem[];
//osRtxConfig_t osRtxConfig;
//void *os_isr_queue;

//osRtxThread_t os_idle_thread_cb;
//uint64_t os_idle_thread_stack[];
//osThreadAttr_t os_idle_thread_attr

//osRtxThread_t os_timer_thread_cb;
//uint64_t os_timer_thread_stack[];
//osThreadAttr_t os_timer_thread_attr;
//osRtxMessageQueue_t os_timer_mq_cb;
//uint32_t os_timer_mq_data[];
//osMessageQueueAttr_t os_timer_mq_attr;


/*--------------------------------- +
|									|
|	Prototype						|
|									|
+ -------------------------------- */


/*--------------------------------- +
|									|
|	Thread							|
|									|
+ -------------------------------- */



/*--------------------------------- +
|									|
|	Main							|
|									|
+ -------------------------------- */
int main(void) {

	/* Internal low-level Init */
	CHIP_Init();
//	EMU_DCDCOptimizeSlice(20);
	EMU_DCDCPowerOff();									/* Modif: No immediate switch for no crash on gg11 devkit */
	CMU_HFRCOBandSet(CPU_MAINOSC_FREQ);
	CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFRCO);
	CMU_ClockEnable(cmuClock_LDMA, true);				/* LDMA is used by multiple thingzes u zee! */
	LDMA_Init_t LDMAinit = LDMA_INIT_DEFAULT;
	LDMA_Init( &LDMAinit );

	/* Timing init */
	REEtime_Init(NULL);									/* REEtime default mode is normal RTCC */

	/* Debugging tools Init */
	REEBUGIO_Init();
	REEBUGSWO_Init();

	arm_rfft_init_f32(NULL, NULL, 128, 0, 1);

	/* RTOS Init */
	SystemCoreClockUpdate();
	osKernelInitialize();
	myAPI_startThread();
	REEF_startThread();
	//REEAL_startThread();
	if (osKernelGetState() == osKernelReady) {
		osKernelStart();	/* Here we go! */
	}
	/* After this line, nothing should be executed unless the kernel crashed or has been stopped */

	/* Weeeeeeeee */
	while(1);	/* How many J will I rob from the universe? no body knows! rob rob rob! rob rob rob... */

	return 0;	/* SUSH WARNING */
}


/*--------------------------------- +
|									|
|	Functions						|
|									|
+ -------------------------------- */






/*--------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */
void RTCC_IRQHandler(void) {

	REEtime_RTCC_ISR(RTCC);
}


#endif /* ifndef __REELIB_DEV__ */
