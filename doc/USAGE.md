REElib
====
Usage Guide for the entire API, refer to specific usage guide for more info. Note that the REElib require an [CMSIS RTOS](http://www.keil.com/pack/doc/cmsis/RTOS/html/index.html) present.



General Utils & Macro
----
[REEtype](/inc/REEtype.h) contains generic useful types definition and macros  
[REEfont](/inc/REEfont.h) contains all graphical text related things  
[REEutil](/inc/REEutil.h) contains multiple bit level macro and other generic utilies  
[REEbuf](/inc/REEbuf.h) implements a variety of typical buffers and queue API  
[REEBUG](/inc/REEBUG.h) is a regroupment of debug interfaces and difference exception handler to ease debugging  



Streaming peripherals
----
All of the streaming peripheals (ie: I2C, UART, SPI, etc.) have an unified interface described as follows: 
```C 
REExxxx_err_t REExxxx_take(REExxxx_t * handle);
REExxxx_err_t REExxxx_release(REExxxx_t * handle);

uint32_t REExxxx_tx(REExxxx_t * handle, void * src, uint32_t len);
uint32_t REExxxx_rx(REExxxx_t * handle, void * dst, uint32_t len);
uint32_t REExxxx_trx(REExxxx_t * handle, void * src, void * dst, uint32_t len);
REExxxx_err_t REExxxx_ioctl(REExxxx_t * handle, REExxxx_ioctl_t * args);

REExxxx_err_t REExxxx_hook(REExxxx_t * handle, REExxxx_hook_t * hook, void(*REEcb_t)(void *));
REExxxx_err_t REExxxx_unhook(REExxxx_t * handle, REExxxx_hook_t * hook, void(*REEcb_t)(void *));
```
Where *xxxx* is the specific peripheral (ie REEI2C_err_t).  



RTOS Services
----
[REEAL](/inc/REEAL.h) is an Analog abstraction layer to easily manage Analog Ins and Outs  
[REEF](/inc/REEF.h) is a Human interface capable of handling buttons, LEDs, RGBs, RGB strings and Matrix Display  
[REEPER](/inc/REEPER.h) is a Provisionning and Emergency Recovery streaming protocol  
[REEstart](/inc/REEStart.h) is an energy manager to handle energy mode gracefully  
[REEmember](/inc/REEmember.h) is an project-level operation variable storage and access system  


Dependencies
----
[emlib](/dep/emlib) reason: low-level MCU/SoC peripherals access, hw register definitions  
[RTX5](/dep/rtx) reason: CMSIS-RTOS2 compliant RTOS, uses custom type for static tcb  
[CMSIS-Core](/dep/cmsis) reason: compiler abstraction layer and other generic useful tools  

