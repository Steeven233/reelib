# REElib

General usage C/C++ lib for EFM32 & EFR32 MCUs

## Usage
Refer to [doc/USAGE.md](/doc/USAGE.md) for general information on how to use the REElib or check each specific [USAGE_XXXX.md](/doc) file for more detail on each sub-section.  
See [LICENSE.md](/LICENSE.md) for the legal condition to use this lib.

## Deployment
1. **Download** the branch you want directly on the [web](https://git.megaxone.net/lib/reelib/-/archive/master/reelib-master.zip) or in a *.gitmodules* file:
>[submodule "reelib"]  
>	path = reelib  
>	url = git@git.megaxone.net:lib/reelib.git  
>	branch = master		# arch specific branch coming soon  
2. **Include** [inc](/inc) & all sub dependencies (ie: [dep/emlib/inc](/dep/emlib/inc)) folder in your compiler/assembler/linker/whateverer include path
3. **Compile** [src](/src) & all sub dependencies (ie: [dep/emlib/src](/dep/emlib/src)) folder in your compiler (copy or link to it)
    * An easy way is to simply put REElib root inside your project's compile tree
    * **Currently know problem:** To support all CPU type we must ship all sources, some source files like RTX's [irqs handler](/dep/rtx/src/irq_cm4f.S) or the [startup](/dep/device/src/startup_efm32gg11b.S)/[system](/dep/device/src/system_efm32gg11b.c) code have multiple version depending on the cpu used, for now you must exclude them manually from your compile tree. A solution is being worked on
4. **Copy** the [templates](/doc) ([main](/doc/template_main.c), [conf](/doc/template_conf.h) and [hardware](/doc/template_hardware.h)) for a quick start
	* If a lib require some configuration, there will be a default one in irs header file, check there to know what symbol are required

## Warning
On target with a CM-0+ CPU, you must add an optimisation flag: **-fomit-frame-pointer**

## Dependencies
REElib depends on multiple external libs which are included in the [dep](/dep) folder in source format.  
There is some light modification in some files, those modification are bounded by special tags:
```C
/* MODIF REELIB START */
	modifiedCode();
/* MODIF REELIB STOP */
```
