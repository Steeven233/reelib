/*
	\name		REEBUG.c
	\author		Laurence DV
	\version	1.0.1
	\brief		Debug interfaces and utils
	\note
	\license	All right reserved RealEE inc. (2019)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEBUG.h>
#include "em_dbg.h"
#include "em_gpio.h"
#include <stdio.h>
#include <string.h>

#include <cmsis_os2.h>
#include <rtx_os.h>
#include <RTX_Config.h>


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
#define __REEBUGIO_MAXNBOFPINS		(16)		/* Physical maximum number of pin in a IO ports */
#define	__REEBUGIO_MAXGPIO_MASK		(0xFFFF)	/* Maximum output value with GPIO Port */

#define __RTOS_IDLECNT_UPDATEPERIOD	(1000U)		/* update period (in systick) for the idle counter */
#define	__RTOS_IDLECNT_CALIBPERIOD	(10U)		/* time used for calibration (in systick) */
#define	__RTOS_IDLECNT_CALIBPRIO	(osPriorityHigh)		/* Priority for the idle thread when calibrating idleCnt */
#define __RTOS_IDLE_PRIO			(osPriorityIdle)		/* Normal idle thread priority */

#ifdef REEBUGIO
	#if REEBUGIO_DMASK ==	0x1
		#define	_REEBUGIO_DSIZE	1
	#elif REEBUGIO_DMASK ==	0x3
		#define	_REEBUGIO_DSIZE	2
	#elif REEBUGIO_DMASK ==	0x7
		#define	_REEBUGIO_DSIZE	3
	#elif REEBUGIO_DMASK ==	0xF
		#define	_REEBUGIO_DSIZE	4
	#elif REEBUGIO_DMASK ==	0x1F
		#define	_REEBUGIO_DSIZE	5
	#elif REEBUGIO_DMASK ==	0x3F
		#define	_REEBUGIO_DSIZE	6
	#elif REEBUGIO_DMASK ==	0x7F
		#define	_REEBUGIO_DSIZE	7
	#elif REEBUGIO_DMASK ==	0xFF
		#define	_REEBUGIO_DSIZE	8
	#elif REEBUGIO_DMASK ==	0x1FF
		#define	_REEBUGIO_DSIZE	9
	#elif REEBUGIO_DMASK ==	0x3FF
		#define	_REEBUGIO_DSIZE	10
	#elif REEBUGIO_DMASK ==	0x7FF
		#define	_REEBUGIO_DSIZE	11
	#elif REEBUGIO_DMASK ==	0xFFF
		#define	_REEBUGIO_DSIZE	12
	#elif REEBUGIO_DMASK ==	0x1FFF
		#define	_REEBUGIO_DSIZE	13
	#elif REEBUGIO_DMASK ==	0x3FFF
		#define	_REEBUGIO_DSIZE	14
	#elif REEBUGIO_DMASK ==	0x7FFF
		#define	_REEBUGIO_DSIZE	15
	#elif REEBUGIO_DMASK ==	0xFFFF
		#define	_REEBUGIO_DSIZE	16
	#else
		#error "INVALID REEBUGIO_DMASK"
	#endif
#endif
#if REEBUGIO_SYNC == 1
	#define	__REEBUGIO_SYNC_EN
#endif


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */
static struct {
	osTimerId_t				timer;
	osThreadId_t			idleThreadId;
	bool					calib;					/* Flag set when we are currently calibrating (idle thread is at high prio) */
	bool					calibrated;				/* Flag set when the idle thread have been measured */
	uint32_t				top;
	uint32_t				cnt;
	float					ratio;
}__RTOSidleTime = {
	.calib = false,
	.top = 1,
	.cnt = 0,
	.ratio = 1.0,
};


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */
#ifdef REEBUGIO
	#define _DDPREG								(GPIO->P[REEIO_PORT(REEBUGIO_D0)].DOUT)
	#if defined(_GPIO_P_DOUTCLR_MASK)
		#define _DDPREG_SET(x)					(GPIO->P[REEIO_PORT(REEBUGIO_D0)].DOUTSET = (x))
		#define _DDPREG_CLR(x)					(GPIO->P[REEIO_PORT(REEBUGIO_D0)].DOUTCLR = (x))
		#define _DDPREG_TOG(x)					(GPIO->P[REEIO_PORT(REEBUGIO_D0)].DOUTTGL = (x))
	#else
		#define _DDPREG_SET(x)					(BUS_RegMaskedSet(&GPIO->P[REEIO_PORT(REEBUGIO_D0)].DOUT, (x)))
		#define _DDPREG_CLR(x)					(BUS_RegMaskedClear(&GPIO->P[REEIO_PORT(REEBUGIO_D0)].DOUT, (x)))
		#define _DDPREG_TOG(x)					(GPIO->P[REEIO_PORT(REEBUGIO_D0)].DOUTTGL = (x))
	#endif
#endif

/* Generate a data synchronisation pulse */
#ifdef __REEBUGIO_SYNC_EN
	#define __REEBUGIO_SYNCPULSE()		(GPIO->P[REEIO_PORT(REEBUGIO_CLK)].DOUTTGL = 1<<REEIO_PIN(REEBUGIO_CLK)),\
										(GPIO->P[REEIO_PORT(REEBUGIO_CLK)].DOUTTGL = 1<<REEIO_PIN(REEBUGIO_CLK))
#endif


/* -------------------------------- +
|									|
|	Private Prototypes				|
|									|
+ -------------------------------- */
static void __RTOSidleCntCb(void * args);
static void __RTOSidleCntInit(void);


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */
static void __RTOSidleCntCb(void * args) {
	(void)args;

	/* 0. Calibration done */
	if (__RTOSidleTime.calib) {
		osThreadSetPriority(__RTOSidleTime.idleThreadId, __RTOS_IDLE_PRIO);		/* Reset idle priority for normal operation */
		__RTOSidleTime.top = MAX(__RTOSidleTime.cnt / __RTOS_IDLECNT_CALIBPERIOD, 1U);	/* At least 1 so we never /0 ! */
		__RTOSidleTime.calib = false;
		__RTOSidleTime.calibrated = true;

		/* Restart timer with update period instead */
		//osTimerStop(__RTOSidleTime.timer);
		osTimerStart(__RTOSidleTime.timer, __RTOS_IDLECNT_UPDATEPERIOD);
	/* 1. Measurement period done */
	} else if (__RTOSidleTime.calibrated) {
		__RTOSidleTime.ratio = ((float)__RTOSidleTime.cnt / __RTOS_IDLECNT_UPDATEPERIOD) / ((float)__RTOSidleTime.top);
	}
	__RTOSidleTime.cnt = 0;		/* idleCnt updated, restart the counter */
}

static void __RTOSidleCntInit(void) {
	const osTimerAttr_t __idleCntTimerConf = {
		.name = "IdleTimeCnt"
	};
	osThreadId_t callingThread = osThreadGetId();
	const char * callingThreadName = osThreadGetName(callingThread);

	if (callingThreadName != NULL) {
		/* This must only be called from idle thread */
		if (!strcmp(osThreadGetName(callingThread), OS_IDLE_THREAD_NAME)) {
			__RTOSidleTime.idleThreadId = callingThread;
			osThreadSetPriority(__RTOSidleTime.idleThreadId, __RTOS_IDLECNT_CALIBPRIO);
			__RTOSidleTime.calib = true;

			/* Measure what is the maximum idleCnt */
			__RTOSidleTime.timer = osTimerNew(&__RTOSidleCntCb, osTimerPeriodic, NULL, &__idleCntTimerConf);
			osTimerStart(__RTOSidleTime.timer, __RTOS_IDLECNT_CALIBPERIOD);
		} else {
			REEBUGSWO_String("[ERR]REEBUG_idleCounter must be called from idle thread\n");
		}
	}
}


/* -------------------------------- +
|									|
|	RTOS utilities					|
|									|
+ -------------------------------- */
void REEBUG_idleCounter(void) {
	osThreadId_t callingThread = osThreadGetId();

	/* first we measure */
	if ((__RTOSidleTime.calibrated == false) && (__RTOSidleTime.calib == false)) {
		__RTOSidleCntInit();

	/* then we count */
	} else if (callingThread == __RTOSidleTime.idleThreadId) {
		__RTOSidleTime.cnt++;
	}
}


float REEBUG_idleRatioGet(void) {
	return __RTOSidleTime.ratio;
}


/* -------------------------------- +
|									|
|	Debug IO						|
|									|
+ -------------------------------- */
void REEBUGIO_Init(void) {
	#ifdef REEBUGIO
	/* Sanity check */
	#if REEBUGIO_DMASK > __REEBUGIO_MAXGPIO_MASK
		#error "MXDBG GPIO has too many pins defined"
	#endif

	/* Enable GPIO clock. */
	#if defined(_SILICON_LABS_32B_SERIES_1)
		CMU->HFBUSCLKEN0 |= CMU_HFBUSCLKEN0_GPIO;
	#else
		CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO;
	#endif

	/* Configure all pin as output */
	/* Data pins */
	for (uint8_t i=0; i<_REEBUGIO_DSIZE; i++) {
		GPIO_PinModeSet(REEIO_PORT(REEBUGIO_D0),REEIO_PIN(REEBUGIO_D0+i), gpioModePushPull, 0);
	}
	/* Sync pin */
	#ifdef __REEBUGIO_SYNC_EN
		GPIO_PinModeSet(REEIO_PORT(REEBUGIO_CLK), REEIO_PIN(REEBUGIO_CLK), gpioModePushPull, 0);
	#endif
	/* --------------------------- */

	/* Test all pin */
	for (uint8_t i=0; i<__REEBUGIO_MAXNBOFPINS; i++) {
		REEBUGIO_SET(1<<i);
		#ifdef __REEBUGIO_SYNC_EN
			__REEBUGIO_SYNCPULSE();
		#endif
		REEBUGIO_CLR(1<<i);
	}
	/* ------------ */
	#endif
}

void REEBUGIO_Code(uint32_t code, uint8_t codeWidth) {
	#ifdef REEBUGIO
	for (uint8_t bitSent = 0; bitSent < codeWidth; bitSent += _REEBUGIO_DSIZE) {
		uint32_t temp0 = code & (REEBUGIO_DMASK << bitSent);		/* scan by codeWidth wide steps */
		uint32_t temp1 = temp0 >> bitSent;							/* but keep them aligned to bit0 */

		REEBUGIO_CLR(REEBUGIO_DMASK);	/* Clean slate */
		REEBUGIO_SET(temp1);			/* Apply the new part of the code */

		/* Synchronize */
		#ifdef __REEBUGIO_SYNC_EN
			__REEBUGIO_SYNCPULSE();
		#endif
	}
	#endif
}

void REEBUGIO_SET(uint16_t mask) {
	#ifdef REEBUGIO
	mask &= REEBUGIO_DMASK;
	_DDPREG_SET((mask)<<REEIO_PIN(REEBUGIO_D0));
	#endif
}

void REEBUGIO_CLR(uint16_t mask) {
	#ifdef REEBUGIO
	_DDPREG_CLR((mask)<<REEIO_PIN(REEBUGIO_D0));
	#endif
}

void REEBUGIO_TOG(uint16_t mask) {
	#ifdef REEBUGIO
	_DDPREG_TOG((mask)<<REEIO_PIN(REEBUGIO_D0));
	#endif
}

void REEBUGIO_PLS(uint16_t mask) {
	#ifdef REEBUGIO
	_DDPREG_TOG((mask)<<REEIO_PIN(REEBUGIO_D0));
	_DDPREG_TOG((mask)<<REEIO_PIN(REEBUGIO_D0));
	#endif
}

void REEBUGIO_Array(uint8_t * array, uint32_t len) {
	for (uint32_t byteSent=0; byteSent<len; byteSent++) {
		REEBUGIO_Code(array[byteSent], 8);
	}
}

void REEBUGIO_String(const char * string) {
	uint32_t i = 0;
	while(string[i] != '\0') {
		REEBUGIO_Code(string[i], 8);
		i++;
	}
}


/* -------------------------------- +
|									|
|	SWO	/ SWV						|
|									|
+ -------------------------------- */
void REEBUGSWO_Init(void) {
	#ifdef REEBUGSWO

	if (DBG_Connected()) {
		uint32_t freq;
		uint32_t div;

		CMU_ClockEnable(cmuClock_HFPER, true);
		CMU_ClockEnable(cmuClock_GPIO, true);

		DBG_SWOEnable(REEBUGSWO_LOC);

		/* Enable trace in core debug */
		CoreDebug->DHCSR |= CoreDebug_DHCSR_C_DEBUGEN_Msk;
		CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

		/* Enable PC and IRQ sampling output */
		DWT->CTRL = 0x400113FF;

		/* Set TPIU prescaler for the current debug clock frequency. Target frequency
		is 875 kHz so we choose a divider that gives us the closest match.
		Actual divider is TPI->ACPR + 1. */
		#if (_SILICON_LABS_32B_SERIES < 2)
			freq = CMU_ClockFreqGet(cmuClock_DBG) + (875000 / 2);
		#else //_SILICON_LABS_32B_SERIES
			freq = CMU_ClockFreqGet(cmuClock_TRACECLK) + (875000 / 2);
		#endif //_SILICON_LABS_32B_SERIES
		div  = freq / 875000;
		TPI->ACPR = div - 1;

		/* Set protocol to NRZ */
		TPI->SPPR = 2;

		/* Disable continuous formatting */
		TPI->FFCR = 0x100;

		/* Unlock ITM and output data */
		ITM->LAR = 0xC5ACCE55;
		ITM->TCR = 0x10009;

		/* ITM Channel 0 is used for UART output */
		ITM->TER |= (1UL << 0);
	}

	/* Test the output */
	REEBUGSWO_String("!\r");
	/* --------------- */
	#endif
}

void REEBUGSWO_Code(uint32_t code) {
	#ifdef REEBUGSWO
		ITM_SendChar(code);
	#endif
}

void REEBUGSWO_Array(uint8_t * array, uint32_t len) {
	#ifdef REEBUGSWO
	for (uint32_t byteSent=0; byteSent<len; byteSent++) {
		REEBUGSWO_Code(array[byteSent]);
	}
	#endif
}

void REEBUGSWO_String(const char * string) {
	#ifdef REEBUGSWO
	uint32_t i = 0;
	while(string[i] != '\0') {
		REEBUGSWO_Code(string[i]);
		i++;
	}
	#endif
}


#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
