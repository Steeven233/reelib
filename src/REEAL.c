/*
	\name		REEAS.c
	\author		Laurence DV
	\version	1.0.0
	\note		
	\license	All right reserved RealEE inc. (2018)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"
/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEAL.h>


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
#define	_REEAL_ADCHWFIFO_SIZE		(4)						/* HW FIFO (internal to the ADC block) size (in conversion number) */
#define	_REEAL_ANYSIGNAL			((uint32_t)0x0000)		/* Useful to wait any possible signals */
#define	_REEAL_ALLSIGNAL			((uint32_t)0xFFFF)		/* Useful to mask all possible signals */

#define	_REEAL_VDAC_MAX				(4096)					/* Maximum value code of the VDAC */
#define	_REEAL_VDAC_MIN				(0)						/* Minimum value code of the VDAC */


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */
static struct {
	REEALeventCb_t			eventCb[REEAL_CB_MAXNB];				/* General event cb */
	struct {
		LDMA_Descriptor_t	DMAdesc[2];
		uint8_t				scanId[REEAL_VADC_NB];					/* Holder for each scanId corresponding to an input */
		uint16_t			result[REEAL_VADC_NB];					/* Final calibrated result */
		uint32_t			rawVal[REEAL_VADC_NB];					/* Data reception buffer (from DMA) */
		uint16_t			min[REEAL_VADC_NB];					/* Minimum value, below which we call alarmCb */
		uint16_t			max[REEAL_VADC_NB];					/* Maximum value, above which we call alarmCb */
		float				gainFactor[REEAL_VADC_NB];				/* correction gain to apply to rawValue */
		uint16_t			offSet[REEAL_VADC_NB];					/* correction offset to apply to rawValue */

		REEALioCb_t			cb[REEAL_VADC_NB][REEAL_CB_MAXNB];		/* NOT IMPLEMENTED Callback for each channel */
		REEALalarmCb_t		alarmCb[REEAL_VADC_NB][REEAL_CB_MAXNB];/* NOT IMPLEMENTED Callback for out-of-bound alarms */
	}ADC;

	struct {
		osMutexId_t			mutex;									/* VDAC section access protection mutex */
		uint16_t			rawVal[REEAL_VOUTPUT_NB];				/* Data buffer for outputing at next trigger */
		REEALioCb_t			cb[REEAL_VOUTPUT_NB][REEAL_CB_MAXNB];	/* NOT IMPLEMENTED Callback for each channel */
	}VDAC;

	struct {
		osMutexId_t			mutex;									/* IDAC section access protection mutex */
		uint16_t			rawVal[REEAL_IOUTPUT_NB];				/* Data buffer for outputing at next trigger */
		REEALioCb_t			cb[REEAL_IOUTPUT_NB][REEAL_CB_MAXNB];	/* NOT IMPLEMENTED Callback for each channel */
	}IDAC;

	/* RTOS stuff */
	struct {
		osRtxThread_t			rt_cb;
		osThreadId_t			rt_id;
		const osThreadAttr_t	rt_conf;
	}thread;

}_REEAL = {
	.thread.rt_conf.name =			"REEAL_thread",
	.thread.rt_conf.cb_mem =		&_REEAL.thread.rt_cb,
	.thread.rt_conf.cb_size =		sizeof(_REEAL.thread.rt_cb),
	.thread.rt_conf.priority =		osPriorityNormal,
};


const osMutexAttr_t _REEAL_VDACmutex = {
		"VDAC",
		osMutexRecursive | osMutexPrioInherit | osMutexRobust,
		NULL,
		0,
};
const osMutexAttr_t _REEAL_IDACmutex = {
		"IDAC",
		osMutexRecursive | osMutexPrioInherit | osMutexRobust,
		NULL,
		0,
};

/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */
#define		_REEAL_PRSCH(x)			CONCAT(adcPRSSELCh, x)			/* Ugly macro to get the correct PRS ch within em_adc */
#define		_REEAL_DMAMASK(ch)		(1<< (ch))						/* DMA ch number to DMA ch mask conversion */

#define		_REEAL_MAXINPUTPERGROUP	(8)								/* Maximum number of input per inputGroup */
#define		_REEAL_INPUTGROUPNB		(4)								/* Maximum number of inputGroup per scan sequence */


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */
void _REEAL_setupLDMA(void) {

	LDMA_TransferCfg_t periTransferTx = LDMA_TRANSFER_CFG_PERIPHERAL_LOOP(ldmaPeripheralSignal_ADC0_SCAN, 1);

	/* -- Multiple DMA calls -- */
	#if (REEAL_VADC_NB > _REEAL_ADCHWFIFO_SIZE)
		const uint8_t loopNb = REEMAX(REEAL_VADC_NB / _REEAL_ADCHWFIFO_SIZE, 1);		/* At least 1 loop */
		const uint8_t remainingNb = REEAL_VADC_NB % _REEAL_ADCHWFIFO_SIZE;

		/* Use LINK descriptor macro for initialization and looping */
		LDMA_Descriptor_t xfer[2] = {
			// Number of linked descriptors depends on how many channels needs to read at a time
			// For this example, 8 channels are read per group
			LDMA_DESCRIPTOR_LINKREL_P2M_BYTE(&ADC0->SCANDATAX, &_REEAL.ADC.rawVal, _REEAL_ADCHWFIFO_SIZE, loopNb),
			LDMA_DESCRIPTOR_SINGLE_P2M_BYTE(&ADC0->SCANDATAX, 0, remainingNb),
		};

		_REEAL.ADC.DMAdesc[0] = xfer[0];
		_REEAL.ADC.DMAdesc[1] = xfer[1];

		/* First descriptor to initialize transfer */
		_REEAL.ADC.DMAdesc[0].xfer.doneIfs = 0;
		_REEAL.ADC.DMAdesc[0].xfer.blockSize = ldmaCtrlBlockSizeUnit4;
		_REEAL.ADC.DMAdesc[0].xfer.ignoreSrec = 1;
		_REEAL.ADC.DMAdesc[0].xfer.reqMode = ldmaCtrlReqModeAll;
		_REEAL.ADC.DMAdesc[0].xfer.size = ldmaCtrlSizeWord;

		/* Second descriptor for single descriptor looping */
		_REEAL.ADC.DMAdesc[1].xfer.decLoopCnt = 1;
		_REEAL.ADC.DMAdesc[1].xfer.doneIfs = 1;
		_REEAL.ADC.DMAdesc[1].xfer.blockSize = ldmaCtrlBlockSizeUnit4;
		_REEAL.ADC.DMAdesc[1].xfer.ignoreSrec = 1;
		_REEAL.ADC.DMAdesc[1].xfer.reqMode = ldmaCtrlReqModeAll;
		_REEAL.ADC.DMAdesc[1].xfer.size = ldmaCtrlSizeWord;

		/* Use relative addressing to keep destination address, stop after looping */
		_REEAL.ADC.DMAdesc[1].xfer.dstAddrMode = ldmaCtrlSrcAddrModeRel;
		_REEAL.ADC.DMAdesc[1].xfer.link = 0;
	/* -- Single DMA call ----- */
	#else
		/* Use LINK descriptor macro for initialization and looping */
		LDMA_Descriptor_t xfer[1] = {
			// Number of linked descriptors depends on how many channels needs to read at a time
			// For this example, 8 channels are read per group
			LDMA_DESCRIPTOR_LINKREL_P2M_BYTE(&ADC0->SCANDATAX, &_REEAL.ADC.rawVal, _REEAL_ADCHWFIFO_SIZE, 1),
		};

		_REEAL.ADC.DMAdesc[0] = xfer[0];

		/* First descriptor to initialize transfer */
		_REEAL.ADC.DMAdesc[0].xfer.doneIfs = 1;
		_REEAL.ADC.DMAdesc[0].xfer.blockSize = ldmaCtrlBlockSizeUnit4;
		_REEAL.ADC.DMAdesc[0].xfer.ignoreSrec = 1;
		_REEAL.ADC.DMAdesc[0].xfer.reqMode = ldmaCtrlReqModeAll;
		_REEAL.ADC.DMAdesc[0].xfer.size = ldmaCtrlSizeWord;
	#endif
	/* ------------------------ */

	LDMA_StartTransfer(REEAL_ADC_DMA_CH, (void*)&periTransferTx, (void*)&_REEAL.ADC.DMAdesc);
}

void _REEAL_setupTIMERPRS(void) {
	CMU_ClockEnable(cmuClock_PRS, true);
	CMU_OscillatorEnable(cmuOsc_LFRCO, true, true);		/* Enable LFRCO for CRYOTIMER */
	CMU_ClockEnable(cmuClock_CRYOTIMER, true);

	CRYOTIMER_Init_TypeDef cryotimerInit = CRYOTIMER_INIT_DEFAULT;

	/* Trigger period = 32 x 1/32768 = 1024 Hz */
	#warning "TODO: set correct sample period according to define settings (see AS_DEF_REFRESHRATE)"
	cryotimerInit.enable = true;
	cryotimerInit.period = cryotimerPeriod_32;
	CRYOTIMER_Init(&cryotimerInit);

	/* Output to PRS channel */
	PRS_SourceSignalSet(REEAL_TRIG_PRS_CH, PRS_CH_CTRL_SOURCESEL_CRYOTIMER, PRS_CH_CTRL_SIGSEL_CRYOTIMERPERIOD, prsEdgePos);
}

void _REEAL_setupADC(void) {
	uint8_t i;
	CMU_ClockEnable(REEAL_ADC_CLK, true);

	ADC_Init_TypeDef		ADCinit		= ADC_INIT_DEFAULT;
	ADC_InitScan_TypeDef	scanInit	= ADC_INITSCAN_DEFAULT;

	/* Initialize common settings */
	/* Set ADC clock */
	CMU->ADCCTRL = CMU_ADCCTRL_ADC0CLKSEL_AUXHFRCO;
	ADCinit.timebase = ADC_TimebaseCalc(0);
	ADCinit.prescale = ADC_PrescaleCalc(REEAL_ADC_CONVCLK, 0);
	ADC_Init(REEAL_ADC_DEV, &ADCinit);

	/* Set inputs */
	for (i=0; i<REEAL_VADC_NB; i++) {
		/* Single-ended */
		if (REEALconf.ain[i].neg == adcNegSelVSS) {
			_REEAL.ADC.scanId[i] = ADC_ScanSingleEndedInputAdd(&scanInit, REEALconf.ain[i].group, REEALconf.ain[i].pos);
		/* Differential */
		} else {
			_REEAL.ADC.scanId[i] = ADC_ScanDifferentialInputAdd(&scanInit, REEALconf.ain[i].group, REEALconf.ain[i].pos, REEALconf.ain[i].neg);
		}
	}

	/* Initialize scan conversion */
	scanInit.reference = REEALconf.ADCref;
	scanInit.prsEnable = true;
	scanInit.prsSel = _REEAL_PRSCH(REEAL_TRIG_PRS_CH);
	scanInit.fifoOverwrite = true;
	ADC_InitScan(REEAL_ADC_DEV, &scanInit);

	/* Set DMA trigger level for the HW FIFO */
	REEAL_ADC_DEV->SCANCTRLX |= (_REEAL_ADCHWFIFO_SIZE - 1) << _ADC_SCANCTRLX_DVL_SHIFT;

	REEAL_ADC_DEV->SCANFIFOCLEAR = ADC_SCANFIFOCLEAR_SCANFIFOCLEAR;
	NVIC_ClearPendingIRQ(ADC0_IRQn);
	NVIC_EnableIRQ(ADC0_IRQn);
}

void _REEAL_setupVDAC(void) {

	CMU_ClockEnable(REEAL_VDAC_CLK, true);

	VDAC_Init_TypeDef vdacInit          = VDAC_INIT_DEFAULT;
	VDAC_InitChannel_TypeDef vdacChInit = VDAC_INITCHANNEL_DEFAULT;
	vdacInit.reference = REEALconf.VDACref;
	vdacInit.prescaler = VDAC_PrescaleCalc(REEAL_VDAC_RATE, true, 0);
	VDAC_Init(REEAL_VDAC_DEV, &vdacInit);

	vdacChInit.enable = true;

	/* Input enabling */
	for (uint8_t i=0; i<REEAL_VOUTPUT_NB; i++) {
		/* OUT0 is PA3 */
		if (REEALconf.avout[i].out == 0) {
			VDAC_InitChannel(REEAL_VDAC_DEV, &vdacChInit, 0);
		/* OUT1 is PD14 */
		} else if (REEALconf.avout[i].out == 1) {
			VDAC_InitChannel(REEAL_VDAC_DEV, &vdacChInit, 1);
		}
	}
}

int8_t _REEAL_findAin(REEAL_ain_t ain) {
	uint8_t i;

	for (i=0; i<REEAL_VADC_NB; i++) {
		if ((ain.pos == REEALconf.ain[i].pos) && (ain.neg == REEALconf.ain[i].neg)) {
			return i;
		}
	}
	return -1;
}

int8_t _REEAL_findIndex(uint32_t data) {
	uint8_t i;
	uint16_t scanId = (data & _ADC_SCANDATAX_SCANINPUTID_MASK) >> _ADC_SCANDATAX_SCANINPUTID_SHIFT;

	for (i=0; i<REEAL_VADC_NB; i++) {
		if (scanId == _REEAL.ADC.scanId[i]) {
			return i;
		}
	}
	return -1;
}

/* -------------------------------- +
|									|
|	Thread 							|
|									|
+ -------------------------------- */
void REEAL_thread(void const *argument) {
	(void)argument;			/* remove warning */
	bool run = true;
	uint8_t i;

	/* Init */
	REEAL_init();

	/* Loop */
	while (run) {
		/* Wait for signal */
		osThreadFlagsWait(_REEAL_ALLSIGNAL, osFlagsWaitAny|osFlagsNoClear, osWaitForever);
		REEAL_sig_t receivedSig = (REEAL_sig_t)osThreadFlagsGet();	/* Signals must be cleared, so local copy needed */
		osThreadFlagsClear(_REEAL_ALLSIGNAL);

		/* 1. Decode and process all signals */
		/* ADC updated */
		if (receivedSig & REEAL_sig_adcScan) {
			/* Apply calibration values */
			for (i=0; i<REEAL_VADC_NB; i++) {

				uint8_t id = _REEAL_findIndex(_REEAL.ADC.rawVal[i]);
				if (id>0) {
					uint32_t tempRaw = _REEAL.ADC.rawVal[i] & 0xFFFF;
					_REEAL.ADC.result[id] = (uint16_t)((float)(tempRaw+_REEAL.ADC.offSet[id]) * _REEAL.ADC.gainFactor[id]);
				}
			}

			/* Execute event cb */
			for (i=0; i<REEAL_CB_MAXNB; i++) {
				if (_REEAL.eventCb[i] != NULL) {
					_REEAL.eventCb[i](REEAL_ADCscan);
				}
			}

			/* Execute input cb */
			/* TODO: execute callbacks per channel */
//			for (i=0; i<REEAL_VADC_NB; i++) {
//				for (j=0; i<REEAL_CB_MAXNB; j++) {
//					if (_REEAL.ADC.cb[i][j] != NULL) {
//						(_REEAL.ADC.cb[i][j])(_REEAL.ADC.result[i]);
//					}
//				}
//			}
		}

		/* VDAC trigger */
		if (receivedSig & REEAL_sig_vdacTrig) {

			for (i=0; i<REEAL_VOUTPUT_NB; i++) {
				VDAC_ChannelOutputSet(REEAL_VDAC_DEV, REEALconf.avout[i].out, _REEAL.VDAC.rawVal[i]);
			}
			/* DAC updated, execute all CB */
			/* TODO: execute callbacks per channel */
		}
	}
}


/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */
/* ==== General ==== */
REEAL_err_t REEAL_init(void) {
	uint32_t i,j;

	/* -- Clocks -- */
	CMU_ClockEnable(cmuClock_GPIO, true);	

	/* -- Configure IO -- */
	#warning "TODO@LaurenceDV: add IO init and IO def to REEALconf"
	/* ------------------ */

	/* -- Configure Timer -- */
	_REEAL_setupTIMERPRS();
	/* --------------------- */

	/* -- Configure ADC -- */
	_REEAL_setupADC();
	/* ------------------- */

	/* -- Configure VDAC -- */
	_REEAL_setupVDAC();
	/* -------------------- */

	/* -- Configure DMA -- */
	_REEAL_setupLDMA();
	/* ------------------- */

	/* -- Control struct -- */
	for (i=0; i<REEAL_VADC_NB; i++) {
		_REEAL.ADC.result[i] =		0;
		_REEAL.ADC.rawVal[i] =		0;
		_REEAL.ADC.min[i] =			0;
		_REEAL.ADC.max[i] =			0;
		_REEAL.ADC.gainFactor[i] =	1.0;
		_REEAL.ADC.offSet[i] =		0;

		for (j=0; j<REEAL_CB_MAXNB; j++) {
			_REEAL.ADC.cb[i][j] = 		NULL;
			_REEAL.ADC.alarmCb[i][j] =	NULL;
		}
	}
	for (i=0; i<REEAL_VOUTPUT_NB; i++) {
		_REEAL.VDAC.rawVal[i] = 0;
		for (j=0; j<REEAL_CB_MAXNB; j++) {
			_REEAL.VDAC.cb[i][j] = NULL;
		}
	}
	for (i=0; i<REEAL_IOUTPUT_NB; i++) {
		_REEAL.IDAC.rawVal[i] = 0;
		for (j=0; j<REEAL_CB_MAXNB; j++) {
			_REEAL.IDAC.cb[i][j] = NULL;
		}
	}
	/* -------------------- */

	/* -- RTOS stuff -- */
	_REEAL.VDAC.mutex = osMutexNew(&_REEAL_VDACmutex);
	_REEAL.IDAC.mutex = osMutexNew(&_REEAL_IDACmutex);
	/* ---------------- */

	return REEAL_success;
}

void REEAL_sleep(void) {
	/* TODO: ensure this is still needed, if so, prepare to sleep */
}

/* ==== CB registering ==== */
REEAL_err_t REEAL_registerEventCb(REEALeventCb_t newCb) {
	uint8_t i;
	if (newCb != NULL) {
		for (i=0; i<REEAL_CB_MAXNB; i++) {
			if (_REEAL.eventCb[i] == NULL) {
				_REEAL.eventCb[i] = newCb;
				return REEAL_success;
			}
		}
		return REEAL_fail;
	}
	return REEAL_null;
}


REEAL_err_t REEAL_startThread(void) {
	_REEAL.thread.rt_id = osThreadNew(REEAL_thread, NULL, &_REEAL.thread.rt_conf);
	return (_REEAL.thread.rt_id == NULL) ? REEAL_fail : REEAL_success;
}

REEAL_err_t REEAL_stopThread(void) {
	osStatus_t status = osThreadTerminate(_REEAL.thread.rt_id);
	return (status != osOK) ? REEAL_fail : REEAL_success;
}


REEAL_err_t REEAL_registerAinCb(REEALioCb_t newCb, REEAL_ain_t ain) {
	uint8_t i;
	if (newCb != NULL) {
		/* Search id for Input */
		int8_t id = _REEAL_findAin(ain);
		if (id >= 0) {

			/* Append at the first empty slot */
			for (i=0; i<REEAL_CB_MAXNB; i++) {
				if (_REEAL.ADC.cb[id][i] == NULL) {
					_REEAL.ADC.cb[id][i] = newCb;
					return REEAL_success;
				}
			}
		}
		return REEAL_fail;
	}
	return REEAL_null;
}

//* ==== Data access ==== */
REEAL_err_t REEAL_getAIN(uint8_t ain, uint16_t * dstPtr) {
	if (dstPtr != NULL) {
		if (ain <REEAL_VADC_NB) {
			*dstPtr = _REEAL.ADC.result[ain];

			return REEAL_success;
		}
		return REEAL_fail;
	}
	return REEAL_null;
}

REEAL_err_t REEAL_getAIN_mV(uint8_t ain, float * dstPtr) {
	if (dstPtr != NULL) {
		if (ain <REEAL_VADC_NB) {
			*dstPtr = ((float)_REEAL.ADC.result[ain] * REEAL_ADCREF_V) / 4096.0;

			return REEAL_success;
		}
		return REEAL_fail;
	}
	return REEAL_null;
}

REEAL_err_t REEAL_getAIN_raw(uint8_t ain, uint16_t * dstPtr) {
	if (dstPtr != NULL) {
		if (ain <REEAL_VADC_NB) {
			*dstPtr = (uint16_t)(_REEAL.ADC.rawVal[ain] & 0xFFFF);

			return REEAL_success;
		}
		return REEAL_fail;
	}
	return REEAL_null;
}

REEAL_err_t REEAL_setAOUT(uint8_t aout, uint16_t src) {
	if (aout <REEAL_VOUTPUT_NB) {
		if (osOK == osMutexAcquire(_REEAL.VDAC.mutex, REEAL_MUTEX_TIMEOUT_MS)) {
			_REEAL.VDAC.rawVal[aout] = src;
			osMutexRelease(_REEAL.VDAC.mutex);
			return REEAL_success;
		}
		return REEAL_timeout;
	}
	return REEAL_fail;
}

REEAL_err_t REEAL_setAOUT_mV(uint8_t aout, float src) {
	if (aout <REEAL_VOUTPUT_NB) {
		uint16_t tempVal = (uint16_t)(_REEAL_VDAC_MAX * (src / REEAL_VDACREF_V));

		if (osOK == osMutexAcquire(_REEAL.VDAC.mutex, REEAL_MUTEX_TIMEOUT_MS)) {
			_REEAL.VDAC.rawVal[aout] = tempVal;
			osMutexRelease(_REEAL.VDAC.mutex);
			return REEAL_success;
		}
		return REEAL_timeout;
	}
	return REEAL_fail;
}

REEAL_err_t REEAL_getAOUT(uint8_t aout, uint16_t * dstPtr) {
	if (dstPtr != NULL) {
		if (aout <REEAL_VOUTPUT_NB) {
			if (osOK == osMutexAcquire(_REEAL.VDAC.mutex, REEAL_MUTEX_TIMEOUT_MS)) {
				*dstPtr = _REEAL.VDAC.rawVal[aout];
				osMutexRelease(_REEAL.VDAC.mutex);
				return REEAL_success;
			}
			return REEAL_timeout;
		}
		return REEAL_fail;
	}
	return REEAL_null;
}

REEAL_err_t REEAL_getAOUT_mV(uint8_t aout, float * dstPtr) {
	return 0; /* SUSH! */
}


/* -------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */
void REEAL_DMAADCISR(void) {
	LDMA_TransferCfg_t adcScanTx = LDMA_TRANSFER_CFG_PERIPHERAL(ldmaPeripheralSignal_ADC0_SCAN);	/* DMA trigger is ADC SCAN sig */
	LDMA_StartTransfer(REEAL_ADC_DMA_CH, &adcScanTx, (void*)&_REEAL.ADC.DMAdesc);

	osThreadFlagsSet(REEAL_threadId, REEAL_sig_adcScan|REEAL_sig_vdacTrig);
}

#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
