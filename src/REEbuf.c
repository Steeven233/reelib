/*
	\name		REEbuf.c
	\author		Laurence DV
	\version	1.0.0
	\brief		Buffer type, functions and utilities
	\note		
	\license	All right reserved RealEE inc. (2019)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"
/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include "REEbuf.h"



/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Global Variable					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Ring Buffer						|
|									|
+ -------------------------------- */

#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
