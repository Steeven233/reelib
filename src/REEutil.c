/*
 * REEutil.c
 *
 *  Created on: Aug 28, 2019
 *      Author: jlecuyer2
 */


#include <REEutil.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <stdint.h>

uint32_t systickDelta(uint32_t t0){
	uint32_t now = systickGet();
	if(now >= t0){
		return now - t0;
	}else{
		return (UINT32_MAX - t0) +  now;
	}
}


/*https://www.crockford.com/base32.html*/
size_t u64ToBase32_encode(char *ptr, uint64_t number, size_t max_len){

	if(max_len < BASE32_MAX_LEN){
		return 0;
	}else{
		memset(ptr, '0', BASE32_MAX_LEN+1);
	}

	unsigned int offset = BASE32_MAX_LEN;

	do {
		ptr[--offset] = BASE32[number % BASE_32_BASE];
	} while (number /= BASE_32_BASE);

	return BASE32_MAX_LEN;
}


/*SNPRINTF that return inserted character instead of ideal required buffer*/
int snprintf_try(char * ptr, size_t size, char *fmt, ...){
	if(size<1){
		return size;
	}
	int ret;
	va_list argp;
	va_start(argp, fmt);
	ret = vsnprintf(ptr, size, fmt, argp);
	va_end(argp);
	if(ret <1){
		return 0;
	}else{
		return MIN(((uint32_t)((uint32_t)size-1U)), (uint32_t) ret);	/*size -1 because of null ending */
	}
}



uint8_t REESCOPE_TO_U8(uint8_t x, uint8_t x1, uint8_t x2){
	/*Default to 0 for lower bound and div/0	*/
	uint16_t temp=0;

	if (x>=x2){
		temp = UINT8_MAX;

	}else if((x > x1) && (x1 != x2)){
		/*	256 * ((x-x1)/(x2-x1) */
		temp = x-x1;
		temp <<= 8;
		temp /= (x2-x1);
	}
	return (uint8_t) (temp & UINT8_MAX);
}

uint16_t REESCOPE_TO_U16(uint16_t x, uint16_t x1, uint16_t x2){
	/*Default to 0 for lower bound and div/0	*/
	uint32_t temp=0;

	if (x>=x2){
		temp = UINT16_MAX;

	}else if((x > x1) && (x1 != x2)){
		/*	65536 * ((x-x1)/(x2-x1) */
		temp = x-x1;
		temp <<= 16;
		temp /= (x2-x1);
	}
	return (uint16_t) (temp & UINT16_MAX);
}




//#define REESCOPE_TO_U16(x, x1, x2)	 (x<=x1 ? 0 : (x>=x2 ? UINT16_MAX : ( (((uint32_t)((x)-(x1)))<<16)/((x2)-(x1)))))


