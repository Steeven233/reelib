/*
	\name		REEUART.c
	\author		Laurence DV
	\version	1.0.0
	\note
	\license	All right reserved RealEE inc. (2019)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEUART.h>

/* Debug*/
#include <REEBUG.h>

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
#define		_REEUART_THREAD_NONE				(0)

#define 	__REEUART_AUTOFLUSHWHENRELEASE		(1)			/* 1: will automatically flush the current user info when hw is released */


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */
typedef struct {
	const LEUART_TypeDef *	leReg;							/* low-energy hw register 	todo: adapt code for structure changes, not implemented yet*/
	const USART_TypeDef *	reg;							/* hw register */
	osThreadId_t			thread;							/* Last/current thread that used this hw */
	REEUART_t *				trans;							/* Last/current transaction control handle */
	osMutexId_t				mutex;							/* hw protection mutex */
	bool					active;							/* hw activity status */
}__REEUARThw_t;


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */

#ifndef USART_PRESENT
	#define	USART_COUNT 0	/* Temporary definition for correct __REEUARTuser sizing */
#endif

#ifndef UART_PRESENT
	#define UART_COUNT 0	/* Temporary definition for correct __REEUARTuser sizing */
#endif

#ifndef LEUART_PRESENT
	#define LEUART_COUNT 0	/* Temporary definition for correct __REEUARTuser sizing */
#endif

__REEUARThw_t __REEUARTuser[USART_COUNT+UART_COUNT+LEUART_COUNT] = {
	#ifdef USART_PRESENT
		#if USART_COUNT >= 1
			{	.reg =		USART0,
				.leReg =	NULL,
				.thread =	_REEUART_THREAD_NONE,
				.trans =	NULL,
				.mutex =	NULL,
				.active =	false},
		#endif
		#if USART_COUNT >= 2
			{	.reg =		USART1,
				.leReg =	NULL,
				.thread =	_REEUART_THREAD_NONE,
				.trans =	NULL,
				.mutex =	NULL,
				.active =	false},
		#endif
		#if USART_COUNT >= 3
			{	.reg =		USART2,
				.leReg =	NULL,
				.thread =	_REEUART_THREAD_NONE,
				.trans =	NULL,
				.mutex =	NULL,
				.active =	false},
		#endif
		#if USART_COUNT >= 4
			{	.reg =		USART3,
				.leReg =	NULL,
				.thread =	_REEUART_THREAD_NONE,
				.trans =	NULL,
				.mutex =	NULL,
				.active =	false},
		#endif
		#if USART_COUNT >= 5
			{	.reg =		USART4,
				.leReg =	NULL,
				.thread =	_REEUART_THREAD_NONE,
				.trans =	NULL,
				.mutex =	NULL,
				.active =	false},
		#endif
		#if USART_COUNT >= 6
			{	.reg =		USART5,
				.leReg =	NULL,
				.thread =	_REEUART_THREAD_NONE,
				.trans =	NULL,
				.mutex =	NULL,
				.active =	false},
		#endif
	#endif
	#ifdef UART_PRESENT
		#if UART_COUNT >= 1
			{	.reg =		UART0,
				.leReg =	NULL,
				.thread =	_REEUART_THREAD_NONE,
				.trans =	NULL,
				.mutex =	NULL,
				.active =	false},
		#endif
		#if UART_COUNT >= 2
			{	.reg =		UART1,
				.leReg =	NULL,
				.thread =	_REEUART_THREAD_NONE,
				.trans =	NULL,
				.mutex =	NULL,
				.active =	false},
		#endif
	#endif
	#ifdef LEUART_PRESENT
		#if LEUART_COUNT >= 1
			{	.reg = 		NULL,
				.leReg =	LEUART0,
				.thread =	_REEUART_THREAD_NONE,
				.trans =	NULL,
				.mutex =	NULL,
				.active =	false},
		#endif
		#if LEUART_COUNT >= 2
			{	.reg	=	NULL,
				.leReg =	LEUART1,
				.thread =	_REEUART_THREAD_NONE,
				.trans =	NULL,
				.mutex =	NULL,
				.active =	false},
		#endif
	#endif
};

/* Undefine those temporary definition to ensure no influence over the rest of the fw */
#ifndef USART_PRESENT
	#undef USART_COUNT
#endif
#ifndef UART_PRESENT
	#undef UART_COUNT
#endif
#ifndef LEUART_PRESENT
	#undef LEUART_COUNT
#endif

/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */
void _REEUART_FlushBuf(REEUART_t * uart) {
//	uart->rx.buf.in = 0;
//	uart->rx.buf.out = 0;
//	uart->rx.buf.cnt = 0;
//	uart->tx.buf.in = 0;
//	uart->tx.buf.out = 0;
//	uart->tx.buf.cnt = 0;
//	memset((void *)uart->rx.buf.data, 0, sizeof(uart->rx.buf.data));
//	memset((void *)uart->tx.buf.data, 0, sizeof(uart->tx.buf.data));
}



/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */
REEUART_err_t REEUART_take(REEUART_t * uart){
	return 0;	/* SUSH! */
}

REEUART_err_t REEUART_release(REEUART_t * uart){
	return 0;	/* SUSH! */
}

uint32_t REEUART_tx(REEUART_t * uart, void * src, uint32_t len){
	return 0;	/* SUSH! */
}

uint32_t REEUART_rx(REEUART_t * uart, void * dst, uint32_t len){
	return 0;	/* SUSH! */
}

uint32_t REEUART_trx(REEUART_t * uart, void * src, void * dst, uint32_t len){
	return 0;	/* SUSH! */
}

REEUART_err_t REEUART_ioctl(REEUART_t * uart, REEUART_ioctl_t * args){
	return 0;	/* SUSH! */
}

REEUART_err_t REEUART_hook(REEUART_t * uart, REEUART_hook_t * hook, void(*REEcb_t)(void *)){
	return 0;	/* SUSH! */
}

REEUART_err_t REEUART_unhook(REEUART_t * uart, REEUART_hook_t * hook, void(*REEcb_t)(void *)){
	return 0;	/* SUSH! */
}





























REEUART_err_t REEUART_Init(REEUART_t * uart) {
//
//	/* Clocks */
//	CMU_ClockEnable(cmuClock_HFPER, true);
//	CMU_ClockEnable(cmuClock_GPIO, true);
//	CMU_ClockEnable(uart->clk, true);
//
//	/* GPIOs */
//	GPIO_PinModeSet(uart->tx.port, uart->tx.pin, gpioModePushPull, 1);	/* USART is Idle-High */
//	GPIO_PinModeSet(uart->rx.port, uart->rx.pin, gpioModeInput, 0);		/* No pull resistor */
//
//	/* USART */
//	USART_InitAsync_TypeDef init = USART_INITASYNC_DEFAULT;
//	init.enable = usartDisable;
//	init.baudrate = uart->baudRate;
//	USART_InitAsync(uart->dev, &init);
//	#if defined(USART_ROUTEPEN_RXPEN)
//		(uart->dev)->ROUTEPEN = USART_ROUTEPEN_RXPEN | USART_ROUTEPEN_TXPEN;
//		(uart->dev)->ROUTELOC0 = (uart->tx.loc) | (uart->rx.loc);
//	#else
//		(uart->dev)->ROUTE = USART_ROUTE_RXPEN | USART_ROUTE_TXPEN | (uart->portLoc);
//	#endif
//
//	_REEUART_FlushBuf(uart);
//
//	/* Clear previous RX interrupts */
//	USART_IntClear(uart->dev, USART_IF_RXDATAV);
//	NVIC_ClearPendingIRQ(uart->rx.irq);
//
//	/* Enable RX interrupts */
//	USART_IntEnable(uart->dev, USART_IF_RXDATAV);
//	NVIC_EnableIRQ(uart->rx.irq);
//
//	/* Finally enable it */
//	USART_Enable(uart->dev, usartEnable);
//
//	return REEUART_success;
	return 0;	/* SUSH! */
}

uint16_t REEUART_Send(REEUART_t * uart, uint8_t * src, uint16_t len) {
//	uint16_t cnt;
//
//	for (cnt=0; cnt<len; cnt++) {
//		while(!(uart->dev->STATUS & USART_STATUS_TXBL)) {
//			osThreadYield();	// osDelay(1);
//		}
//		uart->dev->TXDATA = (uint32_t)src[cnt];
//	}
//
//	return cnt;
	return 0;	/* SUSH! */
}

uint16_t REEUART_Receive(REEUART_t * uart, uint8_t * dst, uint16_t len) {
//	uint16_t cnt;
//	//CORE_DECLARE_IRQ_STATE;
//	USART_IntEnable(uart->dev, USART_IF_RXDATAV);
//
//	for (cnt=0; cnt<len; cnt++) {
//		//CORE_ENTER_ATOMIC();
//		if (uart->rx.buf.cnt > 0) {
//			dst[cnt] = uart->rx.buf.data[uart->rx.buf.out];
//			uart->rx.buf.out++;
//			if (uart->rx.buf.out == REEUART_RXBUFFER_BYTE) {
//				uart->rx.buf.out = 0;
//			}
//			uart->rx.buf.cnt--;
//			//CORE_EXIT_ATOMIC();
//		} else {
//			//CORE_EXIT_ATOMIC();
//			break;
//		}
//	}
//	return cnt;
	return 0;	/* SUSH! */
}

uint16_t REEUART_GetRxPending(REEUART_t * uart) {
//	uint16_t pending;
//	//CORE_CRITICAL_SECTION(pending = uart->rx.buf.cnt);
//	pending = uart->rx.buf.cnt;
//	return pending;
	return 0;	/* SUSH! */
}


/* -------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */
void REEUART_RXISR(REEUART_t * uart) {
//	/* Handle stoopidity */
//	if (uart == NULL) {
//		return;		/* BANZAI! (I have a feeling I will be here very soon...) */
//	}
//
//	/* Data is waiting in HW FIFO */
//	if (uart->dev->STATUS & USART_STATUS_RXDATAV) {
//		if (uart->rx.buf.cnt < REEUART_RXBUFFER_BYTE) {
//			/* There is room for data in the RX buffer so we store the data. */
//			uart->rx.buf.data[uart->rx.buf.in] = USART_RxDataGet(uart->dev);
//			uart->rx.buf.in++;
//			uart->rx.buf.cnt++;
//			if (uart->rx.buf.cnt == REEUART_RXBUFFER_BYTE) {
//				uart->rx.buf.in = 0;
//			}
//		} else {
//			/* The RX buffer is full so we must wait for the RETARGET_ReadChar()
//			* function to make some more room in the buffer. RX interrupts are
//			* disabled to let the ISR exit. The RX interrupt will be enabled in
//			* RETARGET_ReadChar(). */
//			//USART_IntDisable(uart->dev, USART_IF_RXDATAV);
//
//			/* Discard overflowing bytes */
//			(void)USART_RxDataGet(uart->dev);
//		}
//	}
}

void REEUART_TXISR(REEUART_t * uart) {

}

#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
