/*
	\name		REESPI.c
	\author		JLecuyer
	\version	1.0.0
	\note
	\license	All right reserved RealEE inc. (2019)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REESPI.h>
#include <REEcore.h>		/* core & clock related utilities */

/* hardware */
#include <em_chip.h>		/* Chip's register definitions */
#include <em_cmu.h>
#include <em_gpio.h>

/* firmware */
#include <ringbuf.h>

/* Debug*/
//#include <REEBUG.h>

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
#define	_REESPI_THREAD_NONE				(0)

#define __REESPI_AUTOFLUSHWHENRELEASE		(1)			/* 1: will automatically flush the current user info when hw is released */

/* CMSIS RTOS2 Stuff */
#if defined(REELIB_USE_CMSISRTOS2) && defined(USART_PRESENT)
	osRtxMutex_t __REESPI_mutexMem[USART_COUNT];

	const osMutexAttr_t __REESPIMutexConf[USART_COUNT] = {
		{
			.name 		= "REESPI0_mtx",							/* human readable mutex name */
			.attr_bits 	= osMutexRecursive | osMutexPrioInherit,	/* attr_bits */
			.cb_mem		= &__REESPI_mutexMem[0],
			.cb_size	= sizeof(osRtxMutex_t),
		},

		#if USART_COUNT > 1
		{
			.name 		= "REESPI1_mtx",							/* human readable mutex name */
			.attr_bits 	= osMutexRecursive | osMutexPrioInherit,	/* attr_bits */
			.cb_mem		= &__REESPI_mutexMem[1],
			.cb_size	= sizeof(osRtxMutex_t),
		},
		#endif

		#if USART_COUNT > 2
		{
			.name 		= "REESPI2_mtx",							/* human readable mutex name */
			.attr_bits 	= osMutexRecursive | osMutexPrioInherit,	/* attr_bits */
			.cb_mem		= &__REESPI_mutexMem[2],
			.cb_size	= sizeof(osRtxMutex_t),
		},
		#endif

		#if USART_COUNT > 3
		{
			.name 		= "REESPI3_mtx",							/* human readable mutex name */
			.attr_bits 	= osMutexRecursive | osMutexPrioInherit,	/* attr_bits */
			.cb_mem		= &__REESPI_mutexMem[3],
			.cb_size	= sizeof(osRtxMutex_t),
		},
		#endif

		#if USART_COUNT > 4
		{
			.name 		= "REESPI4_mtx",							/* human readable mutex name */
			.attr_bits 	= osMutexRecursive | osMutexPrioInherit,	/* attr_bits */
			.cb_mem		= &__REESPI_mutexMem[4],
			.cb_size	= sizeof(osRtxMutex_t),
		},
		#endif

		#if USART_COUNT > 5
		{
			.name 		= "REESPI5_mtx",							/* human readable mutex name */
			.attr_bits 	= osMutexRecursive | osMutexPrioInherit,	/* attr_bits */
			.cb_mem		= &__REESPI_mutexMem[5],
			.cb_size	= sizeof(osRtxMutex_t),
		},
		#endif
	};
#endif

/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */
typedef struct {
	uint8_t					id;					/* USART offset identifier (0:USART0, 1:USART1...)*/
	#ifdef USART_PRESENT
	 USART_TypeDef * const 	reg;				/* hw register address 0 */
	#endif
	#ifdef REELIB_USE_CMSISRTOS2
	osThreadId_t			thread;				/* Last/current thread that used this hw */
	REESPI_t *				handle;				/* Actual handle given to owner */
	osMutexId_t				mutex;				/* hw protection mutex */
	#endif

	const CMU_Clock_TypeDef	clk;				/* Peripheral clock control*/
	const IRQn_Type			irqRx;				/* Peripheral IRQ identifier*/
	const IRQn_Type			irqTx;				/* Peripheral IRQ identifier*/

	rBuf_t	*			rxBuf;
	rBuf_t	*			txBuf;
}__REESPIhw_t;


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */
#ifndef USART_PRESENT
	#define	USART_COUNT 0	/* Temporary definition for correct __REESPIuser sizing */
#endif

__REESPIhw_t __REESPI_hardware[USART_COUNT] = {
	#ifdef USART_PRESENT
		#if USART_COUNT >= 1
			{	.id = 0,
				.reg =		USART0,
				#ifdef REELIB_USE_CMSISRTOS2
				.thread =	_REESPI_THREAD_NONE,
				.handle =	NULL,
				.mutex =	NULL,
				#endif
				.clk =		cmuClock_USART0,
				#if defined(__CM0PLUS_REV)
				.irqRx = USART0_IRQn,
				.irqTx = 0,
				#else
				.irqRx = USART0_RX_IRQn,
				.irqTx = USART0_TX_IRQn,
				#endif
			},
		#endif
		#if USART_COUNT >= 2
			{	.id = 1,
				.reg =		USART1,
				#ifdef REELIB_USE_CMSISRTOS2
				.thread =	_REESPI_THREAD_NONE,
				.handle =	NULL,
				.mutex =	NULL,
				#endif
				.clk =		cmuClock_USART1,
				#if defined(__CM0PLUS_REV)
				.irqRx = USART1_IRQn,
				.irqTx = 0,
				#else
				.irqRx = USART1_RX_IRQn,
				.irqTx = USART1_TX_IRQn,
				#endif
			},
		#endif
		#if USART_COUNT >= 3
			{	.id = 2,
				.reg =		USART2,
				#ifdef REELIB_USE_CMSISRTOS2
				.thread =	_REESPI_THREAD_NONE,
				.handle =	NULL,
				.mutex =	NULL,
				#endif
				.clk =		cmuClock_USART2,
				#if defined(__CM0PLUS_REV)
				.irqRx = USART2_IRQn,
				.irqTx = 0,
				#else
				.irqRx = USART2_RX_IRQn,
				.irqTx = USART2_TX_IRQn,
				#endif
			},
		#endif
		#if USART_COUNT >= 4
			{	.id = 3,
				.reg =		USART3,
				#ifdef REELIB_USE_CMSISRTOS2
				.thread =	_REESPI_THREAD_NONE,
				.handle =	NULL,
				.mutex =	NULL,
				#endif
				.clk =		cmuClock_USART3,
				#if defined(__CM0PLUS_REV)
				.irqRx = USART3_IRQn,
				.irqTx = 0,
				#else
				.irqRx = USART3_RX_IRQn,
				.irqTx = USART3_TX_IRQn,
				#endif
			},
		#endif
		#if USART_COUNT >= 5
			{	.id = 4,
				.reg =		USART4,
				#ifdef REELIB_USE_CMSISRTOS2
				.thread =	_REESPI_THREAD_NONE,
				.handle =	NULL,
				.mutex =	NULL,
				#endif
				.clk =		cmuClock_USART4,
				#if defined(__CM0PLUS_REV)
				.irqRx = USART4_IRQn,
				.irqTx = 0,
				#else
				.irqRx = USART4_RX_IRQn,
				.irqTx = USART4_TX_IRQn,
				#endif
			},
		#endif
		#if USART_COUNT >= 6
			{	.id = 5,
				.reg =		USART5,
				#ifdef REELIB_USE_CMSISRTOS2
				.thread =	_REESPI_THREAD_NONE,
				.handle =	NULL,
				.mutex =	NULL,
				#endif
				.clk =		cmuClock_USART5,
				#if defined(__CM0PLUS_REV)
				.irqRx = USART5_IRQn,
				.irqTx = 0,
				#else
				.irqRx = USART5_RX_IRQn,
				.irqTx = USART5_TX_IRQn,
				#endif
			},
		#endif
	#endif
};

/* Undefine those temporary definition to ensure no influence over the rest of the fw */
#ifndef USART_PRESENT
	#undef USART_COUNT
#endif
#ifndef SPI_PRESENT
	#undef SPI_COUNT
#endif

/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */
#define INT_IS_ENABLED(USARTx,	x) ((((USART_TypeDef *) USARTx)->IEN & (x)) != 0)

#define INT_ENABLE(USARTx, 		x) (((USART_TypeDef *) USARTx)->IEN |=  (x))
#define INT_DISABLE(USARTx,		x) (((USART_TypeDef *) USARTx)->IEN &= ~(x))


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */
static __REESPIhw_t * __REESPI_findHardware(REESPI_t * SPI) {
	#ifdef USART_PRESENT
	uint8_t id;
	__REESPIhw_t * hw;
	for (id=0; id<USART_COUNT; id++) {
		hw = &__REESPI_hardware[id];
		if((SPI->spidev) == (hw->reg)){	// do CAN base address match ? same peripheral
			return hw;
		}
	}
	#endif
	return NULL;
}

/* Return true if calling thread is owner of the peripheral at the moment of calling */
static bool __REESPI_isOwner(REESPI_t * SPI) {
	#ifdef USART_PRESENT
	__REESPIhw_t * hw;
	if(SPI != NULL){						// Real Handle
		hw 	= __REESPI_findHardware(SPI);	// Search hardware
		if(hw != NULL){								// Hardware found
			if (hw->handle == SPI){				// Handle match with hardware (legitimately acquired)
				return true;
			}
		}
	}
	#endif
	return false;
}



REESPI_err_t _REESPI_FlushBuf(REESPI_t * SPI) {
	__REESPIhw_t * hw;

	if (SPI == NULL) {
		return REESPI_null;

	}else if(!__REESPI_isOwner(SPI)){
		return REESPI_denied;

	}else{

		hw = __REESPI_findHardware(SPI);
		if(hw == NULL){
			return REESPI_notFound;
		}

		// Disable interrupt to avoid filling tx queue
		hw->reg->IEN &= ~(USART_IEN_TXBL | USART_IEN_RXDATAV);
		hw->reg->CMD |= (USART_CMD_CLEARRX | USART_CMD_CLEARTX);

		if(hw->txBuf != NULL){
			rBufFlush(hw->txBuf);
		}

		if(hw->rxBuf != NULL){
			rBufFlush(hw->rxBuf);
		}
		return REESPI_success;
	}
}




/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */
REESPI_err_t REESPI_open(REESPI_t * SPI){
	#ifdef USART_PRESENT
	__REESPIhw_t * hw 	= NULL;

	/* CAN device pointer must be initiated*/
	if((SPI == NULL) || (SPI->spidev == NULL)) {
		return REESPI_null;
	}

	/* Search for the control structure*/
	hw = __REESPI_findHardware(SPI);
	if(hw == NULL){
		return REESPI_notFound;	//
	}

	if(hw->mutex == NULL){
		hw->mutex = osMutexNew(&__REESPIMutexConf[hw->id]);
	}

	if (osOK != osMutexAcquire(hw->mutex, SPI->maxWaitTime_ms)) {
		return REESPI_timeout;
	}

	/* save user information */
	hw->handle = SPI;
	hw->thread = osThreadGetId();


	/* Buffer allocation*/
	if(SPI->buffer_rx_len == 0){
		hw->rxBuf  = rBufCreate(2);
	}else{
		hw->rxBuf = rBufCreate(SPI->buffer_rx_len);
	}
	if(SPI->buffer_tx_len == 0){
		hw->txBuf  = rBufCreate(2);
	}else{
		hw->txBuf = rBufCreate(SPI->buffer_tx_len);
	}


	/* Clock */
	CMU_ClockEnable(cmuClock_HFPER, true);
	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(hw->clk, true);

	/* Reset the SPI device */
	USART_Enable(hw->reg, usartDisable);
	USART_Reset(hw->reg);


	/*Enable Interrupt*/
	if(hw->irqRx != 0){
		NVIC_EnableIRQ(hw->irqRx);
	}
	if(hw->irqTx != 0){
		NVIC_EnableIRQ(hw->irqTx);
	}

	/* Initialize with default settings and then update fields according to application requirements. */
	USART_InitSync_TypeDef initSync = USART_INITSYNC_DEFAULT;

	/* Initialize USART */
	initSync.master = SPI->master;
	initSync.autoCsEnable = false;			/* Cs will be manually controlled by REESPI*/
	initSync.baudrate = SPI->bitrate;		/* Select baudrate*/
	initSync.clockMode = usartClockMode0;	/* Clock idle low, sample on rising edge */
	initSync.msbf = true;					/* Most significant bit first*/
	USART_InitSync(hw->reg, &initSync);


	/* IO and route*/
	GPIO_Unlock();

	/* Enable I/O and set location */

	hw->reg->ROUTELOC0 = ( hw->reg->ROUTELOC0 & ~(_USART_ROUTELOC0_TXLOC_MASK | _USART_ROUTELOC0_RXLOC_MASK | _USART_ROUTELOC0_CLKLOC_MASK))
							| ((SPI->locationTx << _USART_ROUTELOC0_TXLOC_SHIFT)  & _USART_ROUTELOC0_TXLOC_MASK)
							| ((SPI->locationRx << _USART_ROUTELOC0_RXLOC_SHIFT)  & _USART_ROUTELOC0_RXLOC_MASK)
							| ((SPI->locationSck << _USART_ROUTELOC0_CLKLOC_SHIFT) & _USART_ROUTELOC0_CLKLOC_MASK);
	hw->reg->ROUTEPEN |= USART_ROUTEPEN_RXPEN | USART_ROUTEPEN_TXPEN | USART_ROUTEPEN_CLKPEN;

	if(SPI->master == false){
		hw->reg->ROUTELOC0 =( hw->reg->ROUTELOC0 & ~(_USART_ROUTELOC0_CSLOC_MASK))
							| ((SPI->locationSlaveCS << _USART_ROUTELOC0_CSLOC_SHIFT)  & _USART_ROUTELOC0_CSLOC_MASK);
		hw->reg->ROUTEPEN |= USART_ROUTEPEN_CSPEN;
	}


	GPIO_Mode_TypeDef tx_mode=gpioModeInput, rx_mode=gpioModeInput, clk_mode=gpioModeInput, cs_mode=gpioModeInput;
	if(SPI->master){
		rx_mode = gpioModeInput;
		tx_mode = gpioModePushPull;
		clk_mode= gpioModePushPull;

	}else{	/* Multi-master force swap tx and RX when device is Slave*/
		rx_mode = gpioModePushPull;
		tx_mode = gpioModeInput;
		clk_mode= gpioModeInput;
		cs_mode = gpioModeInputPullFilter;
	}

	switch(hw->id){
		#if USART_COUNT > 0
		case 0:	{
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART0_TX_PORT(SPI->locationTx), 		AF_USART0_TX_PIN(SPI->locationTx), 		tx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART0_RX_PORT(SPI->locationRx), 		AF_USART0_RX_PIN(SPI->locationRx), 		rx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART0_CLK_PORT(SPI->locationSck), 	AF_USART0_CLK_PIN(SPI->locationSck), 	clk_mode, 	0);
			if(SPI->master == false){
				GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART0_CS_PORT(SPI->locationSck), 	AF_USART0_CS_PIN(SPI->locationSck), cs_mode, 1);
			}
			break;
		}
		#endif

		#if USART_COUNT > 1
		case 1:	{
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART1_TX_PORT(SPI->locationTx), 		AF_USART1_TX_PIN(SPI->locationTx), 		tx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART1_RX_PORT(SPI->locationRx), 		AF_USART1_RX_PIN(SPI->locationRx), 		rx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART1_CLK_PORT(SPI->locationSck), 	AF_USART1_CLK_PIN(SPI->locationSck), 	clk_mode, 	0);
			if(SPI->master == false){
				GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART1_CS_PORT(SPI->locationSck), 	AF_USART1_CS_PIN(SPI->locationSck), cs_mode, 1);
			}
			break;
		}
		#endif

		#if USART_COUNT > 2
		case 2:	{
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART2_TX_PORT(SPI->locationTx), 		AF_USART2_TX_PIN(SPI->locationTx), 		tx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART2_RX_PORT(SPI->locationRx), 		AF_USART2_RX_PIN(SPI->locationRx), 		rx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART2_CLK_PORT(SPI->locationSck),	AF_USART2_CLK_PIN(SPI->locationSck), 	clk_mode, 	0);
			if(SPI->master == false){
				GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART2_CS_PORT(SPI->locationSck), 	AF_USART2_CS_PIN(SPI->locationSck), cs_mode, 1);
			}
			break;
		}
		#endif

		#if USART_COUNT > 3
		case 3:	{
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART3_TX_PORT(SPI->locationTx), 		AF_USART3_TX_PIN(SPI->locationTx), 		tx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART3_RX_PORT(SPI->locationRx), 		AF_USART3_RX_PIN(SPI->locationRx), 		rx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART3_CLK_PORT(SPI->locationSck), 	AF_USART3_CLK_PIN(SPI->locationSck), 	clk_mode, 	0);
			if(SPI->master == false){
				GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART3_CS_PORT(SPI->locationSck), 	AF_USART3_CS_PIN(SPI->locationSck), cs_mode, 1);
			}
			break;
		}
		#endif

		#if USART_COUNT > 4
		case 4:	{
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART4_TX_PORT(SPI->locationTx),	 	AF_USART4_TX_PIN(SPI->locationTx), 		tx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART4_RX_PORT(SPI->locationRx), 		AF_USART4_RX_PIN(SPI->locationRx), 		rx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART4_CLK_PORT(SPI->locationSck), 	AF_USART4_CLK_PIN(SPI->locationSck), 	clk_mode, 	0);
			if(SPI->master == false){
				GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART4_CS_PORT(SPI->locationSck), 	AF_USART4_CS_PIN(SPI->locationSck), cs_mode, 1);
			}
			break;
		}
		#endif

		#if USART_COUNT > 5
		case 5:	{
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART5_TX_PORT(SPI->locationTx), 		AF_USART5_TX_PIN(SPI->locationTx), 		tx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART5_RX_PORT(SPI->locationRx), 		AF_USART5_RX_PIN(SPI->locationRx), 		rx_mode, 	0);
			GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART5_CLK_PORT(SPI->locationSck), 	AF_USART5_CLK_PIN(SPI->locationSck), 	clk_mode, 	0);
			if(SPI->master == false){
				GPIO_PinModeSet((GPIO_Port_TypeDef)AF_USART5_CS_PORT(SPI->locationSck), 	AF_USART5_CS_PIN(SPI->locationSck), cs_mode, 1);
			}
			break;
		}
		#endif

		default:
			return REESPI_notFound;
	}

	if(!SPI->master){
		USART_IntClear(hw->reg, USART_IF_RXDATAV);
		INT_ENABLE(hw->reg, USART_IEN_RXDATAV);
	}
	USART_Enable(hw->reg, usartEnable);

	_REESPI_FlushBuf(SPI);

	return REESPI_success;
	#else
		return REECAN_fail;
	#endif
}

REESPI_err_t REESPI_close(REESPI_t * SPI){
	#ifdef USART_PRESENT
	__REESPIhw_t * hw 	= NULL;

	if (SPI == NULL) {
		return REESPI_null;
	}

	if(!__REESPI_isOwner(SPI)){
		return REESPI_denied;
	}

	/* Clean os service */
	hw->handle = NULL;
	hw->thread = _REESPI_THREAD_NONE;

	/* Remove buffer*/
	if(hw->rxBuf  != NULL){
		rBufDestroy(hw->rxBuf);
		hw->rxBuf  = NULL;
	}

	if(hw->txBuf  != NULL){
		rBufDestroy(hw->txBuf);
		hw->txBuf  = NULL;
	}

	/* release control */
	if (osOK != osMutexRelease(hw->mutex)) {
		return REESPI_fail;
	}

	/* Clean USART peripheral */
	USART_Enable(hw->reg, false);							/* Disable peripheral */
	USART_Reset(hw->reg);									/* Reset Peripheral */
	CMU_ClockEnable(hw->clk, false);									/* Turn off peripheral clock */

	return REESPI_success;
	#else
	return REESPI_fail;
	#endif
}


uint32_t REESPI_tx(REESPI_t * SPI, void * src, uint32_t len){
	return REESPI_trx(SPI, src, NULL, len);
}

uint32_t REESPI_rx(REESPI_t * SPI, void * dst, uint32_t len){
	return REESPI_trx(SPI, NULL, dst, len);
}


/* Transmit and received len # of byte
 *
 * in non-blocking, the return value is either:
 * Positive :	number of byte sent
 * Negative :	All byte sent, number of byte received
 *
 * */
uint32_t REESPI_trx(REESPI_t * SPI, void * src, void * dst, uint32_t len){
	uint8_t temp=0;
	uint16_t pushed;
	uint32_t tCount, rCount=0;
	__REESPIhw_t * hw;


	if (SPI == NULL) {
		return 0;
	}

	if(!__REESPI_isOwner(SPI)){
		return 0;
	}


	hw = __REESPI_findHardware(SPI);
	if(hw == NULL){
		return 0;
	}

	/*
	 * Iterate on buffer to transmit
	 * */
	for(tCount=0; tCount<len; ){

		/* Push byte in transmit buffer*/
		if(src != NULL){
			temp = *((uint8_t *)src);
			(uint8_t *)src++;
			pushed = rBufBytePush(hw->txBuf, &temp);

		}else if(SPI->master){
			pushed = rBufBytePush(hw->txBuf, &temp);

		}else{
			pushed = true; //Rx only, fake push  (to increment spi exchange count)
		}

		if(pushed){
			tCount++;
		}else{
			osThreadYield();
		}

		if(!INT_IS_ENABLED(hw->reg, USART_IEN_TXBL)){
			INT_ENABLE(hw->reg, USART_IEN_TXBL);
		}

		if(!INT_IS_ENABLED(hw->reg, USART_IF_RXDATAV)){
			INT_ENABLE(hw->reg, USART_IF_RXDATAV);
		}

		/* Pull byte from receive buffer*/
		if(dst != NULL){
			if(rBufBytePull(hw->rxBuf, &temp) && (rCount < len)){
				*((uint8_t *) dst) = temp;
				(uint8_t *)dst++;
				rCount++;
			}
		}
	}

	/* Wait for last byte reception*/
	if(SPI->non_blocking == false){
		do{
			osThreadYield();
		}while((hw->reg->STATUS & USART_STATUS_TXC) == 0);
	}


	/* Empty rx buffer*/
	if(dst != NULL){
		while(rBufBytePull(hw->rxBuf, &temp) && (rCount < len)){
			*((uint8_t *) dst) = temp;
			(uint8_t *)dst++;
			rCount++;
		}
	}
	return rCount;
}


REESPI_err_t REESPI_ioctl(REESPI_t * SPI, REESPI_ioctl_t * args){

	switch(args->type){

		case REESPI_ioctl_flush:{			/* Clear/flush the slave reply buffer	*/
			return _REESPI_FlushBuf(SPI);
		}

		case REESPI_ioctl_set:{				/* Set the slave reply buffer			*/
			return REESPI_notFound;
		}

		case REESPI_ioctl_append:{			/* Append to the slave reply buffer	*/
			return REESPI_notFound;
		}

		default:{
			return REESPI_notFound;
		}
	}
}

REESPI_err_t REESPI_hook(REESPI_t * SPI, REESPI_hook_t * hook, void(*REEcb_t)(void *)){
	return 0;	/* SUSH! */
}

REESPI_err_t REESPI_unhook(REESPI_t * SPI, REESPI_hook_t * hook, void(*REEcb_t)(void *)){
	return 0;	/* SUSH! */
}


/*
 * Return used space in receive buffer
 * */
uint16_t REESPI_GetRxPending(REESPI_t * SPI) {
	#ifdef USART_PRESENT
		__REESPIhw_t * hw 	= NULL;

		/* CAN device pointer must be initiated*/
		if((SPI == NULL) || (SPI->spidev == NULL)) {
			return 0;
		}

		/* Search for the control structure*/
		hw = __REESPI_findHardware(SPI);
		if(hw == NULL){
			return 0;	//
		}
		return rBufUsedSpaceGet(hw->rxBuf);
	#else
		return 0;
	#endif
}

/*
 * Return available space in transmit buffer
 * */
uint16_t REESPI_GetTxAvailable(REESPI_t * SPI) {
	#ifdef USART_PRESENT
		__REESPIhw_t * hw 	= NULL;

		/* CAN device pointer must be initiated*/
		if((SPI == NULL) || (SPI->spidev == NULL)) {
			return 0;
		}

		/* Search for the control structure*/
		hw = __REESPI_findHardware(SPI);
		if(hw == NULL){
			return 0;	//
		}
		return rBufFreeSpaceGet(hw->txBuf);
	#else
		return 0;
	#endif
}

/* -------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */
void REESPI_ISR(__REESPIhw_t * hw) {
	static uint8_t temp;

	if (hw == NULL){
		return;
	}

	/* TX Buffer Level Interrupt Flag
	 * Set when buffer becomes empty if buffer level is set to 0x0,
	 * or when the number of empty TX buffer elements equals specified	buffer level.	*/
	if((hw->reg->IF & USART_IF_TXBL)&&(hw->reg->IEN & USART_IEN_TXBL)){

		if(hw->txBuf == NULL){
			INT_DISABLE(hw->reg, USART_IEN_TXBL);

		}else if(rBufBytePull(hw->txBuf, &temp)){	/* If there are data to pull*/
			hw->reg->TXDATA = temp;

		}else{
			INT_DISABLE(hw->reg, USART_IEN_TXBL);
		}
	}

	/* RX Data Valid Interrupt Flag
	 * Set when data becomes available in the receive buffer. */
	if((hw->reg->IF & USART_IF_RXDATAV)&&(hw->rxBuf != NULL)){
		temp = (uint8_t) (hw->reg->RXDATA & 0x000000ff);
		rBufBytePush(hw->rxBuf, &temp);
	}

//	if(hw->irqRx != 0){
//		NVIC_ClearPendingIRQ(hw->irqRx);
//	}
//	if(hw->irqTx != 0){
//		NVIC_ClearPendingIRQ(hw->irqTx);
//	}
}


#if USART_COUNT > 0					/* USART0 IRQ Handler */
	void USART0_RX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[0]);}
	void USART0_TX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[0]);}
	void USART0_IRQHandler(void){	REESPI_ISR(&__REESPI_hardware[0]);}
#endif

#if USART_COUNT > 1					/* USART1 IRQ Handler */
	void USART1_RX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[1]);}
	void USART1_TX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[1]);}
	void USART1_IRQHandler(void){	REESPI_ISR(&__REESPI_hardware[1]);}
#endif

#if USART_COUNT > 2					/* USART2 IRQ Handler */
	void USART2_RX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[2]);}
	void USART2_TX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[2]);}
	void USART2_IRQHandler(void){	REESPI_ISR(&__REESPI_hardware[2]);}
#endif

#if USART_COUNT > 3					/* USART3 IRQ Handler */
	void USART3_RX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[3]);}
	void USART3_TX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[3]);}
	void USART3_IRQHandler(void){	REESPI_ISR(&__REESPI_hardware[3]);}
#endif

#if USART_COUNT > 4					/* USART4 IRQ Handler */
	void USART4_RX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[4]);}
	void USART4_TX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[4]);}
	void USART4_IRQHandler(void){	REESPI_ISR(&__REESPI_hardware[4]);}
#endif

#if USART_COUNT > 5					/* USART5 IRQ Handler */
	void USART5_RX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[5]);}
	void USART5_TX_IRQHandler(void){REESPI_ISR(&__REESPI_hardware[5]);}
	void USART5_IRQHandler(void){	REESPI_ISR(&__REESPI_hardware[5]);}

#endif




#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
