/*
	@name		REEtime.c
	@author		Laurence DV
	@version	1.0.0
	@brief		template for source files
	@note
	@license	SEE $REElib_root/LICENSE.md
*/
#pragma GCC diagnostic ignored "-Wunused-function"		/* Disable the warning from unused private function in your lib */
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEtime.h>
#include <REEcore.h>		/* core & clock related utilities */
#include <REEBUG.h>			/* REEBUG fct are empty stub when symbol DEBUG is not defined */

#include <em_cmu.h>			/* Clock access */
#include <em_prs.h>			/* For PRS system clocks */

/* -------------------------------- +
|									|
|	Default config loading			|
|									|
+ -------------------------------- */
#ifndef REETIME_BASEFREQ_HZ
	#define	REETIME_BASEFREQ_HZ				(REETIME_BASEFREQ_HZ_DEF)
#endif
#ifndef REETIME_FREQA_HZ
	#define	REETIME_FREQA_HZ				(REETIME_FREQA_HZ_DEF)
#endif
#ifndef REETIME_FREQB_HZ
	#define	REETIME_FREQB_HZ				(REETIME_FREQB_HZ_DEF)
#endif
#ifndef REETIME_FREQA_PRS
	#define	REETIME_FREQA_PRS				(REETIME_FREQA_PRS_DEF)
#endif
#ifndef REETIME_FREQB_PRS
	#define	REETIME_FREQB_PRS				(REETIME_FREQB_PRS_DEF)
#endif


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Prototype				|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Bare Metal API					|
|									|
+ -------------------------------- */
REEcode_t REEtime_Init(const REEtime_conf_t * conf) {
	/* NULL passed, just use default */
	if ((conf == NULL) || (conf->hw == NULL)) {
		conf = &REEtime_conf_def;
	}

	/* Enable clock sources */
	CMU_ClockEnable(cmuClock_LFE, true);				/* Enable LF clock for RTCC itself */
	CMU_ClockEnable(cmuClock_HFLE, true);				/* Enable HF clock for reg sync */
	CMU_OscillatorEnable(cmuOsc_LFRCO, true, true);		/* a bit better accuracy than ULFRCO */
	CMU_ClockSelectSet(cmuClock_LFE, cmuSelect_LFRCO);
	CMU_ClockEnable(cmuClock_PRS, true);

	/* PRS system clocks */
	PRS->ROUTEPEN |= PRS_ROUTEPEN_CH1PEN|PRS_ROUTEPEN_CH2PEN;
	PRS->ROUTELOC0 = PRS_ROUTELOC0_CH1LOC_LOC1|PRS_ROUTELOC0_CH2LOC_LOC1;
	PRS_SourceSignalSet(REETIME_FREQA_PRS, PRS_CH_CTRL_SOURCESEL_RTCC, PRS_CH_CTRL_SIGSEL_RTCCCCV1, prsEdgeOff);
	PRS_SourceSignalSet(REETIME_FREQB_PRS, PRS_CH_CTRL_SOURCESEL_RTCC, PRS_CH_CTRL_SIGSEL_RTCCCCV2, prsEdgeOff);

	/* Temporary for GG11 devkit */
	CMU_ClockEnable(cmuClock_GPIO, true);
	GPIO_PinModeSet(gpioPortF, 4, gpioModePushPull, 0);
	GPIO_PinModeSet(gpioPortF, 5, gpioModePushPull, 0);

	/* If not started, start it! */
	if (!(conf->hw->CTRL & RTCC_CTRL_ENABLE)) {
		CMU_ClockEnable(REEcore_findCMUClock((void *)conf->hw), true);
		RTCC_CounterSet(0);
		RTCC_PreCounterSet(0);
		RTCC_ChannelCCVSet(0, MAX(1U, CMU_ClockFreqGet(cmuClock_RTCC)/REETIME_BASEFREQ_HZ)-1U);
		RTCC_ChannelCCVSet(1, 0xFFFFFFFF);
		RTCC_ChannelCCVSet(2, 0x00000001);
		RTCC_ChannelInit(0, &conf->cc0_conf);
		RTCC_ChannelInit(1, &conf->cc1_conf);
		RTCC_ChannelInit(2, &conf->cc2_conf);
		RTCC_Init(&conf->hw_conf);
		RTCC_EM4WakeupEnable(true);
		RTCC_IntEnable(RTCC_IEN_OSCFAIL);
	}
	NVIC_EnableIRQ(REEcore_findIRQ((void *)conf->hw));

	return REEcode_success;
}

REEtime_t REEtime_stamp(void) {
	REEtime_t now = {};
	return now;
}


/*--------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */
void REEtime_RTCC_ISR(void * hwPtr) {
	/* ISR should be quick, and normally callen with static param (ex: ISR(TIMER0); ) */
	/* Except if the param passed to the ISR is dynamic, keep the check in that case! */
	#ifdef DEBUG
	if (hwPtr == NULL) {
		REEbreak();
	}
	#endif

	/* Actual ISR code */
	RTCC_IntClear(RTCC_IFC_OSCFAIL);
}

#pragma GCC diagnostic warning "-Wunused-function"		/* Restore the warnings from unused private function in your lib */
#pragma GCC diagnostic warning "-Wunused-parameter"
