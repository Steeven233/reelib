/*
	\name		REEI2C.c
	\author		Laurence DV
	\version	1.0.0
	\note		I2C Peripheral lib, largely inspired by i2cspm driver from SiLabs (see developer/sdks/gecko_sdk_suite/vN.N/hardware/kit/common/drivers)
				Only single-master mode currently implemented
	\usage		There is a global variable "__REEI2Chw_t __REEI2Cuser[I2C_COUNT]" which contain all hw-linked control variables
	\todo		The hw matching at each function call is a bit inefficient.... should realee be improved

	\license	All right reserved RealEE inc. (2019)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEI2C.h>

/* Debug*/
#include <REEBUG.h>
#include <REEutil.h>

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
#define	_REEI2C_RESETSEQ_SCL_CYCLE_NB		(9)			/* Number of High to Low to High cycle in a reset sequence */
#define	_REEI2C_THREAD_NONE					(0)			/* Thread id for no thread... (this sentence is very weird, please help) */

#define __REEI2C_AUTOFLUSHWHENRELEASE		(1)			/* 1: will automatically flush the current user info when hw is released */
#define	__REEI2C_STUPIDDELAYMAX				(1000)		/* This is used as TOP cnt value for spinlock style delays */


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */
typedef struct {
	I2C_TypeDef * const reg;							/* hw register address 0 */
	osThreadId_t		thread;							/* Last/current thread that used this hw */
	REEI2C_t *			handle;							/* Last/current control handle */
	osMutexId_t			mutex;							/* hw protection mutex */
	osTimerId_t			timer;							/* os timer dedicated to handling the hw */
	bool				active;							/* hw activity status */
	const osMutexAttr_t mutexConf;
	const osTimerAttr_t timerConf;
	uint32_t 			irqTick;
	I2C_TransferReturn_TypeDef return_code;
}__REEI2Chw_t;


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */
static void __REEI2C_timeout_cb(__REEI2Chw_t * hw);		/* This prototype need to be before osTimerDef... >< */


#ifdef I2C_PRESENT
	static bool __REEI2Cstarted = false;				/* when TRUE, REEI2C lib has been started and is ready to operate */

	__REEI2Chw_t __REEI2Cuser[I2C_COUNT] = {
		#if I2C_COUNT > 0
			{	.reg=		I2C0,
				.thread=	_REEI2C_THREAD_NONE,
				.handle=	NULL,
				.mutex=		NULL,
				.timer= 	NULL,
				.active=	false,
				.mutexConf= {
					.name = "REEI2C0_mtx",						/* human readable mutex name */
					.attr_bits = osMutexRecursive | osMutexPrioInherit,		/* attr_bits */
					.cb_mem =  NULL,										/* memory for control block */
					.cb_size = 0											/* size for control block */
				},
				.timerConf = {
					.name = "I2C0_tmr",
					.attr_bits = 0,		/* attr_bits */
					.cb_mem =  NULL,										/* memory for control block */
					.cb_size = 0											/* size for control block */
				}
			}
		#endif

		#if I2C_COUNT > 1
			,{	.reg=		I2C1,
				.thread=	_REEI2C_THREAD_NONE,
				.handle=	NULL,
				.mutex=		NULL,
				.timer=		NULL,
				.active=	false,
				.mutexConf= {
					.name = "REEI2C1_mtx",						/* human readable mutex name */
					.attr_bits = osMutexRecursive | osMutexPrioInherit,		/* attr_bits */
					.cb_mem =  NULL,										/* memory for control block */
					.cb_size = 0											/* size for control block */
				},
				.timerConf = {
					.name = "I2C1_tmr",
					.attr_bits = 0,		/* attr_bits */
					.cb_mem =  NULL,										/* memory for control block */
					.cb_size = 0											/* size for control block */
				}
			}
		#endif

		#if I2C_COUNT > 2
			,{	.reg=		I2C2,
				.thread=	_REEI2C_THREAD_NONE,
				.handle=	NULL,
				.mutex=		NULL,
				.timer=		NULL,
				.active=	false,
				.mutexConf= {
					.name = "REEI2C2_mtx",						/* human readable mutex name */
					.attr_bits = osMutexRecursive | osMutexPrioInherit,		/* attr_bits */
					.cb_mem =  NULL,										/* memory for control block */
					.cb_size = 0											/* size for control block */
				},
				.timerConf = {
					.name = "I2C2_tmr",
					.attr_bits = 0,		/* attr_bits */
					.cb_mem =  NULL,										/* memory for control block */
					.cb_size = 0											/* size for control block */
				}
			}
		#endif

		#if I2C_COUNT > 3
			,{	.reg=		I2C3,
				.thread=	_REEI2C_THREAD_NONE,
				.handle=	NULL,
				.mutex=		NULL,
				.timer=		NULL,
				.active=	false,
				.mutexConf= {
					.name = "REEI2C3_mtx",						/* human readable mutex name */
					.attr_bits = osMutexRecursive | osMutexPrioInherit,		/* attr_bits */
					.cb_mem =  NULL,										/* memory for control block */
					.cb_size = 0											/* size for control block */
				},
				.timerConf = {
					.name = "I2C3_tmr",
					.attr_bits = 0,		/* attr_bits */
					.cb_mem =  NULL,										/* memory for control block */
					.cb_size = 0											/* size for control block */
				}
			}
			#endif

		#if I2C_COUNT > 4
			,{	.reg=		I2C4,
				.thread=	_REEI2C_THREAD_NONE,
				.handle=	NULL,
				.mutex=		NULL,
				.timer=		NULL,
				.active=	false,
				.mutexConf= {
					.name = "REEI2C4_mtx",						/* human readable mutex name */
					.attr_bits = osMutexRecursive | osMutexPrioInherit,		/* attr_bits */
					.cb_mem =  NULL,										/* memory for control block */
					.cb_size = 0											/* size for control block */
				},
				.timerConf = {
					.name = "I2C4_tmr",
					.attr_bits = 0,		/* attr_bits */
					.cb_mem =  NULL,										/* memory for control block */
					.cb_size = 0											/* size for control block */
				}
			}
		#endif


	};
#endif


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */
static void _REEI2C_StartLib(void) {
	if (!__REEI2Cstarted) {
		/* Create mutex */
		for(uint8_t i=0; i<I2C_COUNT; i++){
			__REEI2Cuser[i].mutex = osMutexNew(&(__REEI2Cuser[i].mutexConf));
			__REEI2Cuser[i].timer = osTimerNew((osTimerFunc_t)__REEI2C_timeout_cb, osTimerOnce, &(__REEI2Cuser[i]), &(__REEI2Cuser[i].timerConf));
		}
		__REEI2Cstarted = true;
	}
}

static __REEI2Chw_t * __REEI2C_findHw(REEI2C_t * i2c) {
	uint8_t i;

	/* Match by hw register address */
	for (i=0; i<I2C_COUNT; i++) {
		if (i2c->ctl.dev == __REEI2Cuser[i].reg) {
			return (&__REEI2Cuser[i]);
		}
	}
	return NULL;
}

static REEI2C_err_t __REEI2C_takeHw(REEI2C_t * i2c) {
	osStatus_t osStatus;
	/* find the correct mutex */
	__REEI2Chw_t * hw = __REEI2C_findHw(i2c);
	if (hw == NULL) {
		return REEI2C_param;
	}

	/* A. take mutex or block */
	if (i2c->ctl.blocking) {
		osStatus = osMutexAcquire(hw->mutex, REEI2C_RTOS_TIMEOUT_MS);
		if (osStatus != osOK) {
			return REEI2C_fail;
		}
	/* B. take mutex or return */
	} else {
		if (osOK != osMutexAcquire(hw->mutex, 0)) {
			return REEI2C_fail;
		}
	}
	return REEI2C_success;
}

static REEI2C_err_t __REEI2C_releaseHw(REEI2C_t * i2c) {
//	osStatus_t osStatus;
	/* find the correct mutex */
	__REEI2Chw_t * hw = __REEI2C_findHw(i2c);
	if (hw == NULL) {
		return REEI2C_param;
	}

	/* A. take mutex or block */
	if(hw->thread == osThreadGetId()){
		return osMutexRelease(hw->mutex);
	}else{
		return REEI2C_fail;
	}
}

static REEI2C_err_t __REEI2C_emCode2REEI2CCode(I2C_TransferReturn_TypeDef emCode) {
	switch (emCode) {
		case i2cTransferDone:		return REEI2C_success;
		case i2cTransferNack:		return REEI2C_nack;
		case i2cTransferUsageFault:	return REEI2C_fail;
		case i2cTransferSwFault:	return REEI2C_fail;
		case i2cTransferBusErr:		return REEI2C_collision;
		case i2cTransferArbLost:	return REEI2C_collision;
		default:					return REEI2C_timeout;
	}
}

static REEI2C_err_t __REEI2C_Config(REEI2C_t * i2c) {

	I2C_Init_TypeDef i2cInit;

	/* Clock */
	CMU_ClockEnable(cmuClock_HFPER, true);
	CMU_ClockEnable(i2c->ctl.clk, true);

	/* IO */
	GPIO_PinModeSet(REEIO_PORT(i2c->ctl.scl), REEIO_PIN(i2c->ctl.scl), gpioModeWiredAndPullUp, 1);
	GPIO_PinModeSet(REEIO_PORT(i2c->ctl.sda), REEIO_PIN(i2c->ctl.sda), gpioModeWiredAndPullUp, 1);

	#if defined (_I2C_ROUTEPEN_MASK)
		i2c->ctl.dev->ROUTEPEN  =	I2C_ROUTEPEN_SDAPEN | I2C_ROUTEPEN_SCLPEN;
		i2c->ctl.dev->ROUTELOC0 =	(uint32_t)	((i2c->ctl.locationSda << _I2C_ROUTELOC0_SDALOC_SHIFT)
												|(i2c->ctl.locationScl << _I2C_ROUTELOC0_SCLLOC_SHIFT));
	#else
		i2c->ctl.dev->ROUTE =	I2C_ROUTE_SDAPEN | I2C_ROUTE_SCLPEN
								| (i2c->portLocation << _I2C_ROUTE_LOCATION_SHIFT);
	#endif

	/* Peripheral */
	i2cInit.enable = true;
	i2cInit.master = i2c->ctl.master;
	i2cInit.freq = i2c->ctl.bitrate;
	i2cInit.refFreq = i2c->ctl.refClk;
	i2cInit.clhr = i2c->ctl.clhr;
	I2C_Init(i2c->ctl.dev, &i2cInit);

	if(i2c->ctl.dev == I2C0){
		NVIC_EnableIRQ(I2C0_IRQn);

	#if defined(I2C_COUNT) && (I2C_COUNT >1)
	}else if(i2c->ctl.dev == I2C1){
		NVIC_EnableIRQ(I2C1_IRQn);
	#endif

	#if defined(I2C_COUNT) && (I2C_COUNT >2)
	}else if(i2c->ctl.dev == I2C2){
		NVIC_EnableIRQ(I2C2_IRQn);
	#endif

	}



	return REEI2C_success;
}

static void __REEI2C_timeout_cb(__REEI2Chw_t * hw) {
	if(hw != NULL){
		hw->active = false;
	}
}

static uint32_t __REEI2C_transfer(REEI2C_t * i2c, REEI2C_Sequence_t * seq) {

	/* 0. Prep */
	I2C_TransferReturn_TypeDef status;
	__REEI2Chw_t * hw = __REEI2C_findHw(i2c);
	uint32_t totalByteNb = (uint32_t)(1 + seq->buf[0].len);
	uint32_t transferedByteNb = 0;

	if (seq->flags & (I2C_FLAG_WRITE_READ|I2C_FLAG_WRITE_WRITE)) {
		totalByteNb += (uint32_t)(1 + seq->buf[1].len);		/* Give time for the second data len and address */
	}
	if (seq->flags & I2C_FLAG_10BIT_ADDR) {
		totalByteNb += 2;	/* Give time for the address second byte (I know it may sometimes be only one but who cares...) */
	}

	/* 1. Transfer */
	hw->active = true;
	status = I2C_TransferInit(i2c->ctl.dev, seq);

	if(status == i2cTransferInProgress){

		uint32_t timeout = REEI2C_TIMEOUTPERBYTE_MS * totalByteNb;
		if(timeout <2){
			timeout = 2;
		}
		osTimerStart(hw->timer, timeout);

		while((i2c->ctl.dev->IEN) && (hw->active)){
			if(systickDelta(hw->irqTick) > 1){
				hw->return_code = I2C_Transfer(i2c->ctl.dev);
				hw->irqTick = systickGet();
			}
			osThreadYield();
		}

	}else if(status != i2cTransferDone){
		/*Something went wrong*/
		REEBUGSWO_String("REEI2C, transferInitFailed(");
		if(status < 0){
			status = -status;
			REEBUGSWO_Code('-');
		}
		REEBUGSWO_Code('0'+status);
		REEBUGSWO_String(")\n");
	}

	status = hw->return_code;//  I2C_Transfer(i2c->ctl.dev);/* Update status */

	if (status == i2cTransferDone) {
		/* Return the total len of this type of transfer */
		if ((seq->flags == I2C_FLAG_READ) || (seq->flags == I2C_FLAG_WRITE)) {
			transferedByteNb = (seq->buf[0].len);					/* Single operation, only buf 0 used */
		} else if ((seq->flags == I2C_FLAG_WRITE_READ) || (seq->flags == I2C_FLAG_WRITE_WRITE)) {
			transferedByteNb = (uint32_t)(seq->buf[0].len + seq->buf[1].len);	/* Dual operation, both buffer used */
		}
	}else{
		i2c->ctl.dev->CMD |= I2C_CMD_STOP;
		osThreadYield();
		i2c->ctl.dev->CMD |= I2C_CMD_ABORT;

		transferedByteNb = 0;
		i2c->ctl.dev->IEN = 0;
		I2C_IntClear(i2c->ctl.dev, _I2C_IF_MASK);
	}
	hw->active = false;
	osTimerStop(hw->timer);
	return transferedByteNb;
}


/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */
REEI2C_err_t REEI2C_take(REEI2C_t * i2c) {
	#if REEI2C_AUTOSTART == 1
	/* Start lib if needed */
	if (!__REEI2Cstarted) {					/* We check because checking a bool is faster than calling a function */
		_REEI2C_StartLib();
	}
	#endif

	/* take control of hardware */
	if (i2c != NULL) {
		/* find the correct mutex */
		__REEI2Chw_t * hw = __REEI2C_findHw(i2c);
		REEI2C_err_t code;
		if (hw == NULL) {
			return REEI2C_param;
		}

		code = __REEI2C_takeHw(i2c);
		if (code != REEI2C_success) {
			return code;
		}

		/* Ensure hw is correctly configured */
		__REEI2C_Config(i2c);

		/* save user information */
		hw->thread = osThreadGetId();
		hw->handle = i2c;

		return REEI2C_success;
	} else {
		return REEI2C_null;
	}
}

REEI2C_err_t REEI2C_release(REEI2C_t * i2c) {
	#if REEI2C_AUTOSTART == 1
	/* Start lib if needed */
	if (!__REEI2Cstarted) {					/* We check because checking a bool is faster than calling a function */
		_REEI2C_StartLib();
	}
	#endif

	if (i2c != NULL) {
		/* find the correct mutex */
		__REEI2Chw_t * hw = __REEI2C_findHw(i2c);
		if (hw == NULL) {
			return REEI2C_param;
		}

		/* flush user information */
		#if __REEI2C_AUTOFLUSHWHENRELEASE == 1
			hw->thread = _REEI2C_THREAD_NONE;
			hw->handle = NULL;
		#endif

		/* Clean os service */
		osTimerDelete(hw->timer);			/* If this fails we have a mem access problem... */

		/* release control */
		if (osOK != osMutexRelease(hw->mutex)) {
			return REEI2C_fail;
		}
		return REEI2C_success;
	} else {
		return REEI2C_null;
	}
}

uint32_t REEI2C_tx(REEI2C_t * i2c, void * src, uint32_t len) {
	uint32_t cnt = 0;
	if ((i2c != NULL) && (src != NULL) && len) {
		/* take control of hardware */
//		if (REEI2C_success != __REEI2C_takeHw(i2c)) {
//			return 0;
//		}

		/* ====== A. MASTER ====== */
		if (i2c->ctl.master) {
			/* 0. Prep */
//			REEI2C_err_t status = REEI2C_fail;
			REEI2C_Sequence_t seq;
			seq.addr = i2c->ctl.address;
			seq.flags = I2C_FLAG_WRITE;
			seq.buf[0].data = src;
			seq.buf[0].len = len;

			/* 1. Transfer */
			cnt = __REEI2C_transfer(i2c, &seq);

		/* ====== B. SLAVE ======= */
		} else {

		}
		/* ======================= */
	}
//	__REEI2C_releaseHw(i2c);	/*Release recussive mutex token taken by __REEI2C_takeHw*/
	return cnt;
}

uint32_t REEI2C_rx(REEI2C_t * i2c, void * dst, uint32_t len) {
	uint32_t cnt = 0;
	if ((i2c != NULL) && (dst != NULL) && len) {
		/* take control of hardware */
//		if (REEI2C_success != __REEI2C_takeHw(i2c)) {
//			return 0;
//		}

		/* ====== A. MASTER ====== */
		if (i2c->ctl.master) {
			/* 0. Prep */
//			REEI2C_err_t status = REEI2C_fail;
			REEI2C_Sequence_t seq;
			seq.addr = i2c->ctl.address;
			seq.flags = I2C_FLAG_READ;
			seq.buf[0].data = dst;
			seq.buf[0].len = len;

			/* 1. Transfer */
			cnt = __REEI2C_transfer(i2c, &seq);

		/* ====== B. SLAVE ======= */
		} else {

		}
		/* ======================= */
	}
//	__REEI2C_releaseHw(i2c);	/*Release recussive mutex token taken by __REEI2C_takeHw*/
	return cnt;
}

uint32_t REEI2C_trx(REEI2C_t * i2c, void * src, void * dst, uint32_t len) {
	uint32_t cnt = 0;
	if ((i2c != NULL) && (src != NULL) && len) {
		/* take control of hardware */
//		if (REEI2C_success != __REEI2C_takeHw(i2c)) {
//			return 0;
//		}

		/* ====== A. MASTER ====== */
		if (i2c->ctl.master) {
			/* 0. Prep */
//			REEI2C_err_t status = REEI2C_fail;
			REEI2C_Sequence_t seq;
			seq.addr = i2c->ctl.address;
			seq.flags = I2C_FLAG_WRITE_READ;
			seq.buf[0].data = src;
			seq.buf[0].len = (uint16_t)len;
			seq.buf[1].data = dst;
			seq.buf[1].len = (uint16_t)len;

			/* 1. Transfer */
			cnt = __REEI2C_transfer(i2c, &seq);

		/* ====== B. SLAVE ======= */
		} else {

		}
		/* ======================= */
	}
//	__REEI2C_releaseHw(i2c);	/*Release recussive mutex token taken by __REEI2C_takeHw*/
	return cnt;
}

REEI2C_err_t REEI2C_ioctl(REEI2C_t * i2c, REEI2C_ioctl_t * args) {
	if ((i2c != NULL) && (args != NULL)) {
		/* take control of hardware */
		REEI2C_err_t code = __REEI2C_takeHw(i2c);
		if (code != REEI2C_success) {
			return code;
		}
		/* ====== A. MASTER ====== */
		if (i2c->ctl.master) {
			switch (args->action) {
				/* -- a. Reset BUS -- */
				case REEI2C_act_resetBus : {
					if(i2c == NULL) {
						__REEI2C_releaseHw(i2c);	/*Release recussive mutex token taken by __REEI2C_takeHw*/
						return REEI2C_null;
					}

					uint8_t i;
					uint32_t routeBackup = 0;

					/* Give IO control to GPIO */
				#if defined (_I2C_ROUTEPEN_MASK)
					routeBackup = i2c->ctl.dev->ROUTEPEN;
					i2c->ctl.dev->ROUTEPEN = 0;
				#else
					routeBackup = i2c->dev->ROUTE
					i2c->dev->ROUTE = 0;
				#endif

					/* Reset sequence is 9 SCL cycle with SDA high */
					GPIO_PinOutSet(REEIO_PORT(i2c->ctl.sda), REEIO_PIN(i2c->ctl.sda));

					for (i=0; i<_REEI2C_RESETSEQ_SCL_CYCLE_NB; i++) {
						GPIO_PinOutSet(REEIO_PORT(i2c->ctl.scl), REEIO_PIN(i2c->ctl.scl));
						osDelay(1);		/* smallest delay possible but still large enough for slaves to see */
						GPIO_PinOutClear(REEIO_PORT(i2c->ctl.scl), REEIO_PIN(i2c->ctl.scl));
						osDelay(1);		/* smallest delay possible but still large enough for slaves to see */
					}

					/* Restore IO control to I2C peripheral */
				#if defined (_I2C_ROUTEPEN_MASK)
					i2c->ctl.dev->ROUTEPEN = routeBackup;
				#else
					i2c->dev->ROUTE = routeBackup;
				#endif
					__REEI2C_releaseHw(i2c);	/*Release recussive mutex token taken by __REEI2C_takeHw*/
					return REEI2C_success;
				}
				/* -- b. Detect a slave -- */
				case REEI2C_act_detectSlave : {
					REEI2C_Sequence_t seq;
					REEI2C_err_t status = REEI2C_fail;
					__REEI2Chw_t * hw = __REEI2C_findHw(i2c);


					/* Prep sequence */
					seq.addr  = (uint16_t)args->value;
					seq.flags = I2C_FLAG_WRITE;
					seq.buf[0].data = NULL;
					seq.buf[0].len = 0;

					/* Start sequence */
					__REEI2C_transfer(i2c, &seq);

					status = hw->return_code;

//					status = I2C_TransferInit(i2c->ctl.dev, &seq);
//
//					/* Wait for completion or timeout */
//					uint16_t stupidDelay;
//					for(stupidDelay=0; stupidDelay<__REEI2C_STUPIDDELAYMAX; stupidDelay++){
//						status = I2C_Transfer(i2c->ctl.dev);		/* Update status */
//						if ((I2C_TransferReturn_TypeDef)status == i2cTransferInProgress) {
//							osThreadYield();
//						} else {
//							break;
//						}
//					}

					/* Report the result */
					__REEI2C_releaseHw(i2c);	/*Release recussive mutex token taken by __REEI2C_takeHw*/
					return __REEI2C_emCode2REEI2CCode(status);	/* If slave is present, will be REEI2C_success */
				}
				default:		break;
			}

		/* ====== B. SLAVE ======= */
		} else {

		}
		/* ======================= */
	} else {
		__REEI2C_releaseHw(i2c);	/*Release recussive mutex token taken by __REEI2C_takeHw*/
		return REEI2C_null;
	}
	__REEI2C_releaseHw(i2c);	/*Release recussive mutex token taken by __REEI2C_takeHw*/
	return REEI2C_fail;
}

REEI2C_err_t REEI2C_hook(REEI2C_t * i2c, REEI2C_hook_t * hook, void(*REEcb_t)(void *)) {
	if ((i2c != NULL) && (hook != NULL) && (REEcb_t != NULL)) {
		/* take control of hardware */
		REEI2C_err_t code = __REEI2C_takeHw(i2c);
		if (code != REEI2C_success) {
			return code;
		}

		/* ====== A. MASTER ====== */
		if (i2c->ctl.master) {

		/* ====== B. SLAVE ======= */
		} else {

		}
		/* ======================= */
	} else {
		return REEI2C_null;
	}
	return REEI2C_success;
}

REEI2C_err_t REEI2C_unhook(REEI2C_t * i2c, REEI2C_hook_t * hook, void(*REEcb_t)(void *)) {
	if ((i2c != NULL) && (hook != NULL) && (REEcb_t != NULL)) {
		/* take control of hardware */
		REEI2C_err_t code = __REEI2C_takeHw(i2c);
		if (code != REEI2C_success) {
			return code;
		}

		/* ====== A. MASTER ====== */
		if (i2c->ctl.master) {

		/* ====== B. SLAVE ======= */
		} else {

		}
		/* ======================= */
	} else {
		return REEI2C_null;
	}
	return REEI2C_success;
}


/*
 * I2Cx IRQ Handler
 *  */

#if  defined(I2C_COUNT) && (I2C_COUNT>0)
void I2C0_IRQHandler(void){ __REEI2Cuser[0].return_code = I2C_Transfer(I2C0); __REEI2Cuser[0].irqTick = systickGet();}
#endif

#if  defined(I2C_COUNT) && (I2C_COUNT>1)
void I2C1_IRQHandler(void){ __REEI2Cuser[1].return_code = I2C_Transfer(I2C1); __REEI2Cuser[1].irqTick = systickGet(); }
#endif

#if  defined(I2C_COUNT) && (I2C_COUNT>2)
void I2C2_IRQHandler(void){ __REEI2Cuser[2].return_code = I2C_Transfer(I2C2); __REEI2Cuser[2].irqTick = systickGet(); }
#endif


#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
