/*
	\name		REEcore.c
	\author		Laurence DV
	\version	1.0.0
	\brief		Core access and low level execution stuff
	\note
	\license	All right reserved RealEE inc. (2019)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <hardware.h>
#include <conf.h>
#include <REEutil.h>
#include <REEtype.h>
#include <REEcore.h>

#include <em_cmu.h>
#include <em_device.h>
#include <em_dbg.h>

#include <stdio.h>
#include <REEBUG.h>

#if defined(_EFM32_PEARL_FAMILY) || defined(_EFM32_JADE_FAMILY) || defined(_EFM32_GIANT_FAMILY) || defined(_EFM32_WONDER_FAMILY)
#define __CORE_ACCESS
#endif


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
/* Memory Fault */
#define __CM_CFSR_INSTACCVIOL	(BIT0)
#define	__CM_CFSR_DATAACCVIOL	(BIT1)
#define	__CM_CFSR_MEMUNSTACKING	(BIT3)
#define	__CM_CFSR_MEMSTACKING	(BIT4)
#define	__CM_CFSR_MEMFPLAZY		(BIT5)		/* Only CM-4F */
#define	__CM_CFSR_MMFARVALID	(BIT7)
/* Bus Fault */
#define	__CM_CFSR_INSTBUSERR	(BIT8)
#define	__CM_CFSR_PRECISERR		(BIT9)
#define	__CM_CFSR_IMPRECISERR	(BIT10)
#define	__CM_CFSR_BUSUNSTACKING	(BIT11)
#define	__CM_CFSR_BUSSTACKING	(BIT12)
#define	__CM_CFSR_BUSFPLAZY		(BIT13)		/* Only CM-4F */
#define	__CM_CFSR_BFARVALID		(BIT15)
/* Usage Fault */
#define	__CM_CFSR_UNDEFINST		(BIT16)
#define	__CM_CFSR_INVALIDSTATE	(BIT17)
#define	__CM_CFSR_INVALIDPC		(BIT18)
#define	__CM_CFSR_COPROCERR		(BIT19)
#define	__CM_CFSR_UNALIGNED		(BIT24)
#define	__CM_CFSR_DIVBYZERO		(BIT25)


/* -------------------------------- +
|									|
|	Global Variable					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */
CMU_Clock_TypeDef REEcore_findCMUClock(void * hwPtr) {
	uint32_t hwAdd = (uint32_t)hwPtr;
	switch (hwAdd) {
		#ifdef CRYPTO0_BASE
		case CRYPTO0_BASE:		return cmuClock_CRYPTO0;
		#endif
		#ifdef CRYPTO1_BASE
		case CRYPTO1_BASE:		return cmuClock_CRYPTO1;
		#endif
		#ifdef LESENSE_BASE
		case LESENSE_BASE:		return cmuClock_LESENSE;
		#endif
		#ifdef EBI_BASE
		case EBI_BASE:			return cmuClock_EBI;
		#endif
		#ifdef ETH_BASE
		case ETH_BASE:			return cmuClock_ETH;
		#endif
		#ifdef SDIO_BASE
		case SDIO_BASE:			return cmuClock_SDIO;
		#endif
		#ifdef GPIO_BASE
		case GPIO_BASE:			return cmuClock_GPIO;
		#endif
		#ifdef PRS_BASE
		case PRS_BASE:			return cmuClock_PRS;
		#endif
		#ifdef LDMA_BASE
		case LDMA_BASE:			return cmuClock_LDMA;
		#endif
		#ifdef GPCRC_BASE
		case GPCRC_BASE:		return cmuClock_GPCRC;
		#endif
		#ifdef CAN0_BASE
		case CAN0_BASE:			return cmuClock_CAN0;
		#endif
		#ifdef CAN1_BASE
		case CAN1_BASE:			return cmuClock_CAN1;
		#endif
		#ifdef TIMER0_BASE
		case TIMER0_BASE:		return cmuClock_TIMER0;
		#endif
		#ifdef TIMER1_BASE
		case TIMER1_BASE:		return cmuClock_TIMER1;
		#endif
		#ifdef TIMER2_BASE
		case TIMER2_BASE:		return cmuClock_TIMER2;
		#endif
		#ifdef TIMER3_BASE
		case TIMER3_BASE:		return cmuClock_TIMER3;
		#endif
		#ifdef TIMER4_BASE
		case TIMER4_BASE:		return cmuClock_TIMER4;
		#endif
		#ifdef TIMER5_BASE
		case TIMER5_BASE:		return cmuClock_TIMER5;
		#endif
		#ifdef TIMER6_BASE
		case TIMER6_BASE:		return cmuClock_TIMER6;
		#endif
		#ifdef WTIMER0_BASE
		case WTIMER0_BASE:		return cmuClock_WTIMER0;
		#endif
		#ifdef WTIMER1_BASE
		case WTIMER1_BASE:		return cmuClock_WTIMER1;
		#endif
		#ifdef WTIMER2_BASE
		case WTIMER2_BASE:		return cmuClock_WTIMER2;
		#endif
		#ifdef WTIMER3_BASE
		case WTIMER3_BASE:		return cmuClock_WTIMER3;
		#endif
		#ifdef USART0_BASE
		case USART0_BASE:		return cmuClock_USART0;
		#endif
		#ifdef USART1_BASE
		case USART1_BASE:		return cmuClock_USART1;
		#endif
		#ifdef USART2_BASE
		case USART2_BASE:		return cmuClock_USART2;
		#endif
		#ifdef USART3_BASE
		case USART3_BASE:		return cmuClock_USART3;
		#endif
		#ifdef USART4_BASE
		case USART4_BASE:		return cmuClock_USART4;
		#endif
		#ifdef USART5_BASE
		case USART5_BASE:		return cmuClock_USART5;
		#endif
		#ifdef UART0_BASE
		case UART0_BASE:		return cmuClock_UART0;
		#endif
		#ifdef UART1_BASE
		case UART1_BASE:		return cmuClock_UART1;
		#endif
		#ifdef QSPI0_BASE
		case QSPI0_BASE:		return cmuClock_QSPI0;
		#endif
		#ifdef LEUART0_BASE
		case LEUART0_BASE:		return cmuClock_LEUART0;
		#endif
		#ifdef LEUART1_BASE
		case LEUART1_BASE:		return cmuClock_LEUART1;
		#endif
		#ifdef LETIMER0_BASE
		case LETIMER0_BASE:		return cmuClock_LETIMER0;
		#endif
		#ifdef LETIMER1_BASE
		case LETIMER1_BASE:		return cmuClock_LETIMER1;
		#endif
		#ifdef CRYOTIMER_BASE
		case CRYOTIMER_BASE:	return cmuClock_CRYOTIMER;
		#endif
		#ifdef PCNT0_BASE
		case PCNT0_BASE:		return cmuClock_PCNT0;
		#endif
		#ifdef PCNT1_BASE
		case PCNT1_BASE:		return cmuClock_PCNT1;
		#endif
		#ifdef PCNT2_BASE
		case PCNT2_BASE:		return cmuClock_PCNT2;
		#endif
		#ifdef I2C0_BASE
		case I2C0_BASE:			return cmuClock_I2C0;
		#endif
		#ifdef I2C1_BASE
		case I2C1_BASE:			return cmuClock_I2C1;
		#endif
		#ifdef I2C2_BASE
		case I2C2_BASE:			return cmuClock_I2C2;
		#endif
		#ifdef ADC0_BASE
		case ADC0_BASE:			return cmuClock_ADC0;
		#endif
		#ifdef ADC1_BASE
		case ADC1_BASE:			return cmuClock_ADC1;
		#endif
		#ifdef ACMP0_BASE
		case ACMP0_BASE:		return cmuClock_ACMP0;
		#endif
		#ifdef ACMP1_BASE
		case ACMP1_BASE:		return cmuClock_ACMP1;
		#endif
		#ifdef ACMP2_BASE
		case ACMP2_BASE:		return cmuClock_ACMP2;
		#endif
		#ifdef ACMP3_BASE
		case ACMP3_BASE:		return cmuClock_ACMP3;
		#endif
		#ifdef VDAC0_BASE
		case VDAC0_BASE:		return cmuClock_VDAC0;
		#endif
		#ifdef USB_BASE
		case USB_BASE:			return cmuClock_USB;
		#endif
		#ifdef IDAC0_BASE
		case IDAC0_BASE:		return cmuClock_IDAC0;
		#endif
		#ifdef CSEN_BASE
		case CSEN_BASE:			return cmuClock_CSEN_HF;
#if defined(DEBUG) && (DEBUG)
		#warning "TODO: add cmuClock_CSEN_LF somehow"
#endif
		#endif
		#ifdef LCD_BASE
		case LCD_BASE:			return cmuClock_LCD;
		#endif
		#ifdef RTC_BASE
		case RTC_BASE:			return cmuClock_RTC;
		#endif
		#ifdef RTCC_BASE
		case RTCC_BASE:			return cmuClock_RTCC;
		#endif
		#if defined(_SILICON_LABS_32B_SERIES_2)
			#ifdef WDOG0_BASE
			case WDOG0_BASE:		return cmuClock_WDOG0;
			#endif
			#ifdef WDOG1_BASE
			case WDOG1_BASE:		return cmuClock_WDOG1;
			#endif
		#endif
		#ifdef TRNG0_BASE
		case TRNG0_BASE:		return cmuClock_TRNG0;
		#endif

		default:				return 0;
	}
}

IRQn_Type REEcore_findIRQ(void * hwPtr) {
	uint32_t hwAdd = (uint32_t)hwPtr;
	switch (hwAdd) {
		default:	return -128;		/* INVALID */

		#if defined(EMU_BASE)
		case EMU_BASE:			return EMU_IRQn;
		#endif
		#if defined(WDOG0_BASE)
		case WDOG0_BASE:		return WDOG0_IRQn;
		#endif
		#if defined(WDOG1_BASE)
		case WDOG1_BASE:		return WDOG1_IRQn;
		#endif
		#if defined(LDMA_BASE)
		case LDMA_BASE:			return LDMA_IRQn;
		#endif
		#if defined(GPIO_BASE)
		case GPIO_BASE:			return GPIO_EVEN_IRQn;
		#endif
		#if defined(CMU_BASE)
		case CMU_BASE:			return CMU_IRQn;
		#endif
		#if defined(MSC_BASE)
		case MSC_BASE:			return MSC_IRQn;
		#endif
		#if defined(FPUEH_BASE)
		case FPUEH_BASE:		return FPUEH_IRQn;
		#endif
		#if defined(EBI_BASE)
		case EBI_BASE:			return EBI_IRQn;
		#endif

		#if defined(TIMER0_BASE)
		case TIMER0_BASE:		return TIMER0_IRQn;
		#endif
		#if defined(TIMER1_BASE)
		case TIMER1_BASE:		return TIMER1_IRQn;
		#endif
		#if defined(TIMER2_BASE)
		case TIMER2_BASE:		return TIMER2_IRQn;
		#endif
		#if defined(TIMER3_BASE)
		case TIMER3_BASE:		return TIMER3_IRQn;
		#endif
		#if defined(TIMER4_BASE)
		case TIMER4_BASE:		return TIMER4_IRQn;
		#endif
		#if defined(TIMER5_BASE)
		case TIMER5_BASE:		return TIMER5_IRQn;
		#endif
		#if defined(TIMER6_BASE)
		case TIMER6_BASE:		return TIMER6_IRQn;
		#endif

		#if defined(WTIMER0_BASE)
		case WTIMER0_BASE:		return WTIMER0_IRQn;
		#endif
		#if defined(WTIMER1_BASE)
		case WTIMER1_BASE:		return WTIMER1_IRQn;
		#endif
		#if defined(WTIMER2_BASE)
		case WTIMER2_BASE:		return WTIMER2_IRQn;
		#endif
		#if defined(WTIMER3_BASE)
		case WTIMER3_BASE:		return WTIMER3_IRQn;
		#endif

		#if defined(LETIMER0_BASE)
		case LETIMER0_BASE:		return LETIMER0_IRQn;
		#endif
		#if defined(LETIMER1_BASE)
		case LETIMER1_BASE:		return LETIMER1_IRQn;
		#endif

		#if defined(CRYOTIMER_BASE)
		case CRYOTIMER_BASE:	return CRYOTIMER_IRQn;
		#endif

		#if defined(PCNT0_BASE)
		case PCNT0_BASE:		return PCNT0_IRQn;
		#endif
		#if defined(PCNT1_BASE)
		case PCNT1_BASE:		return PCNT1_IRQn;
		#endif
		#if defined(PCNT2_BASE)
		case PCNT2_BASE:		return PCNT2_IRQn;
		#endif

		#if defined(RTCC_BASE)
		case RTCC_BASE:			return RTCC_IRQn;
		#endif
		#if defined(RTC_BASE)
		case RTC_BASE:			return RTC_IRQn;
		#endif

		#if defined(USART0_BASE)
			#if defined(__CM0PLUS_REV)
			case USART0_BASE:	return USART0_IRQn;
			#else
			case USART0_BASE:	return USART0_RX_IRQn;
			#endif
		#endif
		#if defined(USART1_BASE)
			#if defined(__CM0PLUS_REV)
			case USART1_BASE:	return USART1_IRQn;
			#else
			case USART1_BASE:	return USART1_RX_IRQn;
			#endif
		#endif
		#if defined(USART2_BASE)
			#if defined(__CM0PLUS_REV)
			case USART2_BASE:	return USART2_IRQn;
			#else
			case USART2_BASE:	return USART2_RX_IRQn;
			#endif
		#endif
		#if defined(USART3_BASE)
			#if defined(__CM0PLUS_REV)
			case USART3_BASE:	return USART3_IRQn;
			#else
			case USART3_BASE:	return USART3_RX_IRQn;
			#endif
		#endif
		#if defined(USART4_BASE)
			#if defined(__CM0PLUS_REV)
			case USART4_BASE:	return USART4_IRQn;
			#else
			case USART4_BASE:	return USART4_RX_IRQn;
			#endif
		#endif
		#if defined(USART5_BASE)
			#if defined(__CM0PLUS_REV)
			case USART5_BASE:	return USART5_IRQn;
			#else
			case USART5_BASE:	return USART5_RX_IRQn;
			#endif
		#endif

		#if defined(UART0_BASE)
			#if defined(__CM0PLUS_REV)
			case UART0_BASE:	return UART0_IRQn;
			#else
			case UART0_BASE:	return UART0_RX_IRQn;
			#endif
		#endif
		#if defined(UART1_BASE)
			#if defined(__CM0PLUS_REV)
			case UART1_BASE:	return UART1_IRQn;
			#else
			case UART1_BASE:	return UART1_RX_IRQn;
			#endif
		#endif

		#if defined(LEUART0_BASE)
		case LEUART0_BASE:		return LEUART0_IRQn;
		#endif
		#if defined(LEUART1_BASE)
		case LEUART1_BASE:		return LEUART1_IRQn;
		#endif

		#if defined(I2C0_BASE)
		case I2C0_BASE:			return I2C0_IRQn;
		#endif
		#if defined(I2C1_BASE)
		case I2C1_BASE:			return I2C1_IRQn;
		#endif
		#if defined(I2C2_BASE)
		case I2C2_BASE:			return I2C2_IRQn;
		#endif

		#if defined(CAN0_BASE)
		case CAN0_BASE:			return CAN0_IRQn;
		#endif
		#if defined(CAN1_BASE)
		case CAN1_BASE:			return CAN1_IRQn;
		#endif

		#if defined(SMU_BASE)
		case SMU_BASE:			return SMU_IRQn;
		#endif

		#if defined(ACMP0_BASE)
		case ACMP0_BASE:		return ACMP0_IRQn;
		#endif
		#if defined(ACMP1_BASE)
		case ACMP1_BASE:		return ACMP0_IRQn;
		#endif
		#if defined(ACMP2_BASE)
		case ACMP2_BASE:		return ACMP2_IRQn;
		#endif
		#if defined(ACMP3_BASE)
		case ACMP3_BASE:		return ACMP2_IRQn;
		#endif

		#if defined(ADC0_BASE)
		case ADC0_BASE:			return ADC0_IRQn;
		#endif
		#if defined(ADC1_BASE)
		case ADC1_BASE:			return ADC1_IRQn;
		#endif

		#if defined(IDAC0_BASE)
		case IDAC0_BASE:		return IDAC0_IRQn;
		#endif

		#if defined(VDAC0_BASE)
		case VDAC0_BASE:		return VDAC0_IRQn;
		#endif

		#if defined(CSEN_BASE)
		case CSEN_BASE:			return CSEN_IRQn;
		#endif

		#if defined(LESENSE_BASE)
		case LESENSE_BASE:		return LESENSE_IRQn;
		#endif

		#if defined(CRYPTO0_BASE)
		case CRYPTO0_BASE:		return CRYPTO0_IRQn;
		#endif
		#if defined(CRYPTO1_BASE)
		case CRYPTO1_BASE:		return CRYPTO1_IRQn;
		#endif

		#if defined(TRNG0_BASE) && !defined(__CM0PLUS_REV)	/* Even though tg11 have TRNG they don't have a vector for it... at least currently (emlib 5.7) */
		case TRNG0_BASE:		return TRNG0_IRQn;
		#endif
	}
}


/* -------------------------------- +
|									|
|	CPU Exception handling			|
|									|
+ -------------------------------- */
/* HardFault data extract function */
#if defined __CORE_ACCESS
void _REEBUG_HardFault(uint32_t *sp) {
	char stringBuf[32];
	uint8_t breakFlag=1;
	uint8_t printCPUReg=0;

    uint32_t cfsr  = SCB->CFSR;
    uint32_t hfsr  = SCB->HFSR;
    uint32_t mmfar = SCB->MMFAR;
    uint32_t bfar  = SCB->BFAR;
    uint32_t r0  = sp[0];
    uint32_t r1  = sp[1];
    uint32_t r2  = sp[2];
    uint32_t r3  = sp[3];
    uint32_t r12 = sp[4];
    uint32_t lr  = sp[5];
    uint32_t pc  = sp[6];	/*Program Counter			*/
    uint32_t psr = sp[7];	/*Program Status Register	*/

	REEBUGSWO_String("Hard Fault: ");
	if (cfsr & __CM_CFSR_INSTACCVIOL) {
		REEBUGSWO_String("Mem manager fault: Instruction access violation(faulty inst. @PC)\n");
		printCPUReg = 1;
	} else if (cfsr & __CM_CFSR_DATAACCVIOL) {
		REEBUGSWO_String("Mem manager fault: Data access violation (faulty inst. @PC) (faulty data @MMFAR)\n");
		printCPUReg = 1;
	} else if (cfsr & __CM_CFSR_MEMUNSTACKING) {
		REEBUGSWO_String("Mem manager fault: unstacking\n");
	} else if (cfsr & __CM_CFSR_MEMSTACKING) {
		REEBUGSWO_String("Mem manager fault: stacking\n");
	} else if (cfsr & __CM_CFSR_MEMFPLAZY) {
		REEBUGSWO_String("Mem manager fault: FPU lazy\n");
	} else if (cfsr & __CM_CFSR_INSTBUSERR) {
		REEBUGSWO_String("Bus fault: instruction error\n");
	} else if (cfsr & __CM_CFSR_PRECISERR) {
		REEBUGSWO_String("Bus fault: data error (precise) (faulty data @BFAR)\n");
	} else if (cfsr & __CM_CFSR_IMPRECISERR) {
		REEBUGSWO_String("Bus fault: data error (imprecise)\n");
	} else if (cfsr & __CM_CFSR_BUSUNSTACKING) {
		REEBUGSWO_String("Bus fault: unstacking\n");
	} else if (cfsr & __CM_CFSR_BUSSTACKING) {
		REEBUGSWO_String("Bus fault: stacking\n");
	} else if (cfsr & __CM_CFSR_BUSFPLAZY) {
		REEBUGSWO_String("Bus fault: FPU lazy\n");
	} else if (cfsr & __CM_CFSR_UNDEFINST) {
		REEBUGSWO_String("Undefined instruction (faulty inst. @PC)\n");
	} else if (cfsr & __CM_CFSR_INVALIDSTATE) {
		REEBUGSWO_String("EPSR rendered invalid (faulty inst. @LR-1)\n");
		printCPUReg = 1;
	} else if (cfsr & __CM_CFSR_INVALIDPC) {
		REEBUGSWO_String("Invalid PC (faulty inst. @PC)\n");
		printCPUReg = 1;
	} else if (cfsr & __CM_CFSR_COPROCERR) {
		REEBUGSWO_String("No Co-Processor\n");
	} else if (cfsr & __CM_CFSR_UNALIGNED) {
		REEBUGSWO_String("Unaligned memory access\n");
	} else if (cfsr & __CM_CFSR_DIVBYZERO) {
		REEBUGSWO_String("Division by 0 (faulty inst. @PC)\n");
		printCPUReg = 1;
	} else {
		REEBUGSWO_String("\n");
		printCPUReg = 1;
	}

	sprintf(stringBuf, "HFSR:\t0x%08x\n", (unsigned int)hfsr);		REEBUGSWO_String(stringBuf);
	sprintf(stringBuf, "CFSR:\t0x%08x\n", (unsigned int)cfsr);		REEBUGSWO_String(stringBuf);
	if (cfsr & __CM_CFSR_MMFARVALID) {
		sprintf(stringBuf, "MMFAR:\t0x%08x\n", (unsigned int)mmfar);REEBUGSWO_String(stringBuf);
	}
	if (cfsr & __CM_CFSR_BFARVALID) {
		sprintf(stringBuf, "BFAR:\t0x%08x\n", (unsigned int)bfar);	REEBUGSWO_String(stringBuf);
	}
	if (printCPUReg) {
		sprintf(stringBuf, "R0:\t0x%08x\n", (unsigned int)r0);		REEBUGSWO_String(stringBuf);
		sprintf(stringBuf, "R1:\t0x%08x\n", (unsigned int)r1);		REEBUGSWO_String(stringBuf);
		sprintf(stringBuf, "R2:\t0x%08x\n", (unsigned int)r2);		REEBUGSWO_String(stringBuf);
		sprintf(stringBuf, "R3:\t0x%08x\n", (unsigned int)r3);		REEBUGSWO_String(stringBuf);
		sprintf(stringBuf, "R12:\t0x%08x\n", (unsigned int)r12);	REEBUGSWO_String(stringBuf);
		sprintf(stringBuf, "LR:\t0x%08x\n", (unsigned int)lr);		REEBUGSWO_String(stringBuf);
		sprintf(stringBuf, "PC:\t0x%08x\n", (unsigned int)pc);		REEBUGSWO_String(stringBuf);
		sprintf(stringBuf, "PSR:\t0x%08x\n", (unsigned int)psr);	REEBUGSWO_String(stringBuf);
	}
	if (lr & BIT2) {
		REEBUGSWO_String("Was using Process stack\n");
	} else {
		REEBUGSWO_String("Was using Main stack\n");
	}

	/* Halt execution if debugger connected */
	if (DBG_Connected()) {
		while(breakFlag) {
			__BKPT(1);
		}
	/* If not, reboot the MCU!!! */
	} else {
		REEBUGSWO_String("Hard fault reset\n");
		NVIC_SystemReset();
	}
}
#else
void debugHardfault(uint32_t *sp) {
	uint32_t r0  = sp[0];
	uint32_t r1  = sp[1];
	uint32_t r2  = sp[2];
	uint32_t r3  = sp[3];
	uint32_t r12 = sp[4];
	uint32_t lr  = sp[5];		/* Link Register*/
	uint32_t pc  = sp[6];		/* Program Counter*/
	uint32_t psr = sp[7];		/* Program Status Register*/

	/* SUSH I know they are not used in the software... geeez */
	(void)r0;(void)r1;(void)r2;(void)r3;(void)r12;(void)lr;(void)pc;(void)psr;

	/* Exitable debug trap loop */
	bool loop = true;
	while(loop);
}
#endif

/* Non-Maskable-Interrupt handler */
#if defined __CORE_ACCESS
void NMI_Handler(void) {
	uint8_t breakFlag = 1;
	volatile uint32_t fault = SCB->CFSR;
	fault = fault;		/* Suppress warning but keep content for debugging inspection */

	if (DBG_Connected()) {
		while(breakFlag) {
			__BKPT(1); /* Halt execution if debugger connected */
		}
	} else {
		REEBUGSWO_String("NMI reset\n");
		NVIC_SystemReset();
	}
}
#endif

/* CPU HardFault interrupt handler */
#if defined __CORE_ACCESS
__attribute__( (naked) )
void HardFault_Handler(void) {
    __asm volatile (
        "tst lr, #4                                        \n"
        "ite eq                                            \n"
        "mrseq r0, msp                                     \n"
        "mrsne r0, psp                                     \n"
        "ldr r1, REEBUG_Hardfault_address                  \n"
        "bx r1                                             \n"
        "REEBUG_Hardfault_address: .word _REEBUG_HardFault \n"
    );
}
#else
__attribute__( (naked) )
void HardFault_Handler(void) {
    __asm volatile (
        "mrs r0, msp                                   \n"
        "mov r1, #4                                    \n"
        "mov r2, lr                                    \n"
        "tst r2, r1                                    \n"
        "beq jump_debugHardfault                       \n"
        "mrs r0, psp                                   \n"
        "jump_debugHardfault:                          \n"
        "ldr r1, debugHardfault_address                \n"
        "bx r1                                         \n"
        "debugHardfault_address: .word debugHardfault  \n"
    );
}
#endif

#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
