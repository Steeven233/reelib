/*
	@name		REECAN.h
	@author		Jonathan Lecuyer LEsperance
	@version	1.0.0
	@note		CAN Peripheral lib, largely inspired by REEI2C
	@license	SEE $REElib_root/LICENSE.md
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ ---------------------------------*/
#include <REECAN.h>
#include <REEcore.h>		/* core & clock related utilities */
#include <REEBUG.h>			/* REEBUG fct are empty stub when symbol DEBUG is not defined */
#include <string.h>			/* Debug*/

/* hardware */
#include <em_chip.h>		/* Chip's register definitions */
#include <em_cmu.h>
#include <em_gpio.h>


/* -------------------------------- +
|									|
|	Default config loading			|
|									|
+ -------------------------------- */
#ifndef REECAN_RTOS_TIMEOUT
	#define		REECAN_RTOS_TIMEOUT_MS			REECAN_RTOS_TIMEOUT_MS_DEF
#endif

#ifndef REECAN_HOOK_MAXCNT
	#define		REECAN_HOOK_MAXCNT				REECAN_HOOK_MAXCNT_DEF
#endif

#ifndef REECAN_ENABLE_SWO_DEBUG
	#define REECAN_ENABLE_SWO_DEBUG  (false)		/*Set to false when dev completed*/
#endif

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ ---------------------------------*/
#define	_REECAN_RESETSEQ_SCL_CYCLE_NB		(9)			/* Number of High to Low to High cycle in a reset sequence */
#define	_REECAN_THREAD_NONE					(0)			/* Thread id for no thread... (this sentence is very weird, please help) */

#define	__REECAN_STUPIDDELAYMAX				(1000)		/* This is used as TOP cnt value for spinlock style delays */

#define __REECAN_ID_11BIT_MASK				(0x000007FF)
#define __REECAN_ID_29BIT_MASK				(0x1FFFFFFF)

/* CMSIS RTOS2 Stuff */
#if defined(REELIB_USE_CMSISRTOS2) && defined(CAN_PRESENT)
	osRtxMutex_t __REECAN_mutexMem[CAN_COUNT];

	const osMutexAttr_t __REECANMutexConf[CAN_COUNT] = {
		{
			.name 		= "REECAN0_mtx",							/* human readable mutex name */
			.attr_bits 	= osMutexRecursive | osMutexPrioInherit,	/* attr_bits */
			.cb_mem		= &__REECAN_mutexMem[0],
			.cb_size	= sizeof(osRtxMutex_t),
		},

		#if CAN_COUNT > 1
		{
			.name 		= "REECAN1_mtx",							/* human readable mutex name */
			.attr_bits 	= osMutexRecursive | osMutexPrioInherit,	/* attr_bits */
			.cb_mem		= &__REECAN_mutexMem[1],
			.cb_size	= sizeof(osRtxMutex_t),
		}
		#endif
	};

	const osTimerAttr_t __REECANTimerConf[CAN_COUNT] = {
		{
			.name = "CAN0_tmr",						/* human readable timer name */
		},
		#if CAN_COUNT > 1
		{
			.name = "CAN1_tmr",						/* human readable timer name */
		}
		#endif
	};
#endif


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ ---------------------------------*/
typedef struct {
	uint8_t					id;					/* CAN offset identifier (0:CAN0, 1:CAN1...)*/
	#ifdef CAN_PRESENT
	CAN_TypeDef * const reg;					/* hw register address 0 (constant pointer to variable data/register)*/
	#endif
	#ifdef REELIB_USE_CMSISRTOS2
	osThreadId_t			thread;				/* Last/current thread that used this hw */
	REECAN_t *				handle;				/* Actual handle given to owner */
	osMutexId_t				mutex;				/* hw protection mutex */
	osTimerId_t				timer;				/* os timer dedicated to handling the hw */
	#endif
	bool					active;				/* hw activity status */

	const CMU_Clock_TypeDef	clk;				/* Peripheral clock control*/
	const IRQn_Type			irq;				/* Peripheral IRQ identifier*/
}__REECANhw_t;

typedef union{
	uint32_t rawVal;
	struct{
		uint32_t 		:29;	/* Message Identifier */
		uint8_t dir		:1;		/* Message Direction */
		uint8_t xtd		:1;		/* Extended Identifier */
		uint8_t valid	:1;		/* Message Valid */
	};
	struct{
		uint32_t id 	:29;	/* Message Identifier */
		uint8_t 		:3;
	}extended;
	struct{
		uint32_t 		:18;
		uint16_t id 	:11;	/* Message Identifier */
		uint8_t 		:3;
	}standard;
}REE_MIR_ARB_t;


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ ---------------------------------*/
static void __REECAN_timeout_cb(REECAN_t * can);

#ifdef CAN_PRESENT
	__REECANhw_t __REECAN_hardware[CAN_COUNT] = {
		#if CAN_COUNT >0
			{	.id = 		0,
				.reg=		CAN0,
			#ifdef REELIB_USE_CMSISRTOS2
				.thread=	_REECAN_THREAD_NONE,
				.handle=	NULL,
				.mutex=		NULL,
				.timer=		NULL,
			#endif
				.active=	false,
				.clk =		cmuClock_CAN0,
				.irq = 		CAN0_IRQn,
			},
		#endif

		#if CAN_COUNT >1
			{	.id = 1,
				.reg=		CAN1,
			#ifdef REELIB_USE_CMSISRTOS2
				.thread=	_REECAN_THREAD_NONE,
				.handle=	NULL,
				.mutex=		NULL,
				.timer=		NULL,
			#endif
				.active=	false,
				.clk =		cmuClock_CAN1,
				.irq = 		CAN1_IRQn,
			},
		#endif
	};
#endif


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ ---------------------------------*/
#define REECAN_MIR_INTERFACE_RD	(0)	/* MIR interface used to read from register */
#define REECAN_MIR_INTERFACE_WR	(1)	/* MIR interface used to read from register */

/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ ---------------------------------*/
static __REECANhw_t * __REECAN_findHardware(REECAN_t * can) {
	#ifdef CAN_PRESENT
	uint8_t id;
	__REECANhw_t * hw;
	for (id=0; id<CAN_COUNT; id++) {
		hw = &__REECAN_hardware[id];
		if(can->candev == (hw->reg)){	// do CAN base address match ? same peripheral
			return hw;
		}
	}
	#endif
	return NULL;
}

/* Return true if calling thread is owner of the peripheral at the moment of calling */
static bool __REECAN_isOwner(REECAN_t * can) {
	#ifdef CAN_PRESENT
	__REECANhw_t * hw;
	if(can != NULL){						// Real Handle
		hw 	= __REECAN_findHardware(can);	// Search hardware
		if(hw != NULL){						// Hardware found
			if (hw->handle == can){			// Handle match with hardware (legitimately acquired)
				return true;
			}
		}
	}
	#endif
	return false;
}

static void __REECAN_timeout_cb(REECAN_t * can) {
	#ifdef CAN_PRESENT
	__REECANhw_t * hw = __REECAN_findHardware(can);
	hw->active = false;
	#endif
}

static uint32_t __REECAN_transfer(REECAN_t * can, REECAN_messageObject_t * message) {
	return 0;
}


/* -------------------------------- +
|									|
|	API								|
|									|
+ ---------------------------------*/
/* Take exclusive control of a can peripheral until released */
REECAN_err_t REECAN_open(REECAN_t * can){
	#ifdef CAN_PRESENT
	__REECANhw_t * hw 	= NULL;

	/* CAN device pointer must be initiated*/
	if((can == NULL) || (can->candev == NULL)) {
		return REECAN_null;
	}

	/* Search for the control structure*/
	hw = __REECAN_findHardware(can);
	if(hw == NULL){
		return REECAN_notFound;	//
	}


	if(hw->mutex == NULL){
		hw->mutex = osMutexNew(&__REECANMutexConf[hw->id]);
	}

	if (osOK != osMutexAcquire(hw->mutex, can->maxWaitTime_ms)) {
		return REECAN_timeout;
	}

	/* save user information */
	hw->handle = can;
	hw->thread = osThreadGetId();
	hw->timer = osTimerNew((void *)__REECAN_timeout_cb, osTimerOnce, (void *)can, &__REECANTimerConf[hw->id]);	/* needs to be created dynamically because of the param passed to cb */

	/* Clock */
	CMU_ClockEnable(cmuClock_HFPER, true);
	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(hw->clk, true);

	/* Reset the CAN device */

	CAN_Enable((CAN_TypeDef*)hw->reg, false);
	CAN_Reset((CAN_TypeDef*)hw->reg);

	/* Clear MIR*/
	CAN_ResetMessages((CAN_TypeDef*)hw->reg,  REECAN_MIR_INTERFACE_WR);	//Reset all MIR

	NVIC_EnableIRQ(hw->irq);

	/* Set default value for baud-rate*/
	CAN_SetBitTiming(( CAN_TypeDef*)hw->reg, 	can->bitrate, 1, 1, 1, 1);
	CAN_SetMode((CAN_TypeDef*)hw->reg, 			can->mode);

	/* IO and route*/
	GPIO_Unlock();
	CAN_SetRoute((CAN_TypeDef*)hw->reg, true, can->locationRx, can->locationTx);
	GPIO_PinModeSet(REEIO_PORT(can->tx), REEIO_PIN(can->tx), gpioModePushPull,1);
	GPIO_PinModeSet(REEIO_PORT(can->rx), REEIO_PIN(can->rx), gpioModeInput,1);

	CAN_Enable((CAN_TypeDef*)hw->reg, true);

	CAN_MessageObject_TypeDef receiver = {
		.id = 0,						// ID is ignored (for match-all)
		.dlc = 8,						// Data len = 8
		.mask = 0,				// Mask is Match-all (0)
		.directionMask = 0,
	};

	//CAN ID 11bit
	receiver.msgNum = hw->handle->canRxChannelReg;
	receiver.extended = false;
	receiver.extendedMask=false;
	CAN_ConfigureMessageObject(	(CAN_TypeDef*)hw->reg, REECAN_MIR_INTERFACE_WR, receiver.msgNum, true, false, false, false, true);
	CAN_SetIdAndFilter(			(CAN_TypeDef*)hw->reg, REECAN_MIR_INTERFACE_WR, true,			&receiver, true);

	//CAN ID 29bit
	receiver.msgNum = hw->handle->canRxChannelExt;
	receiver.extended = true;
	receiver.extendedMask=true;
	CAN_ConfigureMessageObject(	(CAN_TypeDef*)hw->reg, REECAN_MIR_INTERFACE_WR, receiver.msgNum, true, false, false, false, true);
	CAN_SetIdAndFilter(			(CAN_TypeDef*)hw->reg, REECAN_MIR_INTERFACE_WR, true,			&receiver, true);

	return REECAN_success;
	#else
	return REECAN_fail;
	#endif
}

REECAN_err_t REECAN_close(REECAN_t * can) {
	#ifdef CAN_PRESENT
	__REECANhw_t * hw 	= NULL;

	if (can == NULL) {
		return REECAN_null;
	}

	if(!__REECAN_isOwner(can)){
		return REECAN_denied;
	}

	/* Clean os service */
	hw->handle = NULL;
	hw->thread = _REECAN_THREAD_NONE;
	osTimerDelete(hw->timer);			/* If this fails we have a mem access problem... */

	/* release control */
	if (osOK != osMutexRelease(hw->mutex)) {
		return REECAN_fail;
	}

	/* Clean CAN peripheral */
	CAN_ResetMessages((CAN_TypeDef*)hw->reg,  REECAN_MIR_INTERFACE_WR);	/* Reset all MIR */
	NVIC_DisableIRQ(hw->irq);											/* Disable Interrupt */
	CAN_Enable((CAN_TypeDef*)hw->reg, false);							/* Disable peripheral */
	CAN_Reset((CAN_TypeDef*)hw->reg);									/* Reset Peripheral */
	CMU_ClockEnable(hw->clk, false);									/* Turn off peripheral clock */

	return REECAN_success;
	#else
	return REECAN_fail;
	#endif
}



REECAN_err_t REECAN_tx(REECAN_t * can, CAN_MessageObject_TypeDef *message, bool remoteTransfer){

	REECAN_err_t ret = REECAN_fail;

	#ifdef CAN_PRESENT

	if((can==0) || (message==0)){
		ret = REECAN_null;

	}else if(((message->msgNum) == 0) || ((message->msgNum)>32)){
		ret = REECAN_param;

	}else if (!__REECAN_isOwner(can)){
		ret = REECAN_denied;

	}else{

		/* Configure valid, TX/RX, remoteTransfer for a specific Message Object.*/
		CAN_ConfigureMessageObject(	can->candev,					/* A pointer to the CAN peripheral register block. */
									REECAN_MIR_INTERFACE_WR,		/* Indicate which Message Interface Register to use. */
									(message->msgNum),				/* A message number of this Message Object, [1 - 32].*/
									true,							/* True if the Message Object is valid, false otherwise. */
									true,							/* True if the Message Object is used for transmission, false if used for reception.*/
									remoteTransfer,					/* True if the Message Object is used for remote transmission, false otherwise. */
									true,							/* True if it is for a single Message Object or the end of a FIFO buffer, false if the Message Object is part of a FIFO buffer and not the last. */
									true);							/* If true, wait for the end of the transfer between the MIRx registers and the RAM to exit. If false, exit immediately, the transfer can still be in progress. */

		/* Set the ID and the filter for a specific Message Object. */
		CAN_SetIdAndFilter(	can->candev,				/* A pointer to the CAN peripheral register block.	*/
							REECAN_MIR_INTERFACE_WR,	/* Indicate which Message Interface Register to use.*/
							false,						/* A boolean to choose whether or not to use the masks.	*/
							message,					/* Message with filter,mask and ID*/
							true);

		CAN_SendMessage(	can->candev,				/* A pointer to the CAN peripheral register block.*/
							REECAN_MIR_INTERFACE_WR,	/* Indicate which Message Interface Register to use.*/
							message,					/* Message to send */
							true);						/* If true, wait for the end of the transfer between the MIRx registers and the RAM to exit. If false, exit immediately, the transfer can still be in progress. */

		#if REECAN_ENABLE_SWO_DEBUG
		{
		char temp[16];
		REEBUGSWO_String("REECAN_TX " );

		sprintf(temp, "ID(%08X) ", (unsigned int) message->id);
		REEBUGSWO_String(temp);

		for(uint8_t i=0; i< message->dlc; i++){
			sprintf(temp, "%02X ", message->data[i]);
			REEBUGSWO_String(temp);
		}
		REEBUGSWO_String("\n");
		}
		#endif

		ret = REECAN_success;
	}

	#endif

	return ret;	//TODO: add unblocking mecanism
}


/* TODO:: This is untested */
uint32_t REECAN_rx(REECAN_t * can, uint32_t messageId, bool extendedID, uint32_t channel, uint32_t filterMask,  uint8_t * src, uint8_t len){
	#ifdef CAN_PRESENT
	if ((can != NULL) && (src != NULL)  && (len>0)){

		messageId = (extendedID) ? (messageId & __REECAN_ID_29BIT_MASK) : (messageId &__REECAN_ID_11BIT_MASK);
		filterMask = (extendedID) ? (filterMask & __REECAN_ID_29BIT_MASK) : (filterMask &__REECAN_ID_11BIT_MASK);

		REECAN_messageObject_t message = {
			.id		= messageId,
			.mask	= filterMask,
			.extendedMask 	= (filterMask > 0),
			.dlc			= 8,				/* Data Length Code [0 - 8]. */
			.mirChannel		= channel,			/* msgNum A message number of this Message Object, [1 - 32]. */
			.direction_tx	= false,			/* Direction of the message, transmit if true, receive if false */
			.extended		= extendedID,		/* ID extended if true, standard if false. */
			.remoteTransfer = false,			/* True if the Message Object is used for remote transmission, false otherwise. */
			.endOfBuffer	= true,				/* True if it is for a single Message Object or the end of a FIFO buffer, false if the Message Object is part of a FIFO buffer and not the last. */

		};
		return __REECAN_transfer(can,  &message);
	}
	#endif
	return 0;
}

/* TODO:: This is untested */
uint32_t REECAN_trx(REECAN_t * can, uint32_t messageId, bool extendedID, uint32_t channel, uint32_t filterMask,  uint8_t * src, uint8_t len){
	#ifdef CAN_PRESENT
	if ((can != NULL) && (src != NULL)  && (len>0)){

		messageId  	= (extendedID) ? (messageId  & __REECAN_ID_29BIT_MASK) : (messageId  & __REECAN_ID_11BIT_MASK);
		filterMask 	= (extendedID) ? (filterMask & __REECAN_ID_29BIT_MASK) : (filterMask &__REECAN_ID_11BIT_MASK);

		REECAN_messageObject_t message = {
			.id		= messageId,
			.mask	= filterMask,
			.extendedMask 	= (filterMask > 0),
			.dlc			= 8,					/** Data Length Code [0 - 8].  */
			.mirChannel		= channel,				/** msgNum A message number of this Message Object, [1 - 32]. */
			.direction_tx	= false,				/** Direction of the message, transmit if true, receive if false*/
			.extended		= extendedID,			/** ID extended if true, standard if false.  */
			.remoteTransfer = true,					/** True if the Message Object is used for remote transmission, false otherwise.*/
			.endOfBuffer	= false,				/** True if it is for a single Message Object or the end of a FIFO buffer, false if the Message Object is part of a FIFO buffer and not the last. */
		};
		return __REECAN_transfer(can,  &message);
	}
	#endif
	return 0;
}

REECAN_err_t REECAN_ioctl(REECAN_t * can, REECAN_ioctl_t * args) {
	#ifdef CAN_PRESENT
	__REECANhw_t * hw 	= NULL;

	if ((can == NULL) || (args == NULL)) {
		return REECAN_null;

	} else if (!__REECAN_isOwner(can)) {
		return REECAN_denied;
	}

	hw = __REECAN_findHardware(can);
	if(hw==NULL) {
		return REECAN_notFound;
	}

	switch (args->action) {

		/* -- a. Set CAN Operating mode -- */
		case REECAN_act_setMode : {/* Set CAN operating mode, 		[value = (CAN_Mode_TypeDef) mode ]	*/
			#if REECAN_ENABLE_SWO_DEBUG

				REEBUGSWO_String("REECAN::setMode::" );
				switch(can->mode){
					case canModeNormal:			{ REEBUGSWO_String("normal\n\r");		break;}
					case canModeBasic:			{ REEBUGSWO_String("basic\n\r");		break;}
					case canModeLoopBack:		{ REEBUGSWO_String("loop\n\r");			break;}
					case canModeSilentLoopBack:	{ REEBUGSWO_String("silentloop\n\r");	break;}
					case canModeSilent:			{ REEBUGSWO_String("silent\n\r");		break;}
					default:					{ break;}
				}
			#endif
			CAN_SetMode(hw->reg, 		can->mode);
			return REECAN_success;
		}

		/* -- Change CAN baudrate -- */
		case REECAN_act_setBaudrate : {/* Set CAN operating Baud rate	[value = Baudrate] */
			CAN_SetBitTiming(hw->reg, 	can->bitrate, 1, 1, 1, 1);
			return REECAN_success;
		}

		default:		break;
	}
	#endif
	return REECAN_fail;
}

REECAN_err_t REECAN_hook(REECAN_t * can, REECAN_hook_t * hook) {
	#ifdef CAN_PRESENT
	// uint8_t i;

	// if ((can == NULL) || (hook == NULL)) {
	// 	return REECAN_null;
	// }

	// __REECANhw_t * hw = __REECAN_findHardware(can);

	// for(i=0; i< REECAN_HOOK_MAXCNT; i++){
	// 	if(hw->_hookList[i] == NULL){	//Hook slot available
	// 		hw->_hookList[i] = hook;
	// 		return REECAN_success;
	// 	}
	// }
	#endif
	return REECAN_full;	/* No more slot available */
}

REECAN_err_t REECAN_unhook(REECAN_t * can, REECAN_hook_t * hook) {
	#ifdef CAN_PRESENT
	// uint8_t i;

	// if ((can == NULL) || (hook == NULL)) {
	// 	return REECAN_null;
	// }

	// /* take control of hardware */
	// __REECANhw_t * hw = __REECAN_findHardware(can);
	// if (hw == NULL) {
	// 	return REECAN_notFound;
	// }

	// for(i=0; i< REECAN_HOOK_MAXCNT; i++){
	// 	if(hw->_hookList[i] == hook){	//Hook slot available
	// 		hw->_hookList[i] = 0;
	// 		return REECAN_success;
	// 	}
	// }
	#endif
	return REECAN_notFound;	/* No more slot available (really!?) */
}


uint8_t REECAN_getRxErrorCount(REECAN_t * can){
	if(can==NULL){
		return UINT8_MAX;
	}else{
		return 	((can->candev->ERRCNT) & _CAN_ERRCNT_REC_MASK) >> _CAN_ERRCNT_REC_SHIFT;
	}
}


/* -------------------------------- +
|									|
|	Interrupt service routine		|
|									|
+ ---------------------------------*/
#ifdef CAN_PRESENT

/*Interrupt only static variable*/
static uint8_t					 irq_i;
static uint8_t 					 irq_hookNumber;

static REECAN_hook_t 			*irq_ptr;
static CAN_MessageObject_TypeDef irq_message;
static REE_MIR_ARB_t 			*irq_arb;
static REECAN_messageObject_t 	 irq_reecanMessageObject;
#if REECAN_ENABLE_SWO_DEBUG
static char						 irq_temp[16];
#endif


__STATIC_INLINE void CANx_IRQ(CAN_TypeDef * CAN, __REECANhw_t * hw){
	if((hw!=NULL) && (hw->handle!=NULL) && (hw->handle->hookQty>0) && (hw->handle->hookList!=NULL)){
		irq_hookNumber = hw->handle->hookQty;
		irq_message.msgNum = 0;

		if(CAN->STATUS & (CAN_STATUS_RXOK | CAN_STATUS_TXOK)){

			for(irq_i=0; irq_i<33; irq_i++){
				if(hw->reg->MESSAGEDATA & (1<<irq_i)){

					irq_message.msgNum = irq_i+1;

					CAN_ReadMessage(CAN, REECAN_MIR_INTERFACE_RD, &irq_message);

					irq_arb = (REE_MIR_ARB_t*) &CAN->MIR[REECAN_MIR_INTERFACE_RD].ARB;

					if(irq_arb->xtd){
						irq_reecanMessageObject.extended = true;
						irq_reecanMessageObject.id = irq_arb->extended.id;
					}else{
						irq_reecanMessageObject.extended = false;
						irq_reecanMessageObject.id = irq_arb->standard.id;
					}

					irq_reecanMessageObject.isError = false;
					irq_reecanMessageObject.mask = irq_message.mask;
					irq_reecanMessageObject.canHandle = hw->handle;
					irq_reecanMessageObject.dlc = irq_message.dlc;
					irq_reecanMessageObject.mirChannel = irq_message.msgNum;
					irq_reecanMessageObject.direction_tx = (irq_message.msgNum > 2);
					memcpy(irq_reecanMessageObject.data, irq_message.data, 8);


					#if REECAN_ENABLE_SWO_DEBUG
					{
					REEBUGSWO_String("REECAN_RX " );

					sprintf(temp, "ID(%08X) ", (unsigned int) irq_reecanMessageObject.id);
					REEBUGSWO_String(temp);

					for(uint8_t i=0; i< message.dlc; i++){
						sprintf(temp, "%02X ", message.data[i]);
						REEBUGSWO_String(temp);
					}
					REEBUGSWO_String("\n");
					}
					#endif

					for(irq_i=0; irq_i<irq_hookNumber; irq_i++){
						irq_ptr = &hw->handle->hookList[irq_i];

						if(irq_ptr != 0){
							switch(irq_ptr->REECAN_hookType){

								case REECAN_hook_any: {
									irq_ptr->REECAN_callback(&irq_reecanMessageObject);
									break;
								}

								case REECAN_hook_rx: {			// = 1,		/* Called on any message reception */
									if (CAN->STATUS & CAN_STATUS_RXOK){
										irq_ptr->REECAN_callback(&irq_reecanMessageObject);
									}
									break;
								}
									case REECAN_hook_rxId:			// = 2,		/* Called on message reception matching CAN ID */
									case REECAN_hook_rxBroadcast:	// = 3,		/* Called on message reception matching broadcast pattern */
									case REECAN_hook_tx: {			// = 5,		/* Called on any message transmission*/
									if (CAN->STATUS & CAN_STATUS_TXOK){
										irq_ptr->REECAN_callback(&irq_reecanMessageObject);
									}
									break;
								}

								case REECAN_hook_txMIR:	{		// = 6,		/* Called on message transmission matching specified Message Interface Interface */
									break;
								}
								default:			break;
							}
						}
					}
				}
			}

		}else if(CAN->STATUS & _CAN_STATUS_LEC_MASK){
			irq_reecanMessageObject.isError = true;
			irq_reecanMessageObject.canHandle = hw->handle;
			irq_reecanMessageObject.id =  ((CAN->STATUS & _CAN_STATUS_LEC_MASK) >> _CAN_STATUS_LEC_SHIFT);
			for(irq_i=0; irq_i<irq_hookNumber; irq_i++){
				irq_ptr = &hw->handle->hookList[irq_i];
				if(irq_ptr != 0){
					if(irq_ptr->REECAN_hookType == REECAN_hook_error){
						irq_ptr->REECAN_callback(&irq_reecanMessageObject);
					}
				}
			}
		}
	}
	CAN_StatusClear(CAN,		0xFFFFFFFF);	/* can->STATUS */
	CAN_MessageIntClear(CAN, 	0xFFFFFFFF);	/* IF0IF */
	CAN_StatusIntClear(CAN,		0xFFFFFFFF);	/* IF1IF */
}

#if CAN_COUNT > 0

void CAN0_IRQHandler(void){
	CANx_IRQ(CAN0, &__REECAN_hardware[0]);
}
#endif


#if CAN_COUNT > 1
void CAN1_IRQHandler(void){
	CANx_IRQ(CAN1, &__REECAN_hardware[1]);
}
#endif

#endif

#pragma GCC diagnostic warning "-Wunused-function"			/* Re-enable those warning for future files */
#pragma GCC diagnostic warning "-Wunused-parameter"			/* Re-enable those warning for future files */
