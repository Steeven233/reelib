/*
	\name		REEPER.c
	\author		Laurence DV
	\date		2018-09-16
	\version	0.3.0
	\brief		
	\license	All right reserved RealEE inc. (2018)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEPER.h>


/* -------------------------------- +
|									|
|	Help string						|
|									|
+ -------------------------------- */
#define	_REEPER_HELP_PROTOCOL0		"REEPER Protocol V"
#define	_REEPER_HELP_PROTOCOL1		" Max payload "
#define	_REEPER_HELP_PROTOCOL2		"B"
#define	_REEPER_HELP_READ			"R[M,J,H] READ command"
#define	_REEPER_HELP_WRITE			"W[M,J,H] WRITE command"
#define	_REEPER_HELP_EXECUTE		"E[B,H] EXECUTE command"


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
#define	_REEPER_ANYSIGNAL				((uint32_t)0x0000)		/* Useful to wait any possible signals */
#define	_REEPER_ALLSIGNAL				((uint32_t)0xFFFF)		/* Useful to mask all possible signals */

#define	_REEPER_HEADERSIZE				(4)
#define	_REEPER_OPTIONSIZE				(2)
#define	_REEPER_OPTIONPOSITION			(2)						/* position of the option in the byte array representation of the header */
#define	_REEPER_CHECKSUMSIZE			(4)



/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */
//typedef enum {
//	_REEPER_state_unknow =	0,
//	_REEPER_state_active =	1,
//	_REEPER_state_error =	2,
//}_REEPER_state_t;
//
//typedef enum {
//	_REEPER_comState_idle =		0,
//	_REEPER_comState_param =	1,
//	_REEPER_comState_option =	2,
//	_REEPER_comState_payload =	3,
//	_REEPER_comState_checksum =	4,
//}_REEPER_comState_t;
//
//typedef union {
//	uint8_t					all[_REEPER_HEADERSIZE];
//	struct {
//		uint8_t				cmd;
//		uint8_t				param;
//		union {
//			uint16_t		option;
//			struct {
//				uint16_t	verbose:1;
//				uint16_t	checksumType:2;
//				uint16_t	:3;
//				uint16_t	payloadSize:10;
//			};
//		};
//	};
//}_REEPER_header_t;
//
//typedef union {
//	uint8_t 				all[REEPER_PROTOCOL_PAYLOADMAX+_REEPER_HEADERSIZE+_REEPER_CHECKSUMSIZE];
//	struct {
//		_REEPER_header_t	header;
//		uint8_t				payload[REEPER_PROTOCOL_PAYLOADMAX];
//		union {
//			uint32_t		value;
//			uint8_t			byte[_REEPER_CHECKSUMSIZE];
//		}checksum;
//	};
//}_REEPER_frame_t;


/* -------------------------------- +
|									|
|	Private Prototype				|
|									|
+ -------------------------------- */
//void __REEPER_flushBuffer(void);
//void __REEPER_comTimer_cb(void);
//void __REEPER_parseCom(void);
//void __REEPER_receiveCom(void);


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */
//static struct {
//	/* General */
//	_REEPER_state_t		state;
//	osTimerId			timer;
//
//	#if REEPER_COM == REEPER_COM_SERIAL
//	REEUART_t		com;
//	uint16_t(*getRxPending)(REEUART_t *);
//	uint16_t(*receive)(REEUART_t *, uint8_t *, uint16_t);
//	uint16_t(*send)(REEUART_t *, uint8_t *, uint16_t);
//	#endif
//
//	struct {
//		_REEPER_comState_t	state;
//		uint16_t			byteCnt;
//		_REEPER_frame_t		buffer;
//	}rx;
//}_REEPER = {
//	.state =		_REEPER_state_unknow,
//	.getRxPending =	NULL,
//	.receive = 		NULL,
//	.send = 		NULL,
//};

//osTimerDef(_REEPERcomTimer, __REEPER_comTimer_cb);


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */
void __REEPER_flushBuffer(void) {
//	_REEPER.rx.state = _REEPER_comState_idle;
//	_REEPER.rx.byteCnt = 0;
}

void __REEPER_comTimer_cb(void) {
//	__REEPER_flushBuffer();
}

#warning "TODO: REEPER, Populate all commands slot"
void __REEPER_parseCom(void) {
//	switch (_REEPER.rx.buffer.header.cmd) {
//		case REEPER_CMD_PROTOCOLHELP: {
//			uint8_t buf[8] = {};
//			//itoa(REEPER_PROTOCOL_PAYLOADMAX, buf);
//			_REEPER.send(&_REEPER.com, _REEPER_HELP_PROTOCOL0, sizeof(_REEPER_HELP_PROTOCOL0));
//			_REEPER.send(&_REEPER.com, REEPER_PROTOCOL_VER+'0', 1);
//			_REEPER.send(&_REEPER.com, _REEPER_HELP_PROTOCOL1, sizeof(_REEPER_HELP_PROTOCOL1));
//			_REEPER.send(&_REEPER.com, buf, sizeof(buf));
//			_REEPER.send(&_REEPER.com, _REEPER_HELP_PROTOCOL2, sizeof(_REEPER_HELP_PROTOCOL2));
//			break;
//		}
//		/* Read */
//		case REEPER_CMD_READ: {
//			switch (_REEPER.rx.buffer.header.param) {
//				/* Read memory */
//				case REEPER_PARAM_MEMORY: {
//					break;
//				}
//				/* Read a JSON file */
//				case REEPER_PARAM_JSON: {
//					break;
//				}
//				/* Reply with "read" help string */
//				case REEPER_PARAM_HELP: {
//					_REEPER.send(&_REEPER.com, _REEPER_HELP_READ, sizeof(_REEPER_HELP_READ));
//					break;
//				}
//				/* Unsupported */
//				default:	break;
//			}
//			break;
//		}
//		/* Write */
//		case REEPER_CMD_WRITE: {
//			switch (_REEPER.rx.buffer.header.param) {
//				/* Write memory */
//				case REEPER_PARAM_MEMORY: {
//					break;
//				}
//				/* Write a JSON file */
//				case REEPER_PARAM_JSON: {
//					break;
//				}
//				/* Reply with "write" help string */
//				case REEPER_PARAM_HELP: {
//					_REEPER.send(&_REEPER.com, _REEPER_HELP_WRITE, sizeof(_REEPER_HELP_WRITE));
//					break;
//				}
//				/* Unsupported */
//				default:	break;
//			}
//			break;
//		}
//		/* Execute */
//		case REEPER_CMD_EXECUTE: {
//			switch (_REEPER.rx.buffer.header.param) {
//				/* Reboot MCU */
//				case REEPER_PARAM_REBOOT: {
//					/* Call whatever is able to reboot the MCU */
//					break;
//				}
//				/* Reply with "execute" help string */
//				case REEPER_PARAM_HELP: {
//					_REEPER.send(&_REEPER.com, _REEPER_HELP_EXECUTE, sizeof(_REEPER_HELP_EXECUTE));
//					break;
//				}
//				/* Unsupported */
//				default:	break;
//			}
//			break;
//		}
//		default:		break;
//	}
}

void __REEPER_receiveCom(void) {
//	uint16_t byteNb = 0;
//
//	while (_REEPER.getRxPending(&_REEPER.com)) {
//
//		/* We received something */
//		osTimerStop(_REEPER.timer);
//
//		switch (_REEPER.rx.state) {
//			/* Waiting for command */
//recheck_command: case _REEPER_comState_idle: {
//				/* Get the command */
//				if (1 == _REEPER.receive(&_REEPER.com, &_REEPER.rx.buffer.header.cmd, 1)) {
//					/* Check for valid command */
//					if ((_REEPER.rx.buffer.header.cmd == REEPER_CMD_READ) ||
//						(_REEPER.rx.buffer.header.cmd == REEPER_CMD_WRITE) ||
//						(_REEPER.rx.buffer.header.cmd == REEPER_CMD_EXECUTE)) {
//						_REEPER.rx.state = _REEPER_comState_param;
//					} else if (_REEPER.rx.buffer.header.cmd == REEPER_CMD_PROTOCOLHELP) {
//						__REEPER_parseCom();	/* Help asked, act immediately */
//					}
//				}
//				break;
//			}
//			/* Receiving params */
//			case _REEPER_comState_param: {
//				/* Get the command */
//				if (1 == _REEPER.receive(&_REEPER.com, &_REEPER.rx.buffer.header.param, 1)) {
//					/* Check for valid command */
//					if ((_REEPER.rx.buffer.header.param == REEPER_PARAM_HELP) ||
//						(_REEPER.rx.buffer.header.param == REEPER_PARAM_REBOOT) ||
//						(_REEPER.rx.buffer.header.param == REEPER_PARAM_MEMORY) ||
//						(_REEPER.rx.buffer.header.param == REEPER_PARAM_JSON)) {
//						_REEPER.rx.state = _REEPER_comState_option;
//						_REEPER.rx.byteCnt = 0;
//					/* Invalid param, but maybe valid command */
//					} else {
//						_REEPER.rx.state = _REEPER_comState_idle;
//						goto recheck_command;/* Recheck this char with previous state*/
//					}
//				}
//				break;
//			}
//			/* Receiving options */
//			case _REEPER_comState_option: {
//				/* Get 1 char */
//				if (1 == _REEPER.receive(&_REEPER.com, (uint8_t*)(&_REEPER.rx.buffer.header.all[_REEPER_OPTIONPOSITION+_REEPER.rx.byteCnt]), 1)) {
//					_REEPER.rx.byteCnt++;
//				}
//
//				/* Until all char have been received */
//				if (_REEPER.rx.byteCnt >= _REEPER_OPTIONSIZE) {
//					_REEPER.rx.byteCnt = 0;
//					/* Payload will be present */
//					if ((uint16_t)_REEPER.rx.buffer.header.payloadSize > 0) {
//						_REEPER.rx.state = _REEPER_comState_payload;
//					/* No payload, nor checksum */
//					} else {
//						__REEPER_parseCom();	/* Quick command without payload given, act immediately */
//					}
//				}
//
//				break;
//			}
//			/* Receiving payload */
//			case _REEPER_comState_payload: {
//				/* Receive only the minimum of byte necessary */
//				byteNb = REEMIN((uint16_t)_REEPER.rx.buffer.header.payloadSize, _REEPER.getRxPending(&_REEPER.com));
//				_REEPER.rx.byteCnt += _REEPER.receive(&_REEPER.com, (uint8_t*)(&_REEPER.rx.buffer.payload[_REEPER.rx.byteCnt]), byteNb);
//
//				/* We received all the payload */
//				if (_REEPER.rx.byteCnt >= ((uint16_t)_REEPER.rx.buffer.header.payloadSize)) {
//					_REEPER.rx.byteCnt = 0;
//					_REEPER.rx.state = _REEPER_comState_checksum;
//				}
//				break;
//			}
//			/* Receiving checksum */
//			case _REEPER_comState_checksum: {
//				/* Receive only the minimum of byte necessary */
//				byteNb = REEMIN(_REEPER_CHECKSUMSIZE, _REEPER.getRxPending(&_REEPER.com));
//				_REEPER.rx.byteCnt += _REEPER.receive(&_REEPER.com, (uint8_t*)(&_REEPER.rx.buffer.checksum.byte[_REEPER.rx.byteCnt]), byteNb);
//
//				/* We finished to received everything */
//				if (_REEPER.rx.byteCnt >= _REEPER_CHECKSUMSIZE) {
//					__REEPER_parseCom();		/* Handle the frame */
//					__REEPER_flushBuffer();		/* Reset for next frame */
//
//					/* If nothing else pending, stop! */
//					if (!_REEPER.getRxPending(&_REEPER.com)) {
//						return;
//					}
//				}
//				break;
//			}
//			default:	break;
//		}
//
//		/* Start counting for timeout */
//		osTimerStart(_REEPER.timer, REEPER_INTERBYTETIMEOUT_MS);
//	}
}


/* -------------------------------- +
|									|
|	Thread							|
|									|
+ -------------------------------- */
// osMutexDef(_REEFConfMutex);

void REEPER_thread(void const *argument) {
//	(void)argument;			/* remove warning */
//	bool run = true;
//	osEvent signalInput;
//
//	/* Init */
//	REEPER_init();
//
//	/* Loop */
//	while (run) {
//		/* 0. Wait for any signals */
//		signalInput = osSignalWait(_REEPER_ANYSIGNAL, REEPER_THREADMAXPERIOD_MS);
//		if (signalInput.status == osEventSignal) {
//			REEPER_sig_t receivedSig = (REEPER_sig_t)(signalInput.value.signals);	/* Signals are auto-cleared, so local copy needed */
//
//		/* 1. Decode and process all signals */
//			while (receivedSig) {
//				/* Poking signal */
//				if (receivedSig & REEPER_sig_poke) {
//					CLR_BIT(receivedSig, REEPER_sig_poke);	/* Signal treated */
//				}
//				/* Clear any remaining signals (invalid) */
//				CLR_BIT(receivedSig, _REEPER_ALLSIGNAL);
//			}
//		}
//		/* 2. Check com */
//		__REEPER_receiveCom();
//
//		/* 3. Prepare to sleep */
//		nop();
//	}
}

/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */
void REEPER_init(void) {
//
//	/* If already init force shutdown */
//	if (_REEPER.state == _REEPER_state_active) {
//		_REEPER.state = _REEPER_state_unknow;
//	}
//
//	/* RAM */
//	// _REEF.confLock = osMutexCreate(osMutex(_REEFConfMutex));
//	// if (_REEF.confLock == NULL) {
//	// 	_REEF.state = _REEF_state_error;
//	// 	return;
//	// }
//
//	/* COM */
//	#if (REEPER_COM == REEPER_COM_SERIAL)
//	_REEPER.getRxPending = REEUART_GetRxPending;
//	_REEPER.receive = REEUART_Receive;
//	_REEPER.send = REEUART_Send;
//	_REEPER.com.dev = REEPER_DEV;
//	_REEPER.com.clk = REEPER_CLK;
//	_REEPER.com.baudRate = REEPER_BITRATE;
//	_REEPER.com.tx.irq = REEPER_IRQTX;
//	_REEPER.com.tx.loc = REEPER_LOCTX;
//	_REEPER.com.tx.port = REEPER_TX_PORT;
//	_REEPER.com.tx.pin = REEPER_TX_PIN;
//	_REEPER.com.rx.irq = REEPER_IRQRX;
//	_REEPER.com.rx.loc = REEPER_LOCRX;
//	_REEPER.com.rx.port = REEPER_RX_PORT;
//	_REEPER.com.rx.pin = REEPER_RX_PIN;
//	REEUART_Init(&_REEPER.com);
//	#endif
//
//	__REEPER_flushBuffer();
//	memset(_REEPER.rx.buffer.all, 0, sizeof(_REEPER_frame_t));
//
//	/* COM Timeout timer */
//	_REEPER.timer = osTimerCreate(osTimer(_REEPERcomTimer), osTimerOnce, 0);
//
//	_REEPER.state = _REEPER_state_active;
}

void * REEPER_GetComHandle(void) {
//	#if (REEPER_COM == REEPER_COM_SERIAL)
//		return (void*)(&_REEPER.com);
//	#else
//		return NULL;
//	#endif
}

#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
