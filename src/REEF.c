/*
	@name		REEF.c
	@author		Laurence DV
	@version	2.0.0
	@brief		REEF Human Interface, enable simple human interaction within an CMSIS RTOS
				through peripherals, currently supported:
					* Muxed momentary push-button
					* Muxed LEDs
					* Muxed RGB LEDs
					* RGB Addressable string
					* OLED matrix display
	@note		
	@license	All right reserved RealEE inc. (2019)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEF.h>

#include <em_device.h>		/* for HW module addresses */
#include <em_cmu.h>			/* Clock access */
#include <em_core.h>		/* ATOMIC operation */
#include <em_ldma.h>		/* LDMA access */

#include <REEBUG.h>			/* REEBUG fct are empty stub when symbol DEBUG is not defined */

#include <REEFled.h>		/* Support REEFled coral type */


/* -------------------------------- +
|									|
|	Default config loading			|
|									|
+ -------------------------------- */
#ifndef REEF_INITIAL_FPS
	#define REEF_INITIAL_FPS			(REEF_INITIAL_FPS_DEF)
#endif
#ifndef REEF_CORAL_TOTALNB
	#define	REEF_CORAL_TOTALNB			(REEF_CORAL_TOTALNB_DEF)
#endif


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
static struct __attribute__((packed)) {
	struct{
		uint8_t				rate;		/* Current frame-rate in fps */
		uint32_t			cnt;		/* frame id since last boot, wraps after 194,9 days at 255fps */
	}frame;

	struct{
		const uint8_t		totalNb;	/* Total maximum number of corals in this reef */
		volatile uint8_t	activeNb;	/* Current active corrals in this reef */
		REEFcoral_t			list[REEF_CORAL_TOTALNB];
	}coral;

	#ifdef REELIB_USE_CMSISRTOS2
	struct {
		const osThreadAttr_t conf;
		osRtxThread_t		cb;
		osThreadId_t		id;
	}thread;
	struct {
		const osTimerAttr_t conf;
		osRtxTimer_t		cb;
		osTimerId_t			id;
	}timer;
	struct {
		const osMutexAttr_t	conf;
		osRtxMutex_t		cb;
		osMutexId_t			id;
	}lock;
	#endif
}__REEF={
	.coral.totalNb =		REEF_CORAL_TOTALNB,
	.coral.activeNb =		0,

	#ifdef REELIB_USE_CMSISRTOS2
	.thread.conf.name =		"REEF_thd",
	.thread.conf.priority =	osPriorityAboveNormal,
	.thread.conf.cb_mem =	&__REEF.thread.cb,
	.thread.conf.cb_size =	sizeof(__REEF.thread.cb),
	.timer.conf.name =		"REEF_tmr",
	.timer.conf.attr_bits =	0,
	.timer.conf.cb_mem =	&__REEF.timer.cb,
	.timer.conf.cb_size =	sizeof(__REEF.timer.cb),
	.lock.conf.name =		"REEF_mtx",
	.lock.conf.attr_bits =	osMutexRecursive | osMutexPrioInherit | osMutexRobust,
	.lock.conf.cb_mem =		&__REEF.lock.cb,
	.lock.conf.cb_size =	sizeof(__REEF.lock.cb),
	#endif
};


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */
void __REEF_frameTimer_cb(void const *argument) {
	(void)argument;
	#ifdef REELIB_USE_CMSISRTOS2
	osThreadFlagsSet(__REEF.thread.id, REEFflag_newFrame);
	#else
	#error "NOT IMPLEMENTED: REEF_frameTimer_cb baremetal mode"
	#endif
}


/* -------------------------------- +
|									|
|	BareMetal API					|
|									|
+ -------------------------------- */
/* Main init */
void REEF_init(void) {
	/* Start Framing timer */
	#ifdef REELIB_USE_CMSISRTOS2
	__REEF.timer.id = osTimerNew(__REEF_frameTimer_cb, osTimerPeriodic, NULL, &__REEF.timer.conf);
	osTimerStart(__REEF.timer.id, OS_TICK_FREQ/__REEF.frame.rate);		/* CB at the correct frame rate */
	#else
	#error "NOT IMPLEMENTED: REEF framing timer init"
	#endif

	/* Initialize all corals */
	for (uint8_t i=0; i<__REEF.coral.totalNb; i++) {
		nop();
	}
}

/* Execute a new frame */
void REEF_newFrame(void) {
	__REEF.frame.cnt++;		/* Start by counting it */

}


/* -------------------------------- +
|									|
|	RTOS API						|
|									|
+ -------------------------------- */
void __REEF_thread(void *argument) {
	(void)argument;			/* remove warning */

	#ifdef REELIB_USE_CMSISRTOS2
	bool run = true;

	/* Init */
	REEF_init();

	/* Loop */
	while (run) {
		/* 0. -- Wait for any flags -- */
		osThreadFlagsWait(REEFflag_ALL, osFlagsWaitAny|osFlagsNoClear, osWaitForever);
		REEFflag_t flags = (REEFflag_t)osThreadFlagsGet();	/* Signals must be cleared, so local copy needed */
		osThreadFlagsClear(REEFflag_ALL);

		/* 1. -- Decode&Exec in pritority order -- */
		/* A. Stop everything and shutdown all power sources */
		if (flags & REEFflag_stop) {

		/* B. New frame starting */
		} else if (flags & REEFflag_newFrame) {
			REEF_newFrame();
		}
	}
	#else
	/* Silence! for the BareMetal god */
	#endif
}

void REEF_startThread(void) {
	#ifdef REELIB_USE_CMSISRTOS2
	__REEF.thread.id = osThreadNew(__REEF_thread, NULL, &__REEF.thread.conf);
	#else
	/* Silence! for the BareMetal god */
	#endif
}


/*--------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */


#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
