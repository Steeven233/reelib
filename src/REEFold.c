/*
	\name		REEF.c
	\author		Laurence DV
	\date		2018-08-15
	\version	0.3.0
	\brief		REEF Human Interface, enable simple human interaction within an CMSIS RTOS
				through momentary push-button, LEDs, RGB LEDs and SPI OLAD matrix display
	\note		This source file quite long an editor with complete code-folding support! (ex: SublimeText 3)
	\license	All right reserved RealEE inc. (2018)
*/
#warning "@LaurenceDV TODO: adapt the size of RGBSTR pixelBuffer on the current pixel nb as to lessen the cpu usage (call to dma done irq mostly)"


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEF.h>
#include <REEBUG.h>
#include <string.h>


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
#define	_REEF_ANYSIGNAL				((uint32_t)0x0000)		/* Useful to wait any possible signals */
#define	_REEF_ALLSIGNAL				((uint32_t)0xFFFF)		/* Useful to mask all possible signals */

#define	_REEF_DEFAULTPERIOD_MS		(500)					/* Default LED,RGB,etc cycling period (in millisec) */
#define	__REEF_STACKSIZE			(1024)

#define	_REEF_RGBMUX_TOP			MAX( 2, ((REEF_RGBPWMFREQ_HZ / REEF_MUXRATE_HZ) /REEF_RGBNB) )		/* At least 2 TIMER OVF per RGBs */

/* Official timing spec is 800kbps with T0L = 900ns, but this is too fast for
	CM4 CPU@38MHz with current pixel encoding function and no optimisation.
	But it is working correctyly when using GCC 4.9 -O3, therefore there is
	a relaxed timing definition when in Debug mode typically using -O0 */
#define	_WS2812B_T0H_NS				(350)							/* High time for a serial "0" */
#ifdef DEBUG
	#define	_WS2812B_T0L_NS			(900)							/* Low time for a serial "0" */
#else
	#define	_WS2812B_T0L_NS			(900)							/* Official spec */
#endif
#define	_WS2812B_RESET_NS			(50000)								/* minimum time for a Reset sequence */
#define	_WS2812B_RESETSEQ_PIXNB		(3)									/* equivalent nb of pixel for the Reset seq */
#define	_WS2812B_BITPERIOD_NS		(_WS2812B_T0H_NS+_WS2812B_T0L_NS)	/* Total time of a bit on the WS2812B data line */
#define	_WS2812B_BYTEPERIOD_NS		(_WS2812B_BITPERIOD_NS * 8)			/* Total time of a byte on the WS2812B data line */
#define	_WS2812B_PIXELPERIOD_NS		(_WS2812B_BYTEPERIOD_NS * 3)		/* Total time of a pixel on the WS2812B data line */
#define	_WS2812B_PIXELPERRESETSEQ	((_WS2812B_RESET_NS / _WS2812B_PIXELPERIOD_NS)+1)	/* Equivalent number of pixel data lenght for a reset sequence */
#define	_WS2812B_BITPERSPIBYTE		(2)
#define	_WS2812B_SPIBYTEPERRGBBYTE	(8/_WS2812B_BITPERSPIBYTE)
#define _REEF_RGBSTR_ENCODEMASK		(0b1111)
#define	_REEF_RGBSTR_ENCODEMASKWIDTH (4)
#define	_REEF_RGBSTR_SPIBITRATE		((1000000000.0/_WS2812B_BITPERIOD_NS) * _WS2812B_SPIBYTEPERRGBBYTE)
#define	_REEF_RGBSTR_SPIBYTEPERPIX	(((3 * 8) * _WS2812B_BITPERSPIBYTE) / _WS2812B_SPIBYTEPERRGBBYTE)	/* 3x color of 8bit with encoding ratio */

#define	_WS2812B_SPICODE_00			(0b10001000)
#define	_WS2812B_SPICODE_01			(0b10001110)
#define	_WS2812B_SPICODE_10			(0b11101000)
#define	_WS2812B_SPICODE_11			(0b11101110)


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */
typedef enum {
	_REEF_state_unknow =	0,
	_REEF_state_active =	1,
	_REEF_state_error =		2,
}_REEF_state_t;

typedef struct __attribute__((packed)){
	uint8_t		red;
	uint8_t		green;
	uint8_t		blue;
}__REEFrgb_t;




/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */
static volatile uint64_t __REEFstack[__REEF_STACKSIZE] = {};

static struct {
	/* General */
	_REEF_state_t		state;
	osThreadId_t		threadId;
	osTimerId_t			frameTimer;
	osMutexId_t			confLock;

	/* LEDs */
	#if (REEF_LEDNB > 0)
	struct {
		REEFLEDmode_t	mode;
		REEFLEDmode_t	newMode;
		uint16_t		cnt;
		uint16_t		period;
	}LED[REEF_LEDNB];
	#endif

	/* RGBs */
	#if (REEF_RGBNB > 0)
	struct {
		REEFRGBmode_t	mode;
		REEFRGBmode_t	newMode;
		uint16_t		cnt;
		uint16_t		period;
		uint16_t 		redReg;
		uint16_t 		greenReg;
		uint16_t 		blueReg;
		REEFrgb_t		colorA;
		REEFrgb_t		colorB;
	}RGB[REEF_RGBNB];
	struct {
		uint8_t 		ID;
		uint8_t 		muxCnt;
		uint16_t 		scaleFactor;
	}RGBctl;
	#endif

	/* RGB String */
	#if (REEF_RGBSTRPIXNB > 0)
	struct {
		LDMA_Descriptor_t	DMAdesc[2];
		LDMA_TransferCfg_t 	DMAtrig;
		REEFrgb_t			frameBuf[REEF_RGBSTRPIXNB];				/* First stage of frame buffering, original pixel data stored */
		REEFrgb_t			realBuf[REEF_RGBSTRPIXNB];				/* Second stage of buffering, all effects are applied now */
		uint16_t			alpha;
		uint8_t				pixBuf[2][_REEF_RGBSTR_SPIBYTEPERPIX];
		int16_t				pixCnt;
	}RGBSTR;
	#endif

	/* Buttons */
	#if (REEF_BTNNB > 0)
	struct {
		bool			state;
		uint32_t		cnt;
		REEFbtnCb_t		pressCb[REEF_BTNCBNB];
		REEFbtnCb_t		shortCb[REEF_BTNCBNB];
		REEFbtnCb_t		longCb[REEF_BTNCBNB];
		REEFbtnCb_t		releaseCb[REEF_BTNCBNB];
		REEFbtnCb_t		periodicCb[REEF_BTNCBNB];
	}BTN[REEF_BTNNB];
	#endif

	/* Display */
	#if (REEF_DISPEN == 1)
	#endif

	/* Render */
	struct {
		renderCb_t		cb[REEF_RENDERCBNB];
		void * 			args[REEF_RENDERCBNB];
	}render;
}_REEF = {
	.state = _REEF_state_unknow,
	#if (REEF_RGBSTRPIXNB > 0)
	.RGBSTR.DMAtrig = LDMA_TRANSFER_CFG_PERIPHERAL(ldmaPeripheralSignal_USART3_TXBL),
	#warning "CRITICAL TODO: DMA trigger is fixed to USART3 for now..."
	#endif
};

static osRtxThread_t __REEFThreadTcb;

static const osThreadAttr_t __REEFThreadConf = {
	.name = "REEF_thd",
	.cb_mem = &__REEFThreadTcb,
	.cb_size = sizeof(__REEFThreadTcb),
	.stack_mem = &__REEFstack,
	.stack_size = sizeof(__REEFstack),
	.priority = osPriorityNormal7,
};

static const osTimerAttr_t __REEFTimerConf = {
	.name = "REEF_tmr",
};

static const osMutexAttr_t _REEFMutexConf = {
	.name = "REEFconf_mtx",
		osMutexRecursive | osMutexPrioInherit | osMutexRobust,
		NULL,
		0U,
};

#if (REEF_RGBSTRPIXNB > 0)
static const uint8_t _REEF_RGBSTRresetSeq[2][_REEF_RGBSTR_SPIBYTEPERPIX] =
	{	{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, },
		{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02},};
#endif


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */
#if REEF_LED_ACTIVE == (1)
	#define	__REEF_ACTIVATE_LED(x)		(GPIO_PinOutSet(REEFpins.LED[x].port, REEFpins.LED[x].pin))
	#define	__REEF_DISACTIVATE_LED(x)	(GPIO_PinOutClear(REEFpins.LED[x].port, REEFpins.LED[x].pin))
#else
	#define	__REEF_ACTIVATE_LED(x)		(GPIO_PinOutClear(REEFpins.LED[x].port, REEFpins.LED[x].pin))
	#define	__REEF_DISACTIVATE_LED(x)	(GPIO_PinOutSet(REEFpins.LED[x].port, REEFpins.LED[x].pin))
#endif

#if (REEF_RGBCOM_ACTIVE == (1) && (REEF_RGBNB > 0))
	#define	__REEF_ACTIVATE_RGBCOM(x)		(GPIO_PinOutSet(REEFpins.RGB.COM[x].port, REEFpins.RGB.COM[x].pin))
	#define	__REEF_DISACTIVATE_RGBCOM(x)	(GPIO_PinOutClear(REEFpins.RGB.COM[x].port, REEFpins.RGB.COM[x].pin))
#elif ((REEF_RGBCOM_ACTIVE == (0)) && (REEF_RGBNB > 0))
	#define	__REEF_ACTIVATE_RGBCOM(x)		(GPIO_PinOutClear(REEFpins.RGB.COM[x].port, REEFpins.RGB.COM[x].pin))
	#define	__REEF_DISACTIVATE_RGBCOM(x)	(GPIO_PinOutSet(REEFpins.RGB.COM[x].port, REEFpins.RGB.COM[x].pin))
#else
	#define	__REEF_ACTIVATE_RGBCOM(x)		((void)x)
	#define	__REEF_DISACTIVATE_RGBCOM(x)	((void)x)
#endif

#define	__REEF_MS2FRAMECNT(ms)		(ms / (1000UL/REEF_REFRESHRATE_HZ))
#define	__REEF_FRAMECNT2MS(cnt)		(cnt * (1000UL/REEF_REFRESHRATE_HZ))


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */
/* ============= */
/* == General == */
/* ============= */
void __REEF_frameTimer_cb(void const *argument) {
	osThreadFlagsSet(_REEF.threadId, REEF_sig_execFrame|REEF_sig_render);
}

/* ========= */
/* == BTN == */
/* ========= */
/* Init */
static void __REEF_BTNinit(void) {
	#if (REEF_BTNNB > 0)
	uint16_t i;
	for(i=0; i<REEF_BTNNB; i++){
		GPIO_PinModeSet(REEFpins.BTN[i].port, REEFpins.BTN[i].pin, REEFpins.BTN[i].mode, (uint32_t)!REEF_BTN_ACTIVE);
		_REEF.BTN[i].state = false;
		uint8_t j;
		for(j=0; j<REEF_BTNCBNB; j++) {
			_REEF.BTN[i].pressCb[j] = NULL;
			_REEF.BTN[i].shortCb[j] = NULL;
			_REEF.BTN[i].longCb[j] = NULL;
			_REEF.BTN[i].releaseCb[j] = NULL;
			_REEF.BTN[i].periodicCb[j] = NULL;
		}
	}
	#endif	
}

/* Execute BTN callbacks */
static void __REEF_BTNexecCb(REEFbtnCb_t cbList[], uint32_t arg) {
	#if (REEF_BTNNB > 0)
	/* Scan through the table */
	uint8_t j;
	for (j=0; j<REEF_BTNCBNB; j++) {
		/* Stop at first NULL cb */
		if (cbList[j] == NULL) {
			break;
		}

		/* Execute CB */
		cbList[j](arg);
	}
	#endif
}

/* Add a BTN callback to the list */
static void __REEF_BTNappendCb(REEFbtnCb_t cbList[], REEFbtnCb_t newCb) {
	#if (REEF_BTNNB > 0)
	/* Scan through the table */
	uint8_t j;
	for (j=0; j<REEF_BTNCBNB; j++) {
		/* Append at first empty slot */
		if (cbList[j] == NULL) {
			cbList[j] = newCb;
			return;
		}
	}
	#endif
}

/* Update all button states */
static void __REEF_BTNupdate(void) {
	#if (REEF_BTNNB > 0)
	uint32_t i;
	uint8_t btn = 0;
	uint8_t state = 0;
	for (i=0; i<REEF_BTNNB; i++) {
		#if REEF_BTN_ACTIVE == 1
			btn = GPIO_PinInGet(REEFpins.BTN[i].port, REEFpins.BTN[i].pin) & 0b1;
		#else
			btn = !GPIO_PinInGet(REEFpins.BTN[i].port, REEFpins.BTN[i].pin) & 0b1;
		#endif
		state = btn | ((_REEF.BTN[i].state & 0b1) << 1);
		/* ------ EDGE DETECTION ------ */
		switch (state) {
			/* A -- nothing was pressed -- */
			/*     __ */
			case 0b00: {
				_REEF.BTN[i].cnt = 0;
				
				break;
			}
			/* B -- button pressed! yeah something to do! -- */
			/*     _/ */
			case 0b01: {
				_REEF.BTN[i].cnt = 1;
				__REEF_BTNexecCb(_REEF.BTN[i].pressCb, (uint32_t)REEF_BTNpress);
				break;
			}
			/* C -- button released, finally -- */
			/*     -\ */
			case 0b10: {
				_REEF.BTN[i].cnt = 0;
				__REEF_BTNexecCb(_REEF.BTN[i].releaseCb, (uint32_t)REEF_BTNrelease);
				break;
			}
			/* D -- button kept pressed... human are so slow -- */
			/*     -- */
			case 0b11: {
				_REEF.BTN[i].cnt++;
				if (_REEF.BTN[i].cnt == __REEF_MS2FRAMECNT(REEF_BTNSHORTPRESS_MS)) {
					__REEF_BTNexecCb(_REEF.BTN[i].shortCb, (uint32_t)REEF_BTNshort);
					
				} else if (_REEF.BTN[i].cnt == __REEF_MS2FRAMECNT(REEF_BTNLONGPRESS_MS)) {
					__REEF_BTNexecCb(_REEF.BTN[i].longCb, (uint32_t)REEF_BTNlong);
				}

				/* when the modulo result is 0, we are on the period edge */
				if (!(_REEF.BTN[i].cnt % __REEF_MS2FRAMECNT(REEF_BTNPERIODIC_MS))) {
					__REEF_BTNexecCb(_REEF.BTN[i].periodicCb, (uint32_t)REEF_BTNperiodic);
				}
				break;
			}
		}
		/* ---------------------------- */

		_REEF.BTN[i].state = btn;
	}
	#endif
}

/* ========= */
/* == LED == */
/* ========= */
static void __REEF_LEDinit(void) {
	#if (REEF_LEDNB > 0)
	uint16_t i;
	for(i=0; i<REEF_LEDNB; i++){
		GPIO_PinModeSet(REEFpins.LED[i].port, REEFpins.LED[i].pin, REEFpins.LED[i].mode, (uint32_t)!REEF_LED_ACTIVE);
		_REEF.LED[i].mode = REEF_LEDoff;
		_REEF.LED[i].cnt = 0;
		_REEF.LED[i].period = __REEF_MS2FRAMECNT(_REEF_DEFAULTPERIOD_MS);
	}
	#endif
}

/* update all LEDs values */
static void __REEF_LEDupdate(void) {
	#if (REEF_LEDNB > 0)
	uint32_t i;
	for (i=0; i<REEF_LEDNB; i++) {
		/* State sync */
		if (_REEF.LED[i].mode != _REEF.LED[i].newMode) {
			_REEF.LED[i].cnt = 0;
			_REEF.LED[i].mode = _REEF.LED[i].newMode;
		}

		/* State exec */
		switch(_REEF.LED[i].mode) {
			/* ------- SOLID OFF ------ */
			/*	__						*/
			/*	 |_____________________	*/
			/* ------------------------ */
			case REEF_LEDoff: {
				__REEF_DISACTIVATE_LED(i);
				break;
			}
			/* ------- SOLID ON ------- */
			/*	 ______________________	*/
			/*	_|  					*/
			/* ------------------------ */
			case REEF_LEDon: {
				__REEF_ACTIVATE_LED(i);
				break;
			}
			/* -------- BLINKs -------- */
			/*	 ____   ____   ____   	*/
			/*	_|  |___|  |___|  |___	*/
			/* ------------------------ */
			case REEF_LEDblink: {
				_REEF.LED[i].cnt++;
				/* wrap around */
				if (_REEF.LED[i].cnt >= _REEF.LED[i].period) {
					_REEF.LED[i].cnt = 0;
					__REEF_ACTIVATE_LED(i);
				/* First half period */
				} else if (_REEF.LED[i].cnt < (_REEF.LED[i].period/2) ) {
					__REEF_ACTIVATE_LED(i);
				/* Last half period */
				} else {
					__REEF_DISACTIVATE_LED(i);
				}
				break;
			}
		}
	}
	#endif
}

/* ========= */
/* == RGB == */
/* ========= */
static void __REEF_RGBinit(void) {
	#if (REEF_RGBNB > 0)
	uint8_t i;

	CMU_ClockEnable(REEF_TIMER_CLK, true);

	/* IOs & CTL */
	GPIO_PinModeSet(REEFpins.RGB.R.port, REEFpins.RGB.R.pin, REEFpins.RGB.R.mode, (uint32_t)!REEF_RGB_ACTIVE);
	GPIO_PinModeSet(REEFpins.RGB.G.port, REEFpins.RGB.G.pin, REEFpins.RGB.G.mode, (uint32_t)!REEF_RGB_ACTIVE);
	GPIO_PinModeSet(REEFpins.RGB.B.port, REEFpins.RGB.B.pin, REEFpins.RGB.B.mode, (uint32_t)!REEF_RGB_ACTIVE);
	for(i=0; i<REEF_RGBNB; i++){
		GPIO_PinModeSet(REEFpins.RGB.COM[i].port, REEFpins.RGB.COM[i].pin, REEFpins.RGB.COM[i].mode, (uint32_t)!REEF_RGBCOM_ACTIVE);
		_REEF.RGB[i].mode = REEF_RGBoff;
		_REEF.RGB[i].colorA.red = 0;
		_REEF.RGB[i].colorA.green = 0;
		_REEF.RGB[i].colorA.blue = 0;
		_REEF.RGB[i].colorB.red = 0;
		_REEF.RGB[i].colorB.green = 0;
		_REEF.RGB[i].colorB.blue = 0;
		_REEF.RGB[i].cnt = 0;
		_REEF.RGB[i].period = __REEF_MS2FRAMECNT(_REEF_DEFAULTPERIOD_MS);
	}

	/* HW timer in 3xPWM mode */
	TIMER_InitCC_TypeDef timerCCInit = {
		.eventCtrl	=	timerEventEveryEdge,
		.edge		=	timerEdgeBoth,
		.prsSel		=	timerPRSSELCh0,
		.cufoa		=	timerOutputActionSet,
		.cofoa		=	timerOutputActionNone,
		.cmoa		=	timerOutputActionClear,
		.mode		=	timerCCModePWM,
		.filter		=	false,
		.prsInput	=	false,
		.coist		=	false,
	#if REEF_RGB_ACTIVE == (1)
		.outInvert	=	false,
	#else 
		.outInvert	=	true,
	#endif
	};

	/* Set Top Value */
	uint32_t frequency = CMU_ClockFreqGet(REEF_TIMER_CLK)/REEF_RGBPWMFREQ_HZ;
	TIMER_TopSet(REEF_TIMER_DEV, frequency);
	_REEF.RGBctl.scaleFactor = (uint16_t)(((float)TIMER_TopGet(REEF_TIMER_DEV)) / 255.0);	/* Input color values are 8bit max */

	/* Prep Compare Channels */
	TIMER_InitCC(REEF_TIMER_DEV, REEF_TIMER_RED_CC, &timerCCInit);
	TIMER_InitCC(REEF_TIMER_DEV, REEF_TIMER_GREEN_CC, &timerCCInit);
	TIMER_InitCC(REEF_TIMER_DEV, REEF_TIMER_BLUE_CC, &timerCCInit);
	TIMER_CompareBufSet(REEF_TIMER_DEV, REEF_TIMER_RED_CC, 0);
	TIMER_CompareBufSet(REEF_TIMER_DEV, REEF_TIMER_GREEN_CC, 0);
	TIMER_CompareBufSet(REEF_TIMER_DEV, REEF_TIMER_BLUE_CC, 0);

	/* Set timer parameters */
	TIMER_Init_TypeDef timerInit = {
		.enable		=	true,
		.debugRun	=	false,
		.prescale	=	timerPrescale1,
		.clkSel		=	timerClkSelHFPerClk,
		.fallAction	=	timerInputActionNone,
		.riseAction	=	timerInputActionNone,
		.mode		=	timerModeUp,
		.dmaClrAct	=	false,
		.quadModeX4	=	false,
		.oneShot	=	false,
		.sync		=	false,
	};

	#if (REEF_TIMER_RED_CC == 0) || (REEF_TIMER_GREEN_CC == 0) || (REEF_TIMER_BLUE_CC == 0)
	REEF_TIMER_DEV->ROUTEPEN |= TIMER_ROUTEPEN_CC0PEN;
	#endif
	#if (REEF_TIMER_RED_CC == 1) || (REEF_TIMER_GREEN_CC == 1) || (REEF_TIMER_BLUE_CC == 1)
	REEF_TIMER_DEV->ROUTEPEN |= TIMER_ROUTEPEN_CC1PEN;
	#endif
	#if (REEF_TIMER_RED_CC == 2) || (REEF_TIMER_GREEN_CC == 2) || (REEF_TIMER_BLUE_CC == 2)
	REEF_TIMER_DEV->ROUTEPEN |= TIMER_ROUTEPEN_CC2PEN;
	#endif
	#if (REEF_TIMER_RED_CC == 3) || (REEF_TIMER_GREEN_CC == 3) || (REEF_TIMER_BLUE_CC == 3)
	REEF_TIMER_DEV->ROUTEPEN |= TIMER_ROUTEPEN_CC3PEN;
	#endif

	REEF_TIMER_DEV->ROUTELOC0 |= (REEF_TIMER_RED_CCLOC | REEF_TIMER_GREEN_CCLOC | REEF_TIMER_BLUE_CCLOC);
	TIMER_IntEnable(REEF_TIMER_DEV, TIMER_IEN_OF);
	NVIC_EnableIRQ(REEF_TIMER_IRQ);
	TIMER_Init(REEF_TIMER_DEV, &timerInit);		/* RGB Timer start PWM output */
	#endif
}

/* update all RGB values */
static void __REEF_RGBupdate(void) {
	#if (REEF_RGBNB > 0)
	uint32_t i;
	for (i=0; i<REEF_RGBNB; i++) {
		/* State sync */
		if (_REEF.RGB[i].mode != _REEF.RGB[i].newMode) {
			_REEF.RGB[i].cnt = 0;
			_REEF.RGB[i].mode = _REEF.RGB[i].newMode;
		}

		/* State exec */
		switch(_REEF.RGB[i].mode) {
			/* ------- SOLID OFF ------ */
			/*	__						*/
			/*	 |_____________________	*/
			/* ------------------------ */
			case REEF_RGBoff: {
				_REEF.RGB[i].redReg =	0;
				_REEF.RGB[i].greenReg =	0;
				_REEF.RGB[i].blueReg =	0;
				break;
			}
			/* ------- SOLID ON ------- */
			/*	 ______________________	*/
			/*	_|  					*/
			/* ------------------------ */
			case REEF_RGBon: {
				_REEF.RGB[i].redReg =	_REEF.RGB[i].colorA.red * _REEF.RGBctl.scaleFactor;
				_REEF.RGB[i].greenReg =	_REEF.RGB[i].colorA.green * _REEF.RGBctl.scaleFactor;
				_REEF.RGB[i].blueReg =	_REEF.RGB[i].colorA.blue * _REEF.RGBctl.scaleFactor;
				break;
			}
			/* ----- SQUARE WAVE ------ */
			/*	 ____   ____   ____   	*/
			/*	_|  |___|  |___|  |___	*/
			/* ------------------------ */
			case REEF_RGBsquare: {
				_REEF.RGB[i].cnt++;
				/* wrap around */
				if (_REEF.RGB[i].cnt >= _REEF.RGB[i].period) {
					_REEF.RGB[i].cnt = 0;
					_REEF.RGB[i].redReg =	_REEF.RGB[i].colorA.red * _REEF.RGBctl.scaleFactor;
					_REEF.RGB[i].greenReg =	_REEF.RGB[i].colorA.green * _REEF.RGBctl.scaleFactor;
					_REEF.RGB[i].blueReg =	_REEF.RGB[i].colorA.blue * _REEF.RGBctl.scaleFactor;
				/* First half period */
				} else if (_REEF.RGB[i].cnt < (_REEF.RGB[i].period/2) ) {
					_REEF.RGB[i].redReg =	_REEF.RGB[i].colorA.red * _REEF.RGBctl.scaleFactor;
					_REEF.RGB[i].greenReg =	_REEF.RGB[i].colorA.green * _REEF.RGBctl.scaleFactor;
					_REEF.RGB[i].blueReg =	_REEF.RGB[i].colorA.blue * _REEF.RGBctl.scaleFactor;
				/* Last half period */
				} else {
					_REEF.RGB[i].redReg =	_REEF.RGB[i].colorB.red * _REEF.RGBctl.scaleFactor;
					_REEF.RGB[i].greenReg =	_REEF.RGB[i].colorB.green * _REEF.RGBctl.scaleFactor;
					_REEF.RGB[i].blueReg =	_REEF.RGB[i].colorB.blue * _REEF.RGBctl.scaleFactor;
				}

				break;
			}
			/* ---- TRIANGLE WAVE ----- */
			/*	  ___     ___     ___  	*/
			/*	_/   \___/   \___/   \_	*/
			/* ------------------------ */
			case REEF_RGBtriangle: {


				break;
			}
			/* ------ HEART BEAT ------ */
			/*	       ___             	*/
			/*	______j   \____________	*/
			/* ------------------------ */
			case REEF_RGBheartBeat: {


				break;
			}
		}
	}
	#endif
}

/* ================ */
/* == RGB String == */
/* ================ */
static void __REEF_RGBSTRinit(void) {
	#if (REEF_RGBSTRPIXNB > 0)
	CMU_ClockEnable(REEF_RGBSTRSPI_CLK, true);

	/* IOs & CTL */
	GPIO_PinModeSet(REEFpins.RGBSTR.port, REEFpins.RGBSTR.pin, REEFpins.RGBSTR.mode, 1);	/* Idle is high for WS2812B */
	GPIO_PinModeSet(gpioPortC, 7, gpioModePushPull, 1);
	memset(_REEF.RGBSTR.pixBuf[0], 0, _REEF_RGBSTR_SPIBYTEPERPIX);
	memset(_REEF.RGBSTR.pixBuf[1], 0, _REEF_RGBSTR_SPIBYTEPERPIX);

	/* HW SPI */
	USART_InitSync_TypeDef initSync = USART_INITSYNC_DEFAULT;
	initSync.master = true;
	initSync.clockMode = usartClockMode0;
	initSync.baudrate = _REEF_RGBSTR_SPIBITRATE;
	initSync.databits = usartDatabits8;
	initSync.msbf = true;
	USART_InitSync(REEF_RGBSTRSPI_DEV, &initSync);

	#ifdef DEBUG
		REEF_RGBSTRSPI_DEV->ROUTEPEN = USART_ROUTEPEN_TXPEN;
		REEF_RGBSTRSPI_DEV->ROUTELOC0 = REEF_RGBSTRSPI_LOCTX;
	#else
		REEF_RGBSTRSPI_DEV->CTRL |= USART_CTRL_TXINV;
	#endif

	/* DMA */
	// LDMA_Init_t init = LDMA_INIT_DEFAULT;
	// LDMA_Init(&init);

	LDMA_Descriptor_t xfer[] = {
		LDMA_DESCRIPTOR_LINKREL_M2P_BYTE(&_REEF.RGBSTR.pixBuf[0], &REEF_RGBSTRSPI_DEV->TXDATA, _REEF_RGBSTR_SPIBYTEPERPIX, 1),
		LDMA_DESCRIPTOR_LINKREL_M2P_BYTE(&_REEF.RGBSTR.pixBuf[1], &REEF_RGBSTRSPI_DEV->TXDATA, _REEF_RGBSTR_SPIBYTEPERPIX, -1),
	};
	_REEF.RGBSTR.DMAdesc[0] = xfer[0];
	_REEF.RGBSTR.DMAdesc[1] = xfer[1];
	#endif
	REEF_RGBSTRsetAlpha(0xFF);
	REEF_RGBSTRfill(0, REEF_RGBSTRPIXNB, REEpalette[REE_WHITE]);
}

/* Prepare a SPI buffer with the pixel data */
/* ========= WARNING ========== */
/* DO NOT DELETE THIS FUNCTION	*/
/* EVEN IF COMMENTED OUT		*/
/* UNTIL FURTHER NOTICE			*/
/* ============================ */
/* 32.22us CM4@38MHz with GCC 4.9.3 -O0 */
/* 10.33us CM4@38MHz with GCC 4.9.3 -O3 */
	// static void __REEF_RGBSTRencodePix(uint8_t * dst, REEFrgb_t pixData) {
	// 	#if (REEF_RGBSTRPIXNB > 0)
	// 	uint16_t SPIbyteID = 0;
	// 	uint8_t dataID = 0;
	// 	uint8_t mask, shift, testVal;
	// 	uint8_t wBuf[3] = {pixData.green, pixData.red, pixData.blue};

	// 	/* Scan through all 3 color x 8bit in 2bit chunk */
	// 	for (SPIbyteID=0; SPIbyteID<_REEF_RGBSTR_SPIBYTEPERPIX; SPIbyteID++) {

	// 		shift = (SPIbyteID&0b11)*2;		/* bit shift for current pos in RGB Byte */
	// 		mask = (0b11000000 >> shift);

	// 		/* Go through data at slower pace */
	// 		dataID = (SPIbyteID+1)/_WS2812B_SPIBYTEPERRGBBYTE;

	// 		/* mask, then reshift data in correct position */
	// 		testVal = (wBuf[dataID] & mask) >> ((0b110) - shift);

	// 		switch (testVal) {
	// 			case 0b01:	dst[SPIbyteID] = _WS2812B_SPICODE_01;	break;
	// 			case 0b10:	dst[SPIbyteID] = _WS2812B_SPICODE_10;	break;
	// 			case 0b11:	dst[SPIbyteID] = _WS2812B_SPICODE_11;	break;
	// 			default:	dst[SPIbyteID] = _WS2812B_SPICODE_00;	break;
	// 		}
	// 	}
	// 	#endif
	// }

/* 22.6us CM4@38MHz with GCC 4.9.3 -O0 */
/* 4.6us CM4@38MHz with GCC 4.9.3 -O3 */
	// static void __REEF_RGBSTRencodePix(uint8_t * dst, REEFrgb_t pixData) {
	// 	#if (REEF_RGBSTRPIXNB > 0)
	// 	uint8_t byteID = 0;
	// 	uint8_t colorID = 0;
	// 	uint8_t testVal;
	// 	uint8_t wBuf[3] = {pixData.green, pixData.red, pixData.blue};

	// 	#warning "TODO@LaurenceDV: Mask with more bits at the same time, should be faster, you know, maximise bus/reg width usage"
	// 	/* Scan through all 3 color x 8bit in 2bit chunk */
	// 	for (colorID=0; colorID<3; colorID++) {
	// 		for (byteID=0; byteID<_WS2812B_SPIBYTEPERRGBBYTE/2; byteID++) {
	// 			testVal = wBuf[colorID] & (0b11110000 >> (byteID<<2));
	// 			testVal >>= (((_WS2812B_SPIBYTEPERRGBBYTE-1) - byteID) <<2);

	// 			switch (testVal) {
	// 				case 0b01:	*dst = _WS2812B_SPICODE_01;	break;
	// 				case 0b10:	*dst = _WS2812B_SPICODE_10;	break;
	// 				case 0b11:	*dst = _WS2812B_SPICODE_11;	break;
	// 				default:	*dst = _WS2812B_SPICODE_00;	break;
	// 			}
	// 			dst++;
	// 		}
	// 	}
	// 	#endif
	// }

/* 12.15us CM4@38MHz with GCC 4.9.3 -O0 */
/* 3.2us CM4@38MHz with GCC 4.9.3 -O3 */
static void __REEF_RGBSTRencodePix(uint8_t * dst, REEFrgb_t pixData) {
	#if (REEF_RGBSTRPIXNB > 0)
	int8_t i;
	uint8_t colorID = 0;
	uint8_t testVal;
	uint8_t wBuf[3] = {pixData.green, pixData.red, pixData.blue};
	
	/* Scan through all 3 color x 8bit in 2bit chunk */
	for (colorID=0; colorID<3; colorID++) {
		for (i=1; i>=0; i--) {		/* Start by the MSb of the color */
			testVal = wBuf[colorID] >> (_REEF_RGBSTR_ENCODEMASKWIDTH*i) ;
			testVal &= _REEF_RGBSTR_ENCODEMASK;

			/* Encode the 4bit masked, cheap bit re-ordering is done here */
			switch (testVal) {
				default:		dst[1] = _WS2812B_SPICODE_00;	dst[0] = _WS2812B_SPICODE_00;	break;
				case 0b0001:	dst[1] = _WS2812B_SPICODE_01;	dst[0] = _WS2812B_SPICODE_00;	break;
				case 0b0010:	dst[1] = _WS2812B_SPICODE_10;	dst[0] = _WS2812B_SPICODE_00;	break;
				case 0b0011:	dst[1] = _WS2812B_SPICODE_11;	dst[0] = _WS2812B_SPICODE_00;	break;
				case 0b0100:	dst[1] = _WS2812B_SPICODE_00;	dst[0] = _WS2812B_SPICODE_01;	break;
				case 0b0101:	dst[1] = _WS2812B_SPICODE_01;	dst[0] = _WS2812B_SPICODE_01;	break;
				case 0b0110:	dst[1] = _WS2812B_SPICODE_10;	dst[0] = _WS2812B_SPICODE_01;	break;
				case 0b0111:	dst[1] = _WS2812B_SPICODE_11;	dst[0] = _WS2812B_SPICODE_01;	break;
				case 0b1000:	dst[1] = _WS2812B_SPICODE_00;	dst[0] = _WS2812B_SPICODE_10;	break;
				case 0b1001:	dst[1] = _WS2812B_SPICODE_01;	dst[0] = _WS2812B_SPICODE_10;	break;
				case 0b1010:	dst[1] = _WS2812B_SPICODE_10;	dst[0] = _WS2812B_SPICODE_10;	break;
				case 0b1011:	dst[1] = _WS2812B_SPICODE_11;	dst[0] = _WS2812B_SPICODE_10;	break;
				case 0b1100:	dst[1] = _WS2812B_SPICODE_00;	dst[0] = _WS2812B_SPICODE_11;	break;
				case 0b1101:	dst[1] = _WS2812B_SPICODE_01;	dst[0] = _WS2812B_SPICODE_11;	break;
				case 0b1110:	dst[1] = _WS2812B_SPICODE_10;	dst[0] = _WS2812B_SPICODE_11;	break;
				case 0b1111:	dst[1] = _WS2812B_SPICODE_11;	dst[0] = _WS2812B_SPICODE_11;	break;
			}

			dst+=2;
		}
	}
	#endif
}


/* Resend all RGBSTR pixel data */
static void __REEF_RGBSTRupdate(void) {
	#if (REEF_RGBSTRPIXNB > 0)
	_REEF.RGBSTR.pixCnt = 0;
	/* Start by the last pixel */
	__REEF_RGBSTRencodePix(&(_REEF.RGBSTR.pixBuf[0][0]), _REEF.RGBSTR.realBuf[0]);
	__REEF_RGBSTRencodePix(&(_REEF.RGBSTR.pixBuf[1][0]), _REEF.RGBSTR.realBuf[1]);
	_REEF.RGBSTR.DMAdesc[0].xfer.link = 1;
	_REEF.RGBSTR.DMAdesc[1].xfer.link = -1;
	LDMA_StartTransfer(REEF_RGBSTRDMA_CH, (void*)&_REEF.RGBSTR.DMAtrig, (void*)&_REEF.RGBSTR.DMAdesc);
	#endif
}

/* ============= */
/* == Display == */
/* ============= */
static void __REEF_DISPinit(void) {
	#if (REEF_DISPEN == 1)
	nop();
	#endif
}

static void __REEF_DISPupdate(void) {
	#if (REEF_DISPEN == 1)
	nop();
	#endif
}


/* ============ */
/* == Render == */
/* ============ */
static void __REEF_RNDRappendCb(renderCb_t newCb, void * args) {
	/* Scan through the table */
	uint8_t i;
	for (i=0; i<REEF_RENDERCBNB; i++) {
		/* Append at first empty slot */
		if (_REEF.render.cb[i] == NULL) {
			_REEF.render.cb[i] = newCb;
			_REEF.render.args[i] = args;
			return;
		}
	}
}

/* Re-compute effects and other heavy stuff */
static void __REEF_render(void) {
	uint16_t i;

	/* Execute all attached rendering cb */
	for (i=0; i<REEF_RENDERCBNB; i++) {
		/* Stop at empty slot */
		if (_REEF.render.cb[i] == NULL) {
			break;
		}

		/* Execute CB */
		_REEF.render.cb[i](_REEF.render.args[i]);
	}


	/* RGBSTR alpha level */
	#if (REEF_RGBSTRPIXNB > 0)
	for (i=0; i<REEF_RGBSTRPIXNB; i++) {
		_REEF.RGBSTR.realBuf[i] = rgb888_alpha(&_REEF.RGBSTR.frameBuf[i], _REEF.RGBSTR.alpha);
	}
	#endif
}

/* -------------------------------- +
|									|
|	Thread							|
|									|
+ -------------------------------- */
void REEF_thread(void *argument) {
	(void)argument;			/* remove warning */
	bool run = true;

	/* Init */
	REEF_init();

	/* Loop */
	while (run) {
		/* 0. Wait for any signals */
		osThreadFlagsWait(_REEF_ALLSIGNAL, osFlagsWaitAny|osFlagsNoClear, osWaitForever);
		REEFSignals_t receivedSig = (REEFSignals_t)osThreadFlagsGet();	/* Signals must be cleared, so local copy needed */
		osThreadFlagsClear(_REEF_ALLSIGNAL);

		/* 1. Decode and process all signals */
		/* Stop execution */
		if (receivedSig & REEF_sig_stop) {
			if (osOK != osTimerStop(_REEF.frameTimer)) {
				/* Timer couldn't stop... dafuq... do something relevant! */
			}

			CLR_BIT(receivedSig, REEF_sig_stop);	/* Signal treated */
		}

		/* Render job */
		if (receivedSig & REEF_sig_render) {
			__REEF_render();

			CLR_BIT(receivedSig, REEF_sig_render);	/* Signal treated */
		}

		/* Execute a new frame */
		if (receivedSig & REEF_sig_execFrame) {

			/* ============================== */
			/* == 1 == Read buttons ========= */
			/* ============================== */
			__REEF_BTNupdate();

			/* ============================== */
			/* == 2 == Output LEDs ========== */
			/* ============================== */
			__REEF_LEDupdate();
			
			/* ============================== */
			/* == 3 == Output RGBs ========== */
			/* ============================== */
			__REEF_RGBupdate();
			
			/* ============================== */
			/* == 4 == Update RGB String ==== */
			/* ============================== */
			__REEF_RGBSTRupdate();

			/* ============================== */
			/* == 5 == Update Display ======= */
			/* ============================== */
			__REEF_DISPupdate();

			CLR_BIT(receivedSig, REEF_sig_execFrame);	/* Signal treated */
		}


		/* 2. Prepare to sleep */
		nop();
	}
}


/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */
/* ============= */
/* == General == */
/* ============= */
void REEF_startThread(void) {
	_REEF.threadId = osThreadNew(REEF_thread, NULL, &__REEFThreadConf);
}

/* Main init */
void REEF_init(void) {
	osStatus_t  status;

	/* If already init force shutdown */
	if (_REEF.state == _REEF_state_active) {
		_REEF.state = _REEF_state_unknow;
	}

	/* RAM */
	_REEF.confLock = osMutexNew(&_REEFMutexConf);
	if (_REEF.confLock == NULL) {
		_REEF.state = _REEF_state_error;
		return;
	}

	/* Clocks */
	CMU_ClockEnable(cmuClock_HFPER, true);
	CMU_ClockEnable(cmuClock_GPIO, true);
	
	/* LEDs */
	__REEF_LEDinit();

	/* RGBs */
	__REEF_RGBinit();

	/* Buttons */
	__REEF_BTNinit();

	/* Display */
	__REEF_DISPinit();

	/* RGBString */
	__REEF_RGBSTRinit();


	/* Framing start */
	_REEF.frameTimer = osTimerNew(__REEF_frameTimer_cb, osTimerPeriodic, NULL, &__REEFTimerConf);
	if (_REEF.frameTimer != NULL) {
		status = osTimerStart(_REEF.frameTimer, (uint32_t)(1000.0/(float)REEF_REFRESHRATE_HZ) );	/* Start the timer with period = 1000ms/freq */
		if (status != osOK) {
			_REEF.state = _REEF_state_error;	/* Timer didn't start */
			return;
		}
 	} else {
		_REEF.state = _REEF_state_error;		/* Timer not created */
		return;
	}
		
	_REEF.state = _REEF_state_active;
}

/* ========= */
/* == BTN == */
/* ========= */
/* Register a functions as a BTN callbacks */
void REEF_BTNregisterCb(uint8_t btnID, REEFBTNtrig_t trigger, REEFbtnCb_t callback) {
	if (btnID < REEF_BTNNB) {
		if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
			if (callback != NULL) {
				switch (trigger) {
					case REEF_BTNpress: {
						__REEF_BTNappendCb(_REEF.BTN[btnID].pressCb,	callback);
						break;
					}
					case REEF_BTNrelease: {
						__REEF_BTNappendCb(_REEF.BTN[btnID].releaseCb,	callback);
						break;
					}
					case REEF_BTNshort: {
						__REEF_BTNappendCb(_REEF.BTN[btnID].shortCb,	callback);
						break;
					}
					case REEF_BTNlong: {
						__REEF_BTNappendCb(_REEF.BTN[btnID].longCb,		callback);
						break;
					}
					case REEF_BTNperiodic: {
						__REEF_BTNappendCb(_REEF.BTN[btnID].periodicCb, callback);
						break;
					}
					case REEF_BTNall: {
						__REEF_BTNappendCb(_REEF.BTN[btnID].pressCb,	callback);
						__REEF_BTNappendCb(_REEF.BTN[btnID].releaseCb,	callback);
						__REEF_BTNappendCb(_REEF.BTN[btnID].shortCb,	callback);
						__REEF_BTNappendCb(_REEF.BTN[btnID].longCb,		callback);
						__REEF_BTNappendCb(_REEF.BTN[btnID].periodicCb,	callback);
						break;
					}
					default:	break;
				}
			}
			osMutexRelease(_REEF.confLock);
		}
	}
}

/* ========= */
/* == LED == */
/* ========= */
/* Set the LED in a new mode */
void REEF_LEDsetMode(uint8_t LEDID, REEFLEDmode_t mode) {
	if (LEDID < REEF_LEDNB) {
		if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
			_REEF.LED[LEDID].newMode = mode;
			osMutexRelease(_REEF.confLock);
		}
	}
}

/* Set the LED cycling period */
void REEF_LEDsetPeriod(uint8_t LEDID, int32_t millisec) {
	if ((LEDID < REEF_RGBNB) && (millisec > 0)) {
		if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
			uint32_t newPeriod = MAX(millisec, (uint32_t)INT32_MAX);
			_REEF.LED[LEDID].period = newPeriod / (1000/REEF_REFRESHRATE_HZ);
			osMutexRelease(_REEF.confLock);
		}
	}
}

/* Get the LED cycling period */
int32_t REEF_LEDgetPeriod(uint8_t LEDID) {
	if (LEDID < REEF_RGBNB) {
		return (__REEF_FRAMECNT2MS(_REEF.LED[LEDID].period));
	}
	return INT32_MIN;
}

/* ========= */
/* == RGB == */
/* ========= */
/* Force a instant color on the RGB */
void REEF_RGBforce(uint8_t RGBID, REEFrgb_t color) {
	if (RGBID < REEF_RGBNB) {
		if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
			_REEF.RGB[RGBID].colorA.red = color.red;
			_REEF.RGB[RGBID].colorA.green = color.green;
			_REEF.RGB[RGBID].colorA.blue = color.blue;
			_REEF.RGB[RGBID].newMode = REEF_RGBon;
			osMutexRelease(_REEF.confLock);
		}
	}
}

/* Set the RGB in a new mode */
void REEF_RGBsetMode(uint8_t RGBID, REEFRGBmode_t mode) {
	if (RGBID < REEF_RGBNB) {
		if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
			_REEF.RGB[RGBID].newMode = mode;
			osMutexRelease(_REEF.confLock);
		}
	}
}

/* Set the RGB cycling period */
void REEF_RGBsetPeriod(uint8_t RGBID, int32_t millisec) {
	if ((RGBID < REEF_RGBNB) && (millisec > 0)){
		if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
			uint32_t newPeriod = MIN(millisec, (uint32_t)INT32_MAX);
			_REEF.RGB[RGBID].period = newPeriod / (1000/REEF_REFRESHRATE_HZ);
			osMutexRelease(_REEF.confLock);
		}
	}
}

/* Get the RGB cycling period */
int32_t REEF_RGBgetPeriod(uint8_t RGBID) {
	if (RGBID < REEF_RGBNB) {
		return (__REEF_FRAMECNT2MS(_REEF.RGB[RGBID].period));
	}
	return INT32_MIN;
}

/* Set color A */
void REEF_RGBsetColorA(uint8_t RGBID, REEFrgb_t color) {
	if (RGBID < REEF_RGBNB) {
		if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
			_REEF.RGB[RGBID].colorA.red = color.red;
			_REEF.RGB[RGBID].colorA.green = color.green;
			_REEF.RGB[RGBID].colorA.blue = color.blue;
			osMutexRelease(_REEF.confLock);
		}
	}
}

/* Set color B */
void REEF_RGBsetColorB(uint8_t RGBID, REEFrgb_t color) {
	if (RGBID < REEF_RGBNB) {
		if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
			_REEF.RGB[RGBID].colorB.red = color.red;
			_REEF.RGB[RGBID].colorB.green = color.green;
			_REEF.RGB[RGBID].colorB.blue = color.blue;
			osMutexRelease(_REEF.confLock);
		}
	}
}

/* ============= */
/* == RGB STR == */
/* ============= */
/* Force a new color on the RGBSTR pixel */
void REEF_RGBSTRsetPixel(uint8_t PIXID, REEFrgb_t color) {
	#if (REEF_RGBSTRPIXNB > 0)
	if (PIXID < REEF_RGBSTRPIXNB) {
		if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
			_REEF.RGBSTR.frameBuf[PIXID] = rgb888_blend(&color, NULL, _REEF.RGBSTR.alpha);
			osThreadFlagsSet(_REEF.threadId, REEF_sig_render);
			osMutexRelease(_REEF.confLock);
		}
	}
	#endif
}

/* Fill pixels with a single color */
void REEF_RGBSTRfill(uint8_t startID, uint8_t stopID, REEFrgb_t color) {
	#if (REEF_RGBSTRPIXNB > 0)
	if ((startID<REEF_RGBSTRPIXNB)&&(stopID<REEF_RGBSTRPIXNB)) {
		uint8_t pixID;
		if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
			for (pixID=startID; pixID<=stopID; pixID++) {
				_REEF.RGBSTR.frameBuf[pixID] = color;
			}
			osThreadFlagsSet(_REEF.threadId, REEF_sig_render);
			osMutexRelease(_REEF.confLock);
		}
	}
	#endif
}

void REEF_RGBSTRsetAlpha(uint8_t alpha) {
	#if (REEF_RGBSTRPIXNB > 0)
	if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
		_REEF.RGBSTR.alpha = alpha;
		osThreadFlagsSet(_REEF.threadId, REEF_sig_render);
		osMutexRelease(_REEF.confLock);
	}
	#endif
}

uint8_t REEF_RGBSTRgetAlpha(void) {
	#if (REEF_RGBSTRPIXNB > 0)
	uint8_t result = 0;
	if (osOK == osMutexAcquire(_REEF.confLock, REEF_CMDTIMEOUT_MS)) {
		result = _REEF.RGBSTR.alpha;
		osMutexRelease(_REEF.confLock);
	}
	return result;
	#else
	return 0;	/* Comply with the prototype... */
	#endif
}

//void REEF_RGBSTRaddVUmeter(VUmeter_t * handle) {
//	/* Set correct destination */
//	handle->dest = &(_REEF.RGBSTR.frameBuf[handle->startID]);
//
//	/* Register render callback */
//	__REEF_RNDRappendCb((renderCb_t)render_VUmeter, (void*)handle);
//}

/*--------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */
void REEF_RGBISR(void) {
	if (TIMER_IntGetEnabled(REEF_TIMER_DEV) & TIMER_IF_OF) {
		
		_REEF.RGBctl.muxCnt++;
		if (_REEF.RGBctl.muxCnt == _REEF_RGBMUX_TOP) {

			/* Disable old RGB */
			__REEF_DISACTIVATE_RGBCOM(_REEF.RGBctl.ID);
			/* Select new RGB */
			_REEF.RGBctl.ID++;
			if (_REEF.RGBctl.ID == REEF_RGBNB) {
				_REEF.RGBctl.ID = 0;
			}
			/* Prep new color */
			TIMER_CompareBufSet(REEF_TIMER_DEV, REEF_TIMER_RED_CC,	_REEF.RGB[_REEF.RGBctl.ID].redReg);
			TIMER_CompareBufSet(REEF_TIMER_DEV, REEF_TIMER_GREEN_CC,_REEF.RGB[_REEF.RGBctl.ID].greenReg);
			TIMER_CompareBufSet(REEF_TIMER_DEV, REEF_TIMER_BLUE_CC,	_REEF.RGB[_REEF.RGBctl.ID].blueReg);

		} else if (_REEF.RGBctl.muxCnt > _REEF_RGBMUX_TOP) {
			_REEF.RGBctl.muxCnt = 0;

			/* Activate new RGB */
			__REEF_ACTIVATE_RGBCOM(_REEF.RGBctl.ID);
		}
		/* -------------- */

		TIMER_IntClear(REEF_TIMER_DEV, TIMER_IFC_OF);
	}
}

void REEF_RGBSTRISR(void) {
#if (REEF_RGBSTRPIXNB > 0)
	uint8_t bufId = ((uint8_t)_REEF.RGBSTR.pixCnt) & 0b00000001;	/* Buffer to update is the last used */
	_REEF.RGBSTR.pixCnt++;

	/* C. Finished reset Sequence, rise! */
	if (_REEF.RGBSTR.pixCnt >= ((REEF_RGBSTRPIXNB-2) + _WS2812B_PIXELPERRESETSEQ)) {
		_REEF.RGBSTR.DMAdesc[0].xfer.link = 0;
		_REEF.RGBSTR.DMAdesc[1].xfer.link = 0;
		memcpy(&_REEF.RGBSTR.pixBuf[bufId], _REEF_RGBSTRresetSeq[1], sizeof(_REEF_RGBSTRresetSeq[1]));	/* Stop reset sequence */
	/* B. Updated all pixels, start Reset Sequence */
	} else if (_REEF.RGBSTR.pixCnt >= (REEF_RGBSTRPIXNB-1)) {
		memcpy(&_REEF.RGBSTR.pixBuf[bufId], _REEF_RGBSTRresetSeq[0], sizeof(_REEF_RGBSTRresetSeq[0]));	/* Start reset sequence */
	/* A. Updating pixels colors... */
	} else {
		/* The buffer index sub is to send the last pixel first, and respect ping-pong alignment */
		__REEF_RGBSTRencodePix(&_REEF.RGBSTR.pixBuf[bufId][0], _REEF.RGBSTR.realBuf[_REEF.RGBSTR.pixCnt+1]);
	}
#endif
}
