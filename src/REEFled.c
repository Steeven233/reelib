/*
	@name		REEFled.c
	@author		Laurence DV
	@version	2.0.0
	@brief		REEF LED driver
	@note		
	@license	All right reserved RealEE inc. (2019)
*/
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <REEFled.h>
#include <REEBUG.h>			/* REEBUG fct are empty stub when symbol DEBUG is not defined */

#include <em_device.h>		/* for HW module addresses */
#include <em_cmu.h>			/* Clock access */
#include <em_core.h>		/* ATOMIC operation */
#include <em_ldma.h>		/* LDMA access */

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */




/* -------------------------------- +
|									|
|	Thread							|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */



/*--------------------------------- +
|									|
|	ISR								|
|									|
+ -------------------------------- */

#pragma GCC diagnostic warning "-Wunused-function"
#pragma GCC diagnostic warning "-Wunused-parameter"
