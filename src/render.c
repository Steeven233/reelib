/*
	\name		render.c
	\author		Laurence DV
	\date		2018-09-13
	\version	0.1.0
	\brief		
	\note		
	\license	All right reserved RealEE inc. (2018)
*/


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <render.h>


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	API								|
|									|
+ -------------------------------- */


render_err_t render_VUmeter(VUmeter_t * handle) {
	if (handle != NULL) {
		uint8_t pixNb = (handle->stopID - handle->startID)+1;
		uint8_t i;
		uint8_t upperLimit = 0;
		uint8_t lowerLimit = 0;
		renderrgb_t	colorBuf;

		/* Treat each pixel individually */
		for (i=0; i<pixNb; i++) {
			lowerLimit = upperLimit;
			upperLimit = (((uint16_t)i * (uint16_t)UINT8_MAX) /pixNb);

			/* Compute color */
			if (*(handle->control) >= upperLimit) {
				colorBuf = handle->activeColor;		/* Full ON, no need to blend */
			} else if (*(handle->control) < lowerLimit) {
				colorBuf = handle->inactiveColor;	/* Full OFF */
			} else {
				/* Partial ON */
				colorBuf = rgb888_blend(&(handle->activeColor), &(handle->inactiveColor), *(handle->control));
			}

			colorBuf = rgb888_alpha(&colorBuf, handle->alpha);	/* Alpha */

			handle->dest[handle->startID+i] = colorBuf;		/* Apply color */


		}


		return render_success;
	}
	return render_null;
}
