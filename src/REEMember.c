/*
 * REEMember.c
 *
 *  Created on: Jun 9, 2019
 *      Author: jlecuyer
 */
#include <REEMember.h>
#include <string.h>
#include <REEBUG.h>


REEMember_t REEMember_save(uint8_t *data, size_t size){
	MSC_Status_TypeDef 	msc;
	REEMember_t			reesult;

	if(size > REEMEMBER_SIZE){
		reesult =  REEMember_tooBig;
		REEBUGSWO_String("REEMember_save::too big");

	}else {
		msc = MSC_ErasePage(REEMEMBER_ADDRESS);

		if(msc != mscReturnOk){
			REEBUGSWO_String("REEMember_save::erase failed");
		}else{

			MSC_Init();
			msc = MSC_WriteWord(REEMEMBER_ADDRESS, data, size);
			MSC_Deinit();
			if(msc != mscReturnOk){
				REEBUGSWO_String("REEMember_save::write failed");
			}
		}
		reesult = msc;
	}
	return reesult;
}


REEMember_t REEMember_load(uint8_t *data, size_t size){
	REEMember_t			reesult;

	if(size > REEMEMBER_SIZE){
		REEBUGSWO_String("REEMember_load::too big");
		reesult =  REEMember_tooBig;

	}else {
		memcpy(data, REEMEMBER_ADDRESS, size);
		reesult = REEMember_ok;
	}
	return reesult;
}


REEMember_t REEMember_clear(void){
	return (REEMember_t) MSC_ErasePage(REEMEMBER_ADDRESS);
}
