#include <unity.h>
#include <stdbool.h>
#include <stdint.h>

#include <potato.h>

void setUp(void) {

}
void tearDown(void) {

}

void test_areYouHappy_isHappyWithLotsOfPotatoes(void) {

	uint16_t potatoNb = UINT16_MAX;

	TEST_ASSERT(areYouHappy(potatoNb));

}

void test_areYouHappy_isSadWithLessPotatoes(void) {

	uint16_t potatoNb = 1;	/* haha, only 1 potato for you */

	TEST_ASSERT(!areYouHappy(potatoNb));
}


// int main(void) {
// 	UNITY_BEGIN();
// 	RUN_TEST(test_areYouHappy_isHappyWithLotsOfPotatoes);
// 	RUN_TEST(test_areYouHappy_isSadWithLessPotatoes);
// 	return UNITY_END();
// }
